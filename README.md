![enter image description here](https://user-content.gitlab-static.net/dc40785c2756545cb44234c0db464261880079eb/68747470733a2f2f70702e757365726170692e636f6d2f633833373333392f763833373333393736362f36613262652f77594b374c4576547954382e6a7067)


# Welcome to skymp20
A multiplayer mod for Skyrim Legendary Edition. Aims to provide coop and MMO functionality.

## Build Requirements

 1. [CMake 3.15](https://github.com/Kitware/CMake/releases/download/v3.15.6/cmake-3.15.6-win32-x86.msi) or higher. Use `cmake --version` to check.
 2. Visual Studio 2017 or higher.
 3. Binaries of the modified OpenCppCoverage. Contact Leonid Pospelov to get them.
 4. Creation Kit (Steam version recommended).
 5. [Chromium Embedded Framework.](http://opensource.spotify.com/cefbuilds/cef_binary_79.1.36%2Bg90301bd%2Bchromium-79.0.3945.130_windows32.tar.bz2)
 6. Binaries of Viet. Contact Leonid Pospelov to get them.

## Run Requirements
1. [DirectX](https://www.microsoft.com/ru-ru/download/details.aspx?id=35).
2.  Skyrim Legendary Edition v1.9.32.0.8 (Steam version recommended).
3. [SKSE](https://skse.silverlock.org/) (Steam version recommended).

## Building

Run the following commands:
 ```bash
 mkdir build
 cd build
 cmake .. -G "Visual Studio 16 2019" -A Win32 -DSKYRIM_DIR="<Your folder containing TESV.exe>" -DOPENCPPCOV_DIR="<Your folder containing OpenCppCoverage.exe>" -DCEF_PATH="<Your folder containing libcef_dll>" -DVIET_DIST_PATH="<Your folder containing viet.cmake>" -DCPPCOV="<Enable or disable coverage (ON/OFF)>"
```

`-G "Visual Studio 16 2019"` is required for VS 2019. You can skip it if you use VS 2017.

Then, open `build/skymp20.sln` and build. `Debug`, `Release` and `RelWithDebInfo` configurations are supported. Note that CMake will copy binaries into your Skyrim installation automatically.

Feel free to re-generate project files with `cmake ..`
Do this each time you pull.

## Running Tests
skymp20 has CTest-based test system.
```bash
cd build

 # Test everything
ctest -C Debug -V

# Only unit tests
ctest -C Debug -V -R test_unit

# Only Skyrim tests
ctest -C Debug -V -R test_skyrim

# Only VM tests
ctest -C Debug -V -R test_vm
```

## Troubleshooting

**Failed to Bootstrap the vcpkg tool.**
Try run Windows cmd.exe with Admin rights.

**Error: Building package:x86-windows failed with: BUILD_FAILED**
The possible workaround may be opening `winnt.h` (`C:\Program Files (x86)\Windows Kits\10\include\10.0.18362.0\um\winnt.h`) and comment 2487 line containing `C_ASSERT(...`.

**Skyrim starts but then does nothing**
Check that `C:\Users\<YOUR_USER_NAME>\Documents\My Games\Skyrim\SKSE\skse.log` has this line:
 ```
 couldn't load plugin E:\Steam\steamapps\common\Skyrim\Data\SKSE\Plugins\fooplugin.dll (Error 126)
 ```
 If it has, run `fooplugin_exe.exe` in your Skyrim folder and then submit an issue containing error message produced by this executable.

## Related Links

PapyrusVM:
* [Papyrus docs](https://www.creationkit.com/index.php?title=Category:Papyrus)
* [Compiled Script File Format](https://en.uesp.net/wiki/Tes5Mod:Compiled_Script_File_Format)
