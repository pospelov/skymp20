include(../cmake/skymp_execute_process.cmake)
include(../cmake/delete_skyrim_plugins.cmake)
include(../cmake/launch_skyrim.cmake)

launch_skyrim(
  SKYRIM_DIR ${SKYRIM_DIR}
  PLUGIN_PATH ${PLUGIN_PATH}
  TEST_NAME TestCloneForm
)

set(workdir ${CMAKE_CURRENT_BINARY_DIR}/cmake_tests/single_attach_default)

skymp_execute_process(
  EXECUTABLE_PATH ${SKYRIM_DIR}/TESV.exe
  ATTACH TRUE
  CPPCOV OFF
  CPPCOV_PATH ${OPENCPPCOV_DIR}
  OUT_EXIT_CODE EXIT_CODE
  OUT_STDOUT STDOUT
)

if (NOT "${EXIT_CODE}" STREQUAL "0")
  message(FATAL_ERROR "Failed with code \n'${EXIT_CODE}'")
endif()

if (NOT "${STDOUT}" STREQUAL "")
  message(FATAL_ERROR "Unexpected output \n'${STDOUT}'")
endif()
