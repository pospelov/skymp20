include(../cmake/skymp_execute_process.cmake)
include(../cmake/delete_skyrim_plugins.cmake)
include(../cmake/launch_skyrim.cmake)

launch_skyrim(
  SKYRIM_DIR ${SKYRIM_DIR}
  PLUGIN_PATH ${PLUGIN_PATH}
  TEST_NAME TestCloneForm
)

set(workdir ${CMAKE_CURRENT_BINARY_DIR}/cmake_tests/single_attach_cppcov)

skymp_execute_process(
  EXECUTABLE_PATH ${SKYRIM_DIR}/TESV.exe
  ATTACH TRUE
  CPPCOV ON
  CPPCOV_PATH ${OPENCPPCOV_DIR}
  CPPCOV_MODULES *${PLUGIN_NAME}*
  CPPCOV_SOURCES
    ${MY_SOURCE_DIR}\\src\\savefile\\*
    ${MY_SOURCE_DIR}\\src\\skyrim_plugin_base\\utils\\*
  CPPCOV_OUTPUT_DIRECTORY ${workdir}
  OUT_EXIT_CODE EXIT_CODE
  OUT_STDOUT STDOUT
  OUT_STDERR STDERR
)

if (NOT "${EXIT_CODE}" STREQUAL "0")
  message(FATAL_ERROR "Failed with code \n'${EXIT_CODE}'")
endif()

if ("${STDERR}" STREQUAL "")
  message(FATAL_ERROR "Stderr can't be empty when CPPCOV is ON")
endif()

if (NOT "${STDOUT}" STREQUAL "")
  message(FATAL_ERROR "Unexpected output \n'${STDOUT}'")
endif()

if(NOT EXISTS ${workdir}/Modules/fooplugin)
  message(FATAL_ERROR "Coverage report doesn't exist")
endif()

if(NOT EXISTS ${workdir}/LastCoverageResults.log)
  message(FATAL_ERROR "LastCoverageResults.txt doesn't exist")
endif()
