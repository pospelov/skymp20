include(../cmake/skymp_execute_process.cmake)

set(bin ${CMAKE_CURRENT_BINARY_DIR}/cmake_tests/single_cppcov)
file(REMOVE_RECURSE ${bin})
file(MAKE_DIRECTORY ${bin})

execute_process(COMMAND ${CMAKE_COMMAND} -A Win32 ${CMAKE_CURRENT_LIST_DIR}
  WORKING_DIRECTORY ${bin}
  RESULT_VARIABLE res
)
if (NOT "${res}" STREQUAL "0")
  message(FATAL_ERROR "cmake exited with code ${res}")
endif()

execute_process(COMMAND ${CMAKE_COMMAND} --build ${bin}
  WORKING_DIRECTORY ${bin}
  RESULT_VARIABLE res
)
if (NOT "${res}" STREQUAL "0")
  message(FATAL_ERROR "cmake --build exited with code ${res}")
endif()

skymp_execute_process(
  EXECUTABLE_PATH ${bin}/main1.exe
  CPPCOV ON
  CPPCOV_PATH ${OPENCPPCOV_DIR}
  CPPCOV_OUTPUT_DIRECTORY ${bin}/cppcov_out
  OUT_EXIT_CODE EXIT_CODE
  OUT_STDOUT STDOUT
  OUT_STDERR STDERR
)

if (NOT "${EXIT_CODE}" STREQUAL "123")
  message(FATAL_ERROR "Failed with code \n'${EXIT_CODE}'")
endif()

if ("${STDERR}" STREQUAL "")
  message(FATAL_ERROR "Stderr can't be empty when CPPCOV is ON")
endif()

if (NOT "${STDOUT}" STREQUAL "")
  message(FATAL_ERROR "Unexpected output \n'${STDOUT}'")
endif()

if(NOT EXISTS ${bin}/cppcov_out/Modules/main1/main1.cpp.html)
  message(FATAL_ERROR "Coverage report doesn't exist")
endif()

if (NOT "${actual}" STREQUAL "${expected}")
  message(FATAL_ERROR "Different contents")
endif()

if(NOT EXISTS ${bin}/cppcov_out/LastCoverageResults.log)
  message(FATAL_ERROR "LastCoverageResults.txt doesn't exist")
endif()

if(NOT EXISTS ${bin}/cppcov_out/OpenCppCoverage-stdout.log)
  message(FATAL_ERROR "OpenCppCoverage-stdout.log doesn't exist")
endif()

if(NOT EXISTS ${bin}/cppcov_out/OpenCppCoverage-stderr.log)
  message(FATAL_ERROR "OpenCppCoverage-stderr.log doesn't exist")
endif()
