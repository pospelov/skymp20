include(../cmake/skymp_execute_process.cmake)

set(bin ${CMAKE_CURRENT_BINARY_DIR}/cmake_tests/single_default)
file(MAKE_DIRECTORY ${bin})

execute_process(COMMAND ${CMAKE_COMMAND} ${CMAKE_CURRENT_LIST_DIR}
  WORKING_DIRECTORY ${bin}
  RESULT_VARIABLE res
)
if (NOT "${res}" STREQUAL "0")
  message(FATAL_ERROR "cmake exited with code ${res}")
endif()

execute_process(COMMAND ${CMAKE_COMMAND} --build ${bin}
  WORKING_DIRECTORY ${bin}
  RESULT_VARIABLE res
)
if (NOT "${res}" STREQUAL "0")
  message(FATAL_ERROR "cmake --build exited with code ${res}")
endif()

skymp_execute_process(
  EXECUTABLE_PATH ${bin}/main1.exe
  OUT_EXIT_CODE EXIT_CODE
  OUT_STDOUT STDOUT
)

if (NOT "${EXIT_CODE}" STREQUAL "123")
  message(FATAL_ERROR "Failed with code \n'${EXIT_CODE}'")
endif()
if (NOT "${STDOUT}" STREQUAL "Hello main1.cpp")
  message(FATAL_ERROR "Unexpected output \n'${STDOUT}'")
endif()
