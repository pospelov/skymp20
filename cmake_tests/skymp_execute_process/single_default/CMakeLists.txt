cmake_minimum_required(VERSION 3.15)
project(single_test)
add_executable(main1 "main1.cpp")

add_custom_target(CopyExe ALL
  ${CMAKE_COMMAND} -E copy $<TARGET_FILE:main1> ${CMAKE_CURRENT_BINARY_DIR}
)
add_dependencies(CopyExe main1)
