include(../cmake/skymp_execute_process.cmake)

set(bin ${CMAKE_CURRENT_BINARY_DIR}/cmake_tests/multiple_cppcov)
file(REMOVE_RECURSE ${bin})
file(MAKE_DIRECTORY ${bin})

execute_process(COMMAND ${CMAKE_COMMAND} -A Win32 ${CMAKE_CURRENT_LIST_DIR}
  WORKING_DIRECTORY ${bin}
  RESULT_VARIABLE res
)
if (NOT "${res}" STREQUAL "0")
  message(FATAL_ERROR "cmake exited with code ${res}")
endif()

execute_process(COMMAND ${CMAKE_COMMAND} --build ${bin}
  WORKING_DIRECTORY ${bin}
  RESULT_VARIABLE res
)
if (NOT "${res}" STREQUAL "0")
  message(FATAL_ERROR "cmake --build exited with code ${res}")
endif()

set(CPPCOV_TAG "Dj2fQ")

file(REMOVE_RECURSE "${CMAKE_CURRENT_BINARY_DIR}/coverage_tag_${CPPCOV_TAG}")

skymp_execute_process(
  EXECUTABLE_PATH ${bin}/main1.exe
  CPPCOV ON
  CPPCOV_PATH ${OPENCPPCOV_DIR}
  CPPCOV_OUTPUT_DIRECTORY ${bin}/cppcov_out
  CPPCOV_TAG ${CPPCOV_TAG}
  OUT_EXIT_CODE EXIT_CODE_1
  OUT_STDOUT STDOUT_1
  OUT_STDERR STDERR_1
)

skymp_execute_process(
  EXECUTABLE_PATH ${bin}/main2.exe
  CPPCOV ON
  CPPCOV_PATH ${OPENCPPCOV_DIR}
  CPPCOV_OUTPUT_DIRECTORY ${bin}/cppcov_out
  CPPCOV_TAG ${CPPCOV_TAG}
  OUT_EXIT_CODE EXIT_CODE_2
  OUT_STDOUT STDOUT_2
  OUT_STDERR STDERR_2
)

skymp_execute_process(
  EXECUTABLE_PATH ${bin}/main3.exe
  CPPCOV ON
  CPPCOV_PATH ${OPENCPPCOV_DIR}
  CPPCOV_OUTPUT_DIRECTORY ${bin}/cppcov_out
  CPPCOV_TAG ${CPPCOV_TAG}
  OUT_EXIT_CODE EXIT_CODE_3
  OUT_STDOUT STDOUT_3
  OUT_STDERR STDERR_3
)

if (NOT "${EXIT_CODE_1}" STREQUAL "123")
  message(FATAL_ERROR "Wrong main1.exe exit code \n'${EXIT_CODE_1}'")
endif()

if (NOT "${EXIT_CODE_2}" STREQUAL "321")
  message(FATAL_ERROR "Wrong main2.exe exit code \n'${EXIT_CODE_2}'")
endif()

if (NOT "${EXIT_CODE_3}" STREQUAL "322")
  message(FATAL_ERROR "Wrong main3.exe exit code \n'${EXIT_CODE_3}'")
endif()

foreach(i 1 2 3)
  if (NOT "${STDOUT_${i}}" STREQUAL "")
    message(FATAL_ERROR "Unexpected main${i}.exe output \n'${STDOUT}'")
  endif()
  if ("${STDERR_${i}}" STREQUAL "")
    message(FATAL_ERROR "Stderr can't be empty when CPPCOV is ON")
  endif()
  if(NOT EXISTS ${bin}/cppcov_out/Modules/main${i}/main${i}.cpp.html)
    message(FATAL_ERROR "Coverage report for main${i}.exe doesn't exist")
  endif()
endforeach()

if(NOT EXISTS ${bin}/cppcov_out/LastCoverageResults.log)
  message(FATAL_ERROR "LastCoverageResults.txt doesn't exist")
endif()
