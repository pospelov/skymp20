set(tests
  bad_cppcov_path
  bad_exe_path
  multiple_cppcov
  single_attach_cppcov
  single_attach_default
  single_cppcov
  single_default
)
foreach(test ${tests})
  add_test(
    NAME aaa_sep_${test}
    COMMAND ${CMAKE_COMMAND}
    -DOPENCPPCOV_DIR=${OPENCPPCOV_DIR}
    -DSKYRIM_DIR=${SKYRIM_DIR}
    -DPLUGIN_PATH=$<TARGET_FILE:fooplugin>
    -DMY_SOURCE_DIR=${CMAKE_CURRENT_SOURCE_DIR}
    -P ${CMAKE_CURRENT_LIST_DIR}/skymp_execute_process/${test}/test_run.cmake
  )
endforeach()

set_tests_properties(aaa_sep_bad_cppcov_path PROPERTIES WILL_FAIL TRUE)
set_tests_properties(aaa_sep_bad_exe_path PROPERTIES WILL_FAIL TRUE)
