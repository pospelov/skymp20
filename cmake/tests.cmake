add_executable(unit "src/tests_unit/unit_main.cpp" ".clang-format")
target_link_libraries(unit PRIVATE foo Catch2::Catch2)
target_include_directories(unit PRIVATE "src")
link_and_include_viet(unit)

enable_testing()
include(cmake/tests_unit.cmake)
include(cmake/tests_skyrim.cmake)
include(cmake/tests_vm.cmake)
