function(get_test_mongo_uri)
  set(options)
  set(oneValueArgs OUTPUT_VARIABLE AUTH_DATA)
  set(multiValueArgs)
  cmake_parse_arguments(A
    "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN}
  )
  set(res "mongodb://${A_AUTH_DATA}@skymptestcluster-shard-00-00-tsfqo.mongodb")
  string(APPEND res ".net:27017,skymptestcluster-shard-00-01-tsfqo.mongodb.net")
  string(APPEND res ":27017,skymptestcluster-shard-00-02-tsfqo.mongodb.net")
  string(APPEND res ":27017/test?ssl=true&replicaSet=SkympTestCluster-shard")
  string(APPEND res "-0&authSource=admin&retryWrites=true&w=majority")
  set(${A_OUTPUT_VARIABLE} ${res} PARENT_SCOPE)
endfunction()
