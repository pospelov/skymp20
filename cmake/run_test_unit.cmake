include(${CMAKE_CURRENT_LIST_DIR}/skymp_execute_process.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/get_test_mongo_uri.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/determine_cppcov_tag.cmake)

if(NOT TEST_MONGO_AUTH_DATA EQUAL "NONE")
  get_test_mongo_uri(
    AUTH_DATA ${TEST_MONGO_AUTH_DATA}
    OUTPUT_VARIABLE TEST_MONGO_URI
  )
  set(ENV{TEST_MONGO_URI} ${TEST_MONGO_URI})
endif()

determine_cppcov_tag(OUTPUT_VARIABLE tag)

skymp_execute_process(
  EXECUTABLE_PATH ${EXE_PATH}
  CPPCOV ${CPPCOV}
  CPPCOV_TAG ${tag}
  CPPCOV_PATH ${OPENCPPCOV_DIR}
  CPPCOV_MODULES *
  CPPCOV_SOURCES *
  CPPCOV_OUTPUT_DIRECTORY ${COVERAGE_HTML_OUT_DIR}
  OUT_EXIT_CODE EXIT_CODE
  OUT_STDOUT STDOUT
)

if(NOT "${EXIT_CODE}" STREQUAL "0")
  message(FATAL_ERROR "Bad exit status ${EXIT_CODE}")
endif()

execute_process(COMMAND ${EXE_PATH}
  RESULT_VARIABLE res
  OUTPUT_VARIABLE out
)

message("${out}")
