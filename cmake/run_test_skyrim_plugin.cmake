include(${CMAKE_CURRENT_LIST_DIR}/delete_skyrim_plugins.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/launch_skyrim.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/skymp_execute_process.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/determine_cppcov_tag.cmake)

launch_skyrim(
  SKYRIM_DIR ${SKYRIM_DIR}
  PLUGIN_PATH ${PLUGIN_PATH}
  TEST_NAME ${TEST_NAME}
  MO2_DIR ${MO2_DIR}
  MO2_ROOT_DIR ${MO2_ROOT_DIR}
  MO2_STARTER ${MO2_STARTER}
)

determine_cppcov_tag(OUTPUT_VARIABLE tag)

skymp_execute_process(
  EXECUTABLE_PATH ${SKYRIM_DIR}/TESV.exe
  ATTACH TRUE
  CPPCOV ${CPPCOV}
  CPPCOV_TAG ${tag}
  CPPCOV_PATH ${OPENCPPCOV_DIR}
  CPPCOV_MODULES *${PLUGIN_NAME}*
  CPPCOV_SOURCES
    ${MY_SOURCE_DIR}\\src\\savefile\\*
    ${MY_SOURCE_DIR}\\src\\skyrim_plugin_base\\utils\\*
  CPPCOV_OUTPUT_DIRECTORY ${COVERAGE_HTML_OUT_DIR}
  OUT_EXIT_CODE EXIT_CODE
  OUT_STDOUT STDOUT
)

if (NOT ${EXIT_CODE} STREQUAL 0)

  set(p ${SKYRIM_DIR}/_ctest_temp/${TEST_NAME}.txt)
  if (EXISTS ${p})
    file(READ ${p} data)

    message("\n")
    message(STATUS "${TEST_NAME}.txt:")
    message(${data})
    message("\n")
  endif()

  message(FATAL_ERROR "Bad exit code: ${EXIT_CODE}")
endif()
