macro(add_skyrim_test TEST_NAME)
  set(cov ${CMAKE_CURRENT_BINARY_DIR}/__coverage)
  file(REMOVE_RECURSE ${cov})
  add_test(
    NAME test_skyrim_plugin_${TEST_NAME}
    COMMAND ${CMAKE_COMMAND}
      -DSKYRIM_DIR=${SKYRIM_DIR}
      -DPLUGIN_PATH=$<TARGET_FILE:fooplugin>
      -DMY_SOURCE_DIR=${CMAKE_CURRENT_SOURCE_DIR}
      -DCOVERAGE_HTML_OUT_DIR=${cov}
      -DOPENCPPCOV_DIR=${OPENCPPCOV_DIR}
      -DTEST_NAME=${TEST_NAME}
      -DCPPCOV=${CPPCOV}
      -DMO2_DIR=${MO2_DIR}
      -DMO2_ROOT_DIR=${MO2_ROOT_DIR}
      -DMO2_STARTER=$<TARGET_FILE:mo_starter>
      -P ${CMAKE_CURRENT_LIST_DIR}/run_test_skyrim_plugin.cmake
  )
endmacro()

function(add_skyrim_tests)
  set(prefix "test_skyrim_plugin_")

  file(GLOB tests "${CMAKE_CURRENT_SOURCE_DIR}/src/fooplugin/tests/*.cpp")
  foreach(test ${tests})
    file(READ ${test} data)
    string(REPLACE "\n" ";" data ${data})
    foreach(line ${data})
      if (${line} MATCHES "^SKYRIM_TEST_CASE(.*)$")
        string(REPLACE "SKYRIM_TEST_CASE(" "" line ${line})
        string(REPLACE ")" "" line ${line})
        list(APPEND test_names ${line})
      endif()
    endforeach()
  endforeach()

  foreach(test_name ${test_names})
    add_skyrim_test(${test_name})
    message(STATUS "Adding test '${prefix}${test_name}'")
  endforeach()
endfunction()

add_skyrim_tests()
set_tests_properties(test_skyrim_plugin_BrokenTest PROPERTIES WILL_FAIL true)
