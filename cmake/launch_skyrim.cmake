function(launch_skyrim)
  set(options)
  set(oneValueArgs SKYRIM_DIR PLUGIN_PATH TEST_NAME MO2_DIR MO2_ROOT_DIR MO2_STARTER)
  set(multiValueArgs)
  cmake_parse_arguments(A
    "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN}
  )

  foreach(a SKYRIM_DIR PLUGIN_PATH TEST_NAME MO2_DIR MO2_ROOT_DIR MO2_STARTER)
    if (NOT A_${a})
      message(FATAL_ERROR "Missing arguments: ${a}")
    endif()
  endforeach()

  delete_skyrim_plugins(
    DIRECTORY ${A_MO2_DIR}
    IGNORE "OneTweak.dll" "SkyrimSouls.dll" "add_dll_directory_search_skymp_client.dll"
  )

  get_filename_component(PLUGIN_NAME ${A_PLUGIN_PATH} NAME)
  message(STATUS "Installing ${PLUGIN_NAME}")
  file(COPY ${A_PLUGIN_PATH} DESTINATION "${A_MO2_DIR}/SKYRIM ROYALE/skse/plugins")

  file(REMOVE_RECURSE "${A_SKYRIM_DIR}/_ctest_temp")

  if (${A_TEST_NAME} STREQUAL "")
    message(FATAL_ERROR "Invalid test name: ${A_TEST_NAME}")
  endif()
  file(WRITE ${A_SKYRIM_DIR}/_ctest_temp/test_name.txt ${A_TEST_NAME})

  message(STATUS "Starting ModOrganizer.exe")
  execute_process(COMMAND "${A_MO2_STARTER}" WORKING_DIRECTORY ${MO2_ROOT_DIR})

  # It takes some time to start the Skyrim
  execute_process(COMMAND ${CMAKE_COMMAND} -E sleep 5)
endfunction()
