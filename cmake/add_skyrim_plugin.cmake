include(cmake/download_git_deps.cmake)
include(cmake/download_frida.cmake)
include(cmake/viet.cmake)

function(add_skyrim_plugin)
  set(options NO_EXE)
  set(oneValueArgs NAME MO2_DIR)
  set(multiValueArgs SOURCES)
  cmake_parse_arguments(A
    "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN}
  )

  if (NOT A_MO2_DIR)
    message(FATAL_ERROR "Missing MO2_DIR argument")
  endif()

  set(skyrim_plugin_base_INCLUDE "src")
  set(skyrim_plugin_base_TARGET skyrim_plugin_base)
  if (NOT TARGET skyrim_plugin_base)
    file(GLOB_RECURSE src "src/skyrim_plugin_base/*")
    add_library(skyrim_plugin_base STATIC ${src})
    target_include_directories(skyrim_plugin_base PRIVATE
      ${skyrim_plugin_base_INCLUDE}
      ${minhook_inc}
    )
    link_and_include_viet(skyrim_plugin_base)
  endif()

  set(dir ${CMAKE_CURRENT_BINARY_DIR}/external_skyrim_plugin)

  download_git_deps(DIRECTORY ${dir} TARGETS
    https://gitlab.com/pospelov/skse.git,36e780abea25049420b2d567ae51feb330676c87
    https://github.com/devKlausS/dxsdk.git,996cf740444ce56178e6bc32e7fbe2c8b0f40f08
    https://github.com/Pospelove/SkyrimSouls.git,a406fc720de3a38dc6869c70d01691c1e6f53c3a
  )

  set(THIRD_PARTY_DIR ${dir}/build/skse/src/skse)
  set(SKYRIM_SOULS_DIR ${dir}/build/SkyrimSouls/src/SkyrimSouls)
  set(DXSDK_DIR ${dir}/build/dxsdk/src/dxsdk)

  # frida
  if (NOT TARGET skyrim_hooks)
    if(NOT TARGET skyrim_hooks_resources)
      cmrc_add_resource_library(skyrim_hooks_resources
        ${CMAKE_SOURCE_DIR}/src/frida/hooks.js
        ALIAS skyrim_hooks_resources_alias
        NAMESPACE skyrim_hooks_resources
        WHENCE src
      )
    endif()

    download_frida()
    add_library(skyrim_hooks SHARED
      ${CMAKE_SOURCE_DIR}/src/frida/hooks.cpp
      ${CMAKE_SOURCE_DIR}/src/frida/hooks.js
    )
    target_include_directories(skyrim_hooks PRIVATE ${CMAKE_CURRENT_BINARY_DIR}/frida-gumjs ${CMAKE_CURRENT_SOURCE_DIR}/src)
    target_link_libraries(skyrim_hooks PRIVATE skyrim_hooks_resources)
    target_link_directories(skyrim_hooks PRIVATE ${CMAKE_CURRENT_BINARY_DIR}/frida-gumjs)
    set_target_properties(skyrim_hooks skyrim_hooks_resources PROPERTIES MSVC_RUNTIME_LIBRARY MultiThreaded)

    target_compile_definitions(skyrim_plugin_base PUBLIC -DHOOKS_DLL_NAME=\"skyrim_hooks.dll\")
  endif()

  # savefile
  if(NOT TARGET savefile)
    file(GLOB_RECURSE src "src/savefile/*")
    add_library(savefile STATIC ${src} ".clang-format")
    target_link_libraries(savefile PRIVATE ZLIB::ZLIB)
    set_target_properties(savefile PROPERTIES LINKER_LANGUAGE CXX)
  endif()

  # assets
  if(NOT TARGET skyrim_plugin_resources)
    set(ASSET_FILES ${CMAKE_SOURCE_DIR}/src/savefile/template.ess)
    cmrc_add_resource_library(skyrim_plugin_resources ${ASSET_FILES}
      ALIAS assets NAMESPACE skyrim_plugin_resources WHENCE src
    )
  endif()

  # SkyrimSouls
  if (NOT TARGET SkyrimSouls)
    include("${SKYRIM_SOULS_DIR}/CMakeLists.txt")
  endif()

  # ScriptDragon
  set(SD_TARGET ScriptDragon)
  set(SD_INCLUDE "${THIRD_PARTY_DIR}/scriptdragon")
  file(GLOB SD_SRC "${THIRD_PARTY_DIR}/scriptdragon/*")
  if (NOT TARGET ${SD_TARGET})
    add_library(${SD_TARGET} STATIC ${SD_SRC})
    set_property(TARGET ${SD_TARGET} APPEND_STRING PROPERTY COMPILE_FLAGS " /FI\"stdexcept\" ")
  endif()

  # common
  set(COMMON_TARGET common)
  set(COMMON_INCLUDE "${THIRD_PARTY_DIR}/common")
  file(GLOB COMMON_SRC "${THIRD_PARTY_DIR}/common/*")
  if (NOT TARGET ${COMMON_TARGET})
    add_library(${COMMON_TARGET} ${COMMON_SRC})
    set_property(TARGET ${COMMON_TARGET} APPEND_STRING PROPERTY COMPILE_FLAGS " /FI\"IPrefix.h\" ")
    target_include_directories(${COMMON_TARGET} PUBLIC "${THIRD_PARTY_DIR}" "..")
  endif()

  # SKSE
  set(SKSE_TARGET SKSE)
  set(SKSE_INCLUDE "${THIRD_PARTY_DIR}/skse")
  file(GLOB SKSE_SRC "${THIRD_PARTY_DIR}/skse/*")
  if (NOT TARGET ${SKSE_TARGET})
    add_library(${SKSE_TARGET} ${SKSE_SRC})
    set_property(TARGET ${SKSE_TARGET} APPEND_STRING PROPERTY COMPILE_FLAGS " /FI\"../common/IPrefix.h\" ")
    target_include_directories(${SKSE_TARGET} PUBLIC "${THIRD_PARTY_DIR}")
    target_compile_definitions(${SKSE_TARGET} PUBLIC RUNTIME=1 RUNTIME_VERSION=0x09200000)
  endif()

  # DirectX
  set(DXSDK_TARGET)
  set(DXSDK_INCLUDE "${DXSDK_DIR}/Include")
  set(DXSDK_LIBS
    "${DXSDK_DIR}/Lib/x86/d3d9.lib"
    "${DXSDK_DIR}/Lib/x86/D3dx9.lib"
    "${DXSDK_DIR}/Lib/x86/DxErr.lib"
  )

  # add_dll_directory_search
  add_library(add_dll_directory_search_${A_NAME} SHARED ${CMAKE_SOURCE_DIR}/src/add_dll_directory_search/main.cpp)

  add_library(${A_NAME} SHARED ${A_SOURCES})
  target_link_libraries(${A_NAME} PRIVATE
    ${mhook_lib}
    savefile
    espm
    assets
  )

  target_link_libraries(skyrim_plugin_base PRIVATE
    assets
    nlohmann_json::nlohmann_json
  )

  foreach(tgt ${A_NAME} skyrim_plugin_base)
    foreach(v SKSE COMMON SD DXSDK skyrim_plugin_base)
      target_include_directories(${tgt} PRIVATE ${${v}_INCLUDE})

      if (${v}_TARGET)
        if (NOT ${tgt} STREQUAL ${${v}_TARGET})
          target_link_libraries(${tgt} PRIVATE ${${v}_TARGET})
        endif()
      endif()

      if (${v}_LIBS)
        target_link_libraries(${tgt} PRIVATE ${${v}_LIBS})
      endif()

    endforeach()
  endforeach()


  if (NOT ${A_NO_EXE})
    add_executable(${A_NAME}_exe ${A_SOURCES})
    get_target_property(var ${A_NAME} INCLUDE_DIRECTORIES)
    target_include_directories(${A_NAME}_exe PRIVATE ${var})
    get_target_property(var ${A_NAME} LINK_LIBRARIES)
    target_link_libraries(${A_NAME}_exe PRIVATE ${var})
  endif()

  set(PACK ${CMAKE_BINARY_DIR}/pack)

  add_custom_target(${A_NAME}_pack ALL
    COMMAND ${CMAKE_COMMAND}
    -DPACK=${PACK}
    -DBINARY_DIR=${CMAKE_BINARY_DIR}
    -DSOURCE_DIR=${CMAKE_SOURCE_DIR}
    -DTARGET_FILE_NAME=$<TARGET_FILE_NAME:${A_NAME}>
    -DTARGET_PDB_FILE=$<TARGET_PDB_FILE:${A_NAME}>
    -DCEF_PATH=${CEF_PATH}
    -DTARGET_FILE_skymp_cef_renderer=$<TARGET_FILE:skymp_cef_renderer>
    -DTARGET_FILE_skymp_cef_renderer_exe=$<TARGET_FILE:skymp_cef_renderer_exe>
    -DTARGET_FILE_SkyrimSouls=$<TARGET_FILE:SkyrimSouls>
    -DCFG=$<CONFIG>
    -DMO2_DIR=${A_MO2_DIR}
    -P ${CMAKE_SOURCE_DIR}/cmake/pack_skyrim_plugin.cmake
  )

  add_dependencies(${A_NAME}_pack ${A_NAME} add_dll_directory_search_${A_NAME} skyrim_hooks skymp_cef_renderer skymp_cef_renderer_exe SkyrimSouls)
  if (NOT ${A_NO_EXE})
    add_dependencies(${A_NAME}_pack ${A_NAME}_exe)
  endif()

  set(targets_to_bin ${A_NAME} skyrim_hooks add_dll_directory_search_${A_NAME})
  if (TARGET ${A_NAME}_exe)
    list(APPEND targets_to_bin ${A_NAME}_exe)
  endif()

  set_target_properties(${targets_to_bin} PROPERTIES
    ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
    LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin/${A_NAME}"
  )

  link_and_include_viet(${A_NAME})
  if (TARGET ${A_NAME}_exe)
    link_and_include_viet(${A_NAME}_exe)
  endif()

endfunction()

function(install_skyrim_plugin)
  set(options)
  set(oneValueArgs NAME MO2_DIR)
  set(multiValueArgs)
  cmake_parse_arguments(A
    "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN}
  )

  if (NOT A_MO2_DIR)
    message(FATAL_ERROR "MO2_DIR argument is missing")
  endif()

  set(PACK ${CMAKE_BINARY_DIR}/pack/${A_NAME}/$<CONFIG>)

  add_custom_target(${A_NAME}_install ALL
    COMMAND ${CMAKE_COMMAND} -E remove_directory "${A_MO2_DIR}/SKYRIM ROYALE/SKSE/Plugins"
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${PACK} ${A_MO2_DIR}
  )
  add_dependencies(${A_NAME}_install ${A_NAME}_pack)
endfunction()
