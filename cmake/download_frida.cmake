function(download_frida)
  set(FRIDA_INSTALLER_PATH "${CMAKE_CURRENT_BINARY_DIR}/frida-gumjs/frida-gumjs-devkit.exe")
  set(FRIDA_LIB_PATH "${CMAKE_CURRENT_BINARY_DIR}/frida-gumjs/frida-gumjs.lib")
  get_filename_component(FRIDA_INSTALLER_DIR ${FRIDA_INSTALLER_PATH} DIRECTORY)

  if(NOT EXISTS ${FRIDA_LIB_PATH})
    message(STATUS "Downloading Frida")
    file(DOWNLOAD
      "https://github.com/frida/frida/releases/download/12.2.25/frida-gumjs-devkit-12.2.25-windows-x86.exe"
      ${FRIDA_INSTALLER_PATH}
    )
    execute_process(COMMAND ${FRIDA_INSTALLER_PATH} WORKING_DIRECTORY ${FRIDA_INSTALLER_DIR})
  endif()
endfunction()
