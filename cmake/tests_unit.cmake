if (NOT TEST_MONGO_AUTH_DATA)
  set(unit_mongo_auth "NONE")
else()
  set(unit_mongo_auth "${TEST_MONGO_AUTH_DATA}")
endif()

add_test(
  NAME test_unit
  COMMAND ${CMAKE_COMMAND}
    -DEXE_PATH=$<TARGET_FILE:unit>
    -DCOVERAGE_HTML_OUT_DIR=${CMAKE_CURRENT_BINARY_DIR}/__coverage
    -DOPENCPPCOV_DIR=${OPENCPPCOV_DIR}
    -DTEST_MONGO_AUTH_DATA=${unit_mongo_auth}
    -DCPPCOV=${CPPCOV}
    -P ${CMAKE_CURRENT_LIST_DIR}/run_test_unit.cmake
)

set(unit_mongo_auth)
