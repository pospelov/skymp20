function(download_git_deps)
  set(options)
  set(oneValueArgs DIRECTORY)
  set(multiValueArgs TARGETS)
  cmake_parse_arguments(A
    "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN}
  )

  set(p "${A_DIRECTORY}/CMakeLists.txt")

  file(MAKE_DIRECTORY ${A_DIRECTORY})
  file(WRITE ${p} "
    cmake_minimum_required(VERSION 3.12)
    project(externals)
    include(ExternalProject)
  ")
  
  foreach(tgt_raw ${A_TARGETS})
    string(REPLACE "," ";" tgt ${tgt_raw})
    list(GET tgt 0 url)
    list(GET tgt 1 tag)
    get_filename_component(prefix ${url} NAME_WE)
    message(STATUS "url=${url}")
    message(STATUS "prefix=${prefix}")
    message(STATUS "tag=${tag}")
    file(APPEND ${p} "
      message(STATUS \"Cloning into \${CMAKE_BINARY_DIR}/${prefix}\")
      ExternalProject_Add(${prefix}
        PREFIX \${CMAKE_BINARY_DIR}/${prefix}
        GIT_REPOSITORY ${url}
        GIT_TAG ${tag}
        CONFIGURE_COMMAND \"\"
        BUILD_COMMAND \"\"
        INSTALL_COMMAND \"\"
      )
    ")
  endforeach()

  set(bin ${A_DIRECTORY}/build)
  file(MAKE_DIRECTORY ${bin})

  execute_process(
    COMMAND ${CMAKE_COMMAND} ${A_DIRECTORY}
    WORKING_DIRECTORY ${bin}
    RESULT_VARIABLE res
  )
  if(NOT res EQUAL "0")
    message(FATAL_ERROR "Gen step error: ${res}")
  endif()

  execute_process(
    COMMAND ${CMAKE_COMMAND} --build .
    WORKING_DIRECTORY ${bin}
    RESULT_VARIABLE res
  )
  if(NOT res EQUAL "0")
    message(FATAL_ERROR "Build step error: ${res}")
  endif()
endfunction()
