include(${CMAKE_CURRENT_LIST_DIR}/skymp_execute_process.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/determine_cppcov_tag.cmake)

determine_cppcov_tag(OUTPUT_VARIABLE tag)

skymp_execute_process(
  EXECUTABLE_PATH ${VM_MAIN_PATH}
  CPPCOV ${CPPCOV}
  CPPCOV_TAG ${tag}
  CPPCOV_PATH ${OPENCPPCOV_DIR}
  CPPCOV_MODULES *
  CPPCOV_SOURCES *
  CPPCOV_EXCLUDED_SOURCES *papyrus-vm-main\\tests.h
  CPPCOV_OUTPUT_DIRECTORY ${COVERAGE_HTML_OUT_DIR}
  OUT_EXIT_CODE EXIT_CODE
  OUT_STDOUT STDOUT
)

if (NOT ${EXIT_CODE} STREQUAL "0")
  message(FATAL_ERROR "${VM_MAIN_PATH} failed with \n${EXIT_CODE}")
endif()
