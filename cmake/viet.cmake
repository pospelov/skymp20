macro(find_viet)
  include(${VIET_DIST_PATH}/viet.cmake)

  if (UNIX)
    foreach(lib ${VIET_LIBS})
      if (${lib} MATCHES ".*\\.so$")
        list(REMOVE_ITEM VIET_LIBS ${lib})
        list(APPEND VIET_LIBS_SO ${lib})
      endif()
    endforeach()

    foreach(lib ${VIET_LIBS_SO})
      message(STATUS "Ignoring VIET_LIBS: ${lib}")
    endforeach()
  endif()
endmacro()

function(link_and_include_mongocxx TARGET_NAME)
  pmm(VCPKG REVISION ${VCPKG_REVISION} REQUIRES
    mongo-cxx-driver[boost]
  )

  if (NOT WIN32)
    pmm(VCPKG REVISION ${VCPKG_REVISION} REQUIRES
      openssl
    )
  endif()

  find_package(libmongocxx REQUIRED)
  find_package(libbsoncxx REQUIRED)
  find_package(mongo-c-driver CONFIG REQUIRED)

  target_link_libraries(${TARGET_NAME} PRIVATE
    ${LIBMONGOCXX_LIBRARIES} ${LIBBSONCXX_LIBRARIES}
    ${MONGOC_LIBRARIES} ${MONGOC_STATIC_LIBRARIES})

  if (NOT WIN32)
    find_package(OpenSSL REQUIRED)
    target_link_libraries(${TARGET_NAME} PRIVATE OpenSSL::SSL OpenSSL::Crypto)
    target_link_libraries(${TARGET_NAME} PRIVATE pthread resolv rt m sasl2)
  endif()
endfunction()

function(link_and_include_viet TARGET)
  if ("${VIET_LIBS}" STREQUAL "")
    find_viet()
  endif()

  target_link_libraries(${TARGET} PRIVATE ${VIET_LIBS})
  target_include_directories(${TARGET} PRIVATE ${VIET_DIST_PATH}/include)

  link_and_include_mongocxx(${TARGET})
endfunction()
