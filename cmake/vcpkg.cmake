if(WIN32)
  set(triplet :x86-windows)
endif()

list(APPEND required catch2${triplet} zlib${triplet} nlohmann-json${triplet} sparsepp${triplet})
if (WIN32)
  list(APPEND required minhook${triplet} mhook${triplet} imgui${triplet})
endif()

pmm(VCPKG
  REVISION ${VCPKG_REVISION}
  REQUIRES ${required}
)
find_package(Catch2 REQUIRED)
find_package(ZLIB REQUIRED)
find_package(nlohmann_json REQUIRED)

if (WIN32)
  find_package(imgui CONFIG REQUIRED)

  find_library(mhook_lib_dbg mhook PATH_SUFFIXES debug REQUIRED)
  set(mhook_lib_rel ${mhook_lib_dbg})
  string(REPLACE "/debug" "" mhook_lib_rel ${mhook_lib_dbg})
  string(REPLACE "\\debug" "" mhook_lib_rel ${mhook_lib_dbg})
  set(mhook_lib optimized ${mhook_lib_rel} debug ${mhook_lib_dbg})

  # Mhook include is also required but all includes are in the same
  # Directory when installed from vcpkg so it just works
  find_file(minhook_inc NAMES MinHook.h REQUIRED)
  get_filename_component(minhook_inc ${minhook_inc} DIRECTORY)
endif()
