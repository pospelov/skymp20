function(delete_skyrim_plugins)
  set(options)
  set(oneValueArgs DIRECTORY)
  set(multiValueArgs IGNORE)
  cmake_parse_arguments(A
    "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN}
  )

  message(STATUS "Deleting Skyrim plugins")

  file(GLOB_RECURSE plugins "${A_DIRECTORY}/SKYRIM ROYALE/skse/plugins/*.dll")
  foreach(plugin ${plugins})
    get_filename_component(plugin_name ${plugin} NAME)

    set(ignored NO)
    foreach(ignored_name ${A_IGNORE})
      if (${ignored_name} STREQUAL ${plugin_name})
        set(ignored YES)
      endif()
    endforeach()

    if (${ignored} STREQUAL NO)
      message(STATUS "Deleting plugin ${plugin_name}")
      file(REMOVE ${plugin})
    else()
      message(STATUS "Skipping plugin ${plugin_name}")
    endif()

  endforeach()
endfunction()
