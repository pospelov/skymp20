/*
 * To build, set up your Release configuration like this:
 *
 * [Runtime Library]
 * Multi-threaded (/MT)
 *
 * Visit www.frida.re to learn more about Frida.
 */

#include "frida-gumjs.h"
#include <cmrc/cmrc.hpp>
#include <stdexcept>
#include <string>
#include <windows.h>

CMRC_DECLARE(skyrim_hooks_resources);

static void on_message(GumScript* script, const gchar* message, GBytes* data,
                       gpointer user_data);

static_assert(sizeof(size_t) == 4);

void Hook()
{
  // hooks_js isn't null terminated
  auto file =
    cmrc::skyrim_hooks_resources::get_filesystem().open("frida/hooks.js");
  static const std::string g_hooksSrc(file.begin(), file.size());

  GumScriptBackend* backend;
  GCancellable* cancellable = NULL;
  GError* error = NULL;
  GumScript* script;
  GMainContext* context;

  gum_init_embedded();

  backend = gum_script_backend_obtain_duk();

  script = gum_script_backend_create_sync(
    backend, "example", g_hooksSrc.data(), cancellable, &error);
  g_assert(error == NULL);

  gum_script_set_message_handler(script, on_message, NULL, NULL);

  gum_script_load_sync(script, cancellable);

  // MessageBeep (MB_ICONINFORMATION);
  Sleep(1);

  context = g_main_context_get_thread_default();
  while (g_main_context_pending(context) || 1)
    g_main_context_iteration(context, TRUE);

  gum_script_unload_sync(script, cancellable);

  g_object_unref(script);

  gum_deinit_embedded();
}

static void on_message(GumScript* script, const gchar* message, GBytes* data,
                       gpointer user_data)
{
  JsonParser* parser;
  JsonObject* root;
  const gchar* type;

  parser = json_parser_new();
  json_parser_load_from_data(parser, message, -1, NULL);
  root = json_node_get_object(json_parser_get_root(parser));

  type = json_object_get_string_member(root, "type");
  if (strcmp(type, "log") == 0) {
    const gchar* log_message;

    log_message = json_object_get_string_member(root, "payload");
    // g_print ("%s\n", log_message);
    OutputDebugStringA(log_message);
  } else {
    // g_print ("on_message: %s\n", message);
    OutputDebugStringA(message);
  }

  g_object_unref(parser);
}

extern "C" {
__declspec(dllexport) int SetUpHooks()
{
  Hook();
  return 0;
}
}
