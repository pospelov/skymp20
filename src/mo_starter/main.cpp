#include <cstring>
#include <iostream>
#include <string>

#define WINDOWS_LEAN_AND_MEAN
#include <Windows.h>

int main()
{
  std::cout << "Starting mo_starter.exe" << std::endl;

  STARTUPINFO si;
  ZeroMemory(&si, sizeof(STARTUPINFO));
  si.cb = sizeof(STARTUPINFO);

  PROCESS_INFORMATION pi;
  char cmdline[] = "ModOrganizer.exe -p Default moshortcut://Skyrim:SKSE";

  if (!CreateProcessA(NULL, cmdline, NULL, NULL, FALSE, 0, NULL, NULL, &si,
                      &pi)) {
    std::cout << cmdline << " exited with code " << GetLastError();
    abort();
  }

  CloseHandle(pi.hProcess);
  CloseHandle(pi.hThread);
}