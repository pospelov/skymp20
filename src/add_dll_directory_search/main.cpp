#include <Windows.h>
#include <cassert>
#include <filesystem>
#include <sstream>

namespace fs = std::filesystem;

namespace {
wchar_t buff[MAXSHORT];

void ChengeDllDirectoryFind()
{
  try {
    if (auto length = GetModuleFileNameW(NULL, buff, MAXSHORT)) {

      fs::path skyrimDataDirectory = fs::path(buff).parent_path() / "Data";

      auto newDllDirectory = (skyrimDataDirectory / "Multiplayer").wstring();
      SetDllDirectoryW(newDllDirectory.data());

      if (!fs::exists(newDllDirectory / fs::path("ScriptDragon.dll"))) {
        std::stringstream ss;
        ss << "It seems the path to Data/Multiplayer is not found correctly:"
           << std::endl;
        ss << fs::path(newDllDirectory);
        throw std::runtime_error(ss.str());
      }
    } else {
      std::stringstream ss;
      ss << "GetModuleFileNameW failed with code " << GetLastError();
      throw std::runtime_error(ss.str());
    }
  } catch (std::exception& e) {
    MessageBoxA(NULL, e.what(), NULL, MB_ICONERROR);
    std::exit(-1);
  }
};
}

extern "C" {

__declspec(dllexport) BOOL WINAPI
  DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
  switch (fdwReason) {
    case DLL_PROCESS_ATTACH:
      ChengeDllDirectoryFind();
      break;
    case DLL_PROCESS_DETACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
      break;
  }

  return TRUE;
}

__declspec(dllexport) bool SKSEPlugin_Query(const void* skse, void* info)
{
  return true;
}

__declspec(dllexport) bool SKSEPlugin_Load(const void* skse)
{

  return true;
}
}