#pragma once
#include <D3dx9tex.h>
#include <memory>

class SmartDXTexture
{
public:
  // Can return nullptr
  std::shared_ptr<IDirect3DTexture9> GetTexture() const noexcept;

  enum class EditRes
  {
    Success = 0,
    CreateTextureFailed = 1,
    EditTextureFailed = 2
  };

  EditRes EditFromPng(IDirect3DDevice9* device, const unsigned char* pngData,
                      size_t pngDataSize) noexcept;
  EditRes Edit(IDirect3DDevice9* device, const unsigned char* data, int width,
               int height) noexcept;
  int GetWidth() const noexcept;  // returns -1 if empty
  int GetHeight() const noexcept; // returns -1 if empty

private:
  bool EditTexture(LPDIRECT3DTEXTURE9 texture, const void* buffer, int width,
                   int height);

  std::shared_ptr<IDirect3DTexture9> tex;
  std::unique_ptr<int> pWidth, pHeight;
};