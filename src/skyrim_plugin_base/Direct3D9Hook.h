#pragma once
#include "SkyrimPlugin.h"
#include <optional>
#include <stdexcept>

namespace Direct3D9Hook {

typedef void(*OnRender)(void *ctx, void *device);

std::optional<std::runtime_error> HookDirect3D9(
  const OnRender& onRender, void *ctx) noexcept;
}