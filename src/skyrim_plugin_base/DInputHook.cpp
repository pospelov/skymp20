#include "DInputHook.h"
#include "InputListener.h"
#include "SkyrimPlugin.h"
#include <array>
#include <atomic>
#include <cassert>
#include <common/IPrefix.h>
#include <cstdint>
#include <ctime>
#include <dinput.h>
#include <memory>
#include <skse/SafeWrite.h>
#include <skse/Utilities.h>

// If you remove this, Alt+Tab kills the process.
// Please do not try to fix UB in this file. Just let it "work".
#pragma optimize("", off)

#define IMPL_DEFINE_GUID(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8)     \
  EXTERN_C const GUID name = { l, w1, w2, { b1, b2, b3, b4, b5, b6, b7, b8 } }

IMPL_DEFINE_GUID(GUID_SysMouse1, 0x6F1D2B60, 0xD5A0, 0x11CF, 0xBF, 0xC7, 0x44,
                 0x45, 0x53, 0x54, 0x00, 0x00);
IMPL_DEFINE_GUID(GUID_SysKeyboard1, 0x6F1D2B61, 0xD5A0, 0x11CF, 0xBF, 0xC7,
                 0x44, 0x45, 0x53, 0x54, 0x00, 0x00);

namespace {
std::atomic<clock_t> g_lastDangerousDown = 0;

class InputHook
{
public:
  static InputHook* GetInstance()
  {
    static InputHook TheIInputHook;
    return &TheIInputHook;
  }

  void SetListener(const InputListener& listener) { m_listener = listener; }

  bool IsInputEnabled() const noexcept
  {
    auto f = m_listener.isKeyboardEnabled;
    return !f || f();
  }

  bool IsMouseButtonsEnabled() const noexcept
  {
    auto f = m_listener.isMouseButtonsEnabled;
    return !f || f();
  }

  void ProcessKeyboardData(uint8_t* apData)
  {
    for (uint32_t idx = 0; idx < 256; idx++) {
      if (m_pressedWas[idx] != apData[idx]) {
        m_pressedWas[idx] = apData[idx];

        const bool down = apData[idx] != 0;

        if (m_listener.onKeyStateChange)
          m_listener.onKeyStateChange(idx, down);

        constexpr int keyE = 0x12;
        if (down && idx == keyE)
          g_lastDangerousDown = clock();
      }
    }
  }

  void ProcessMouseData(DIMOUSESTATE2* mouseState)
  {
    const bool enableMouseMove =
      !m_listener.isMouseMoveEnabled || m_listener.isMouseMoveEnabled();
    if (!enableMouseMove)
      mouseState->lX = mouseState->lY = mouseState->lZ = 0;

    if (mouseState->lZ != 0) {
      if (m_listener.onMouseWheelRotate)
        m_listener.onMouseWheelRotate(mouseState->lZ);

      const bool enableWheel =
        !m_listener.isWheelRotateEnabled || m_listener.isWheelRotateEnabled();
      if (!enableWheel)
        mouseState->lZ = 0;
    }

    const MouseButton mouseBtns[] = { MouseButton::Left, MouseButton::Right,
                                      MouseButton::Middle };
    for (size_t i = 0; i < std::size(mouseBtns); ++i) {
      uint8_t& state = mouseState->rgbButtons[i];
      const bool pressed = state & 0x80;
      if (pressed != m_mousePressedWas[i]) {
        m_mousePressedWas[i] = pressed;
        if (m_listener.onMouseStateChange)
          m_listener.onMouseStateChange(mouseBtns[i], pressed);
      }
    }
  }

private:
  InputHook()
  {
    m_pressedWas.fill(0);
    m_mousePressedWas.fill(false);
  }

  InputListener m_listener;
  std::array<uint8_t, 256> m_pressedWas;
  std::array<bool, 4> m_mousePressedWas;
};
}

class myDirectInputDevice : public IDirectInputDevice8A
{
public:
  myDirectInputDevice(IDirectInputDevice8A* device_, bool keyboard_,
                      std::atomic<bool>* m_simulateClickFlood_)
    : mRealDevice(device_)
    , mKeyboard(keyboard_)
    , m_simulateClickFlood(m_simulateClickFlood_)
  {
  }

  HRESULT _stdcall QueryInterface(REFIID riid, LPVOID* ppvObj)
  {
    return mRealDevice->QueryInterface(riid, ppvObj);
  }

  ULONG _stdcall AddRef(void) { return mRealDevice->AddRef(); }

  ULONG _stdcall Release(void)
  {
    ULONG count = mRealDevice->Release();

    if (count == 0) {
      delete this;
      return 0;
    }
    return count;
  }

  // IDirectInputDevice8A
  HRESULT _stdcall GetCapabilities(LPDIDEVCAPS a)
  {
    return mRealDevice->GetCapabilities(a);
  }
  HRESULT _stdcall EnumObjects(LPDIENUMDEVICEOBJECTSCALLBACKA a, LPVOID b,
                               DWORD c)
  {
    return mRealDevice->EnumObjects(a, b, c);
  }
  HRESULT _stdcall GetProperty(REFGUID a, DIPROPHEADER* b)
  {
    return mRealDevice->GetProperty(a, b);
  }
  HRESULT _stdcall SetProperty(REFGUID a, const DIPROPHEADER* b)
  {
    return mRealDevice->SetProperty(a, b);
  }
  HRESULT _stdcall Acquire(void) { return mRealDevice->Acquire(); }
  HRESULT _stdcall Unacquire(void) { return mRealDevice->Unacquire(); }

  HRESULT _stdcall GetDeviceState(DWORD outDataLen, LPVOID outData)
  {
    if (mKeyboard) {
      uint8_t rawData[256];
      HRESULT hr = mRealDevice->GetDeviceState(256, rawData);
      if (hr != DI_OK)
        return hr;

      InputHook::GetInstance()->ProcessKeyboardData(rawData);

      memcpy(outData, rawData, outDataLen < 256 ? outDataLen : 256);

      return hr;
    } else {
      HRESULT ret = mRealDevice->GetDeviceState(outDataLen, outData);

      if (m_simulateClickFlood && *m_simulateClickFlood) {
        for (int i : { 0, 1 })
          ((DIMOUSESTATE*)outData)->rgbButtons[i] = rand() % 2 ? 1 : 0;
      }

      for (int i : { 0, 1 })
        if (((DIMOUSESTATE*)outData)->rgbButtons[i])
          g_lastDangerousDown = clock();

      if (InputHook::GetInstance()->IsMouseButtonsEnabled() == false) {
        DIMOUSESTATE2 fakeMouseState;
        memcpy(&fakeMouseState, outData, outDataLen);
        for (int i = 0; i < std::size(fakeMouseState.rgbButtons); ++i) {
          fakeMouseState.rgbButtons[i] = 0;
        }
        memcpy(outData, &fakeMouseState, outDataLen);
      }

      if (ret != DI_OK)
        return ret;

      DIMOUSESTATE2* mouseState = (DIMOUSESTATE2*)outData;

      InputHook::GetInstance()->ProcessMouseData(mouseState);

      return ret;
    }
    return DI_OK;
  }

  HRESULT _stdcall GetDeviceData(DWORD dataSize, DIDEVICEOBJECTDATA* outData,
                                 DWORD* outDataLen, DWORD flags)
  {
    HRESULT ret =
      mRealDevice->GetDeviceData(dataSize, outData, outDataLen, flags);

    if (!InputHook::GetInstance()->IsInputEnabled()) {
      *outDataLen = 0;
    }

    if (mKeyboard) {
      uint8_t rawData[256];
      HRESULT hr = mRealDevice->GetDeviceState(256, rawData);
      if (hr == DI_OK) {
        InputHook::GetInstance()->ProcessKeyboardData(rawData);
        memset(rawData, 0, 256);
      }
    }

    return ret;
  }

  HRESULT _stdcall SetDataFormat(const DIDATAFORMAT* a)
  {
    return mRealDevice->SetDataFormat(a);
  }
  HRESULT _stdcall SetEventNotification(HANDLE a)
  {
    return mRealDevice->SetEventNotification(a);
  }
  HRESULT _stdcall SetCooperativeLevel(HWND a, DWORD b)
  {
    return mRealDevice->SetCooperativeLevel(a, b);
  }
  HRESULT _stdcall GetObjectInfo(LPDIDEVICEOBJECTINSTANCEA a, DWORD b, DWORD c)
  {
    return mRealDevice->GetObjectInfo(a, b, c);
  }
  HRESULT _stdcall GetDeviceInfo(LPDIDEVICEINSTANCEA a)
  {
    return mRealDevice->GetDeviceInfo(a);
  }
  HRESULT _stdcall RunControlPanel(HWND a, DWORD b)
  {
    return mRealDevice->RunControlPanel(a, b);
  }
  HRESULT _stdcall Initialize(HINSTANCE a, DWORD b, REFGUID c)
  {
    return mRealDevice->Initialize(a, b, c);
  }
  HRESULT _stdcall CreateEffect(REFGUID a, LPCDIEFFECT b,
                                LPDIRECTINPUTEFFECT* c, LPUNKNOWN d)
  {
    return mRealDevice->CreateEffect(a, b, c, d);
  }
  HRESULT _stdcall EnumEffects(LPDIENUMEFFECTSCALLBACKA a, LPVOID b, DWORD c)
  {
    return mRealDevice->EnumEffects(a, b, c);
  }
  HRESULT _stdcall GetEffectInfo(LPDIEFFECTINFOA a, REFGUID b)
  {
    return mRealDevice->GetEffectInfo(a, b);
  }
  HRESULT _stdcall GetForceFeedbackState(LPDWORD a)
  {
    return mRealDevice->GetForceFeedbackState(a);
  }
  HRESULT _stdcall SendForceFeedbackCommand(DWORD a)
  {
    return mRealDevice->SendForceFeedbackCommand(a);
  }
  HRESULT _stdcall EnumCreatedEffectObjects(
    LPDIENUMCREATEDEFFECTOBJECTSCALLBACK a, LPVOID b, DWORD c)
  {
    return mRealDevice->EnumCreatedEffectObjects(a, b, c);
  }
  HRESULT _stdcall Escape(LPDIEFFESCAPE a) { return mRealDevice->Escape(a); }
  HRESULT _stdcall Poll(void) { return mRealDevice->Poll(); }
  HRESULT _stdcall SendDeviceData(DWORD a, LPCDIDEVICEOBJECTDATA b, LPDWORD c,
                                  DWORD d)
  {
    return mRealDevice->SendDeviceData(a, b, c, d);
  }
  HRESULT _stdcall EnumEffectsInFile(LPCSTR a, LPDIENUMEFFECTSINFILECALLBACK b,
                                     LPVOID c, DWORD d)
  {
    return mRealDevice->EnumEffectsInFile(a, b, c, d);
  }
  HRESULT _stdcall WriteEffectToFile(LPCSTR a, DWORD b, LPDIFILEEFFECT c,
                                     DWORD d)
  {
    return mRealDevice->WriteEffectToFile(a, b, c, d);
  }
  HRESULT _stdcall BuildActionMap(LPDIACTIONFORMATA a, LPCSTR b, DWORD c)
  {
    return mRealDevice->BuildActionMap(a, b, c);
  }
  HRESULT _stdcall SetActionMap(LPDIACTIONFORMATA a, LPCSTR b, DWORD c)
  {
    return mRealDevice->SetActionMap(a, b, c);
  }
  HRESULT _stdcall GetImageInfo(LPDIDEVICEIMAGEINFOHEADERA a)
  {
    return mRealDevice->GetImageInfo(a);
  }

private:
  IDirectInputDevice8A* mRealDevice;
  bool mKeyboard;
  std::atomic<bool>* m_simulateClickFlood;
};

class myDirectInput : public IDirectInput8A
{
public:
  myDirectInput(IDirectInput8A* obj, std::atomic<bool>* simulateClickFlood)
    : mRealInput(obj)
    , m_simulateClickFlood(simulateClickFlood)
  {
  }

  HRESULT _stdcall QueryInterface(REFIID riid, LPVOID* ppvObj)
  {
    return mRealInput->QueryInterface(riid, ppvObj);
  }

  ULONG _stdcall AddRef(void) { return mRealInput->AddRef(); }

  ULONG _stdcall Release(void)
  {
    ULONG count = mRealInput->Release();

    if (count == 0) {
      delete this;
      return 0;
    }

    return count;
  }

  HRESULT _stdcall CreateDevice(REFGUID typeGuid,
                                IDirectInputDevice8A** device,
                                IUnknown* unused)
  {
    if (typeGuid != GUID_SysKeyboard1 && typeGuid != GUID_SysMouse1) {
      return mRealInput->CreateDevice(typeGuid, device, unused);
    } else {
      IDirectInputDevice8A* dev;

      HRESULT hr = mRealInput->CreateDevice(typeGuid, &dev, unused);
      if (hr != DI_OK)
        return hr;

      *device = new myDirectInputDevice(
        dev, (typeGuid == GUID_SysKeyboard1) != FALSE, m_simulateClickFlood);

      return hr;
    }
  }

  HRESULT _stdcall EnumDevices(DWORD a, LPDIENUMDEVICESCALLBACKA b, void* c,
                               DWORD d)
  {
    return mRealInput->EnumDevices(a, b, c, d);
  }
  HRESULT _stdcall GetDeviceStatus(REFGUID r)
  {
    return mRealInput->GetDeviceStatus(r);
  }
  HRESULT _stdcall RunControlPanel(HWND a, DWORD b)
  {
    return mRealInput->RunControlPanel(a, b);
  }
  HRESULT _stdcall Initialize(HINSTANCE a, DWORD b)
  {
    return mRealInput->Initialize(a, b);
  }
  HRESULT _stdcall FindDevice(REFGUID a, LPCSTR b, LPGUID c)
  {
    return mRealInput->FindDevice(a, b, c);
  }
  HRESULT _stdcall EnumDevicesBySemantics(LPCSTR a, LPDIACTIONFORMATA b,
                                          LPDIENUMDEVICESBYSEMANTICSCBA c,
                                          void* d, DWORD e)
  {
    return mRealInput->EnumDevicesBySemantics(a, b, c, d, e);
  }
  HRESULT _stdcall ConfigureDevices(LPDICONFIGUREDEVICESCALLBACK a,
                                    LPDICONFIGUREDEVICESPARAMSA b, DWORD c,
                                    void* d)
  {
    return mRealInput->ConfigureDevices(a, b, c, d);
  }

private:
  IDirectInput8A* mRealInput;
  ULONG mReferences;
  std::atomic<bool>* const m_simulateClickFlood;
};

static std::atomic<bool> g_simulateClickFlood = false;

typedef HRESULT(_stdcall* tDirectInput8Create)(HINSTANCE, DWORD, REFIID,
                                               LPVOID, LPUNKNOWN);
static tDirectInput8Create DirectInput8Create_RealFunc;

static HRESULT _stdcall FakeDirectInput8Create(HINSTANCE instance,
                                               DWORD version, REFIID iid,
                                               void* out, IUnknown* outer)
{
  IDirectInput8A* dinput;
  const HRESULT hr =
    DirectInput8Create_RealFunc(instance, version, iid, &dinput, outer);

  if (hr != DI_OK)
    return hr;

  *((IDirectInput8A**)out) = new myDirectInput(dinput, &g_simulateClickFlood);

  return DI_OK;
}

bool DInputHook::HookDirectInput(const InputListener& listener) noexcept
{
  const uint32_t thunkAddress =
    (uint32_t)GetIATAddr((unsigned char*)GetModuleHandle(NULL), "dinput8.dll",
                         "DirectInput8Create");
  if (!thunkAddress)
    return false;

  DirectInput8Create_RealFunc = (tDirectInput8Create) * (DWORD*)thunkAddress;
  SafeWrite32(thunkAddress, (DWORD)FakeDirectInput8Create);

  auto inputHook = InputHook::GetInstance();
  inputHook->SetListener(listener);
  return true;
}

void DInputHook::SetClickFloodActive(bool active) noexcept
{
  g_simulateClickFlood = active;
}

bool DInputHook::WasDangerousButtonDown(int ms) noexcept
{
  return int64_t(g_lastDangerousDown) > int64_t(clock()) - ms;
}