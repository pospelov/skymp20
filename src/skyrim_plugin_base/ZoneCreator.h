#pragma once
#include <common/IPrefix.h>
#include <cstdint>
#include <lib_espm/espm.h>
#include <map>
#include <memory>
#include <skse/GameData.h>
#include <skse/GameForms.h>
#include <skse/GameObjects.h>
#include <skse/GameReferences.h>
#include <skse/NiTypes.h>
#include <skyrim_plugin_base/utils/FindClosestRef.h>
#include <vector>

#include <lib_espm/Loader.h>

class FireWallRef
{
public:
  FireWallRef(TESForm* base);

  ~FireWallRef();

  TESObjectREFR* GetRef() const noexcept;
  void Enable(bool abFadeIn) const noexcept;

  FireWallRef(const FireWallRef&) = delete;
  FireWallRef& operator=(const FireWallRef&) = delete;

private:
  uint32_t m_refId = 0;
  TESForm* m_base = nullptr;
};

class ZoneCreator
{
public:
  struct FireWallInfo
  {
    NiPoint3 position, lastPos;
    NiPoint3 rotation;
    std::shared_ptr<FireWallRef> wallRef;

    void FallIntoPlace();
    void StartMoving();
  };

  struct FireWallState
  {
    NiPoint3 previousCenter;
    float previousRadius = -1;

    bool isPlayerInside = true;
    std::vector<FireWallInfo> walls;
    void StartMoving();
    NiPoint3 GetPositionFirstWall();
    enum
    {
      WaterHeight = 247,
    };
  };

  static void Run(FireWallState& wallsState, float radius,
                  const NiPoint3& centrPos, TESForm* base,
                  const espm::CombineBrowser& espmBrowser, TESWeather* weather = nullptr);

private:
  struct WallPosition
  {
    NiPoint3 pos;
    NiPoint3 rot;
  };

  static void CreateFireWalls(FireWallState& state, float radius,
                              const NiPoint3& centrPos);

  static void CreateNewPositionForFireWalls(FireWallState& wallsState,
                                            float radius, float previousRadius,
                                            const NiPoint3& centrPos);

  static NiPoint3 GetPositionPointOnCircle(float radius,
                                           const NiPoint3& centrPos,
                                           float degrees);

  static NiPoint3 GetRotationPointOnCircle(float x, float y,
                                           const NiPoint3& centrPos);

  static std::vector<WallPosition> GetRequiredFireWalls(const NiPoint3& center,
                                                        float radius);

  static bool PointInCircle(const NiPoint3& point, float radius,
                            const NiPoint3& centrPos);
  static float GetCurrentRadius(const NiPoint3& centr, const NiPoint3& point);

  static void MoveToNavMeshZ(FireWallState& wallsState,
                             const espm::CombineBrowser& espmBrowser,
                             uint32_t worldSpaceId);
};