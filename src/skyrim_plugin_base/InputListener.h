#pragma once
#include <cstdint>
#include <functional>

enum class MouseButton
{
  Left,
  Right,
  Middle
};

struct InputListener
{
  std::function<void(uint8_t dxKeyCode, bool down)> onKeyStateChange;
  std::function<void(MouseButton dxKeyCode, bool down)> onMouseStateChange;
  std::function<void(float delta)> onMouseWheelRotate;

  std::function<bool()> isMouseMoveEnabled, isMouseButtonsEnabled,
    isWheelRotateEnabled, isKeyboardEnabled;
};