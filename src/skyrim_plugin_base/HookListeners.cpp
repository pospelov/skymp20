#include "HookListeners.h"

std::vector<HookListeners::SendAnimationEventFn>&
HookListeners::SendAnimEvent()
{
  static std::vector<SendAnimationEventFn> g_listeners;
  return g_listeners;
}

std::vector<HookListeners::SendAnimationEventLeaveFn>&
HookListeners::SendAnimEventLeave()
{
  static std::vector<SendAnimationEventLeaveFn> g_listeners;
  return g_listeners;
}