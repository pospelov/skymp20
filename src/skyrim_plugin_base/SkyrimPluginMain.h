// Include this in your plugin's main file. You need to define
// SKYRIM_PLUGIN_NAME before including.

#include "SkyrimPlugin.h"

// This function must be defined in your plugin's file
namespace SkyrimPlugin {
SkyrimPlugin::Plugin* GetPlugin();
}

extern "C" {
__declspec(dllexport) bool SKSEPlugin_Query(const SKSEInterface* skse,
                                            PluginInfo* info)
{
  return SkyrimPlugin::GetPlugin()->Query(skse, info, SKYRIM_PLUGIN_NAME);
}

__declspec(dllexport) bool SKSEPlugin_Load(const SKSEInterface* skse)
{
  return SkyrimPlugin::GetPlugin()->Load(skse);
}

__declspec(dllexport) void main()
{
  return SkyrimPlugin::GetPlugin()->SDMain();
}

__declspec(dllexport) BOOL WINAPI
  DllMain(HMODULE hModule, DWORD fdwReason, LPVOID lpReserved)
{
  switch (fdwReason) {
    case DLL_PROCESS_ATTACH:
      SkyrimPlugin::GetPlugin()->DllMain(hModule);
      break;
  }
  return TRUE;
}

__declspec(dllexport) void SKYMP2_Print(const char* text)
{
  SkyrimPlugin::GetPlugin()->Print(text);
}

__declspec(dllexport) const char* SKYMP2_SetWeaponDrawn(const char* jDump)
{
  return SkyrimPlugin::GetPlugin()->SetWeaponDrawn(jDump);
}

__declspec(dllexport) void* SKYMP2_SendAnimationEvent(TESObjectREFR* refr,
                                                      void* fsAnimEventName)
{
  return SkyrimPlugin::GetPlugin()->SendAnimationEvent(refr, fsAnimEventName);
}

__declspec(dllexport) void SKYMP2_SendAnimationEventLeave(
  TESObjectREFR* refr, void* fsAnimEventName, uint32_t returnValue)
{
  return SkyrimPlugin::GetPlugin()->SendAnimationEventLeave(
    refr, fsAnimEventName, returnValue);
}

__declspec(dllexport) void SKYMP2_OnEquipItem(void* actor, void* tesForm,
                                              bool equip)
{
}

__declspec(dllexport) void SKYMP2_Render()
{
  return SkyrimPlugin::GetPlugin()->Render();
}
};
