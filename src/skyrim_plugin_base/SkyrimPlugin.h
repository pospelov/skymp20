#pragma once
#include "HitEvent.h"
#include "InputListener.h"
#include "common/IPrefix.h"
#include "scriptdragon/plugin.h"
#include "skse/PluginAPI.h"
#include "skse/skse_version.h"
#include <cstdint>
#include <memory>

namespace espm {
class CombineBrowser;
}

class TESObjectREFR;

namespace SkyrimPlugin {
class Plugin
{
  friend void GOnRender(void* ctx, void* device);

public:
  Plugin();

  virtual void HelperThreadMain() = 0;
  virtual void GameThreadMain() = 0;
  virtual InputListener CreateInputListener() = 0;
  virtual void OnRender(void* device) = 0;
  virtual void OnHit(const HitEvent& hitEvent){};

  void DrawChromium(void* device);
  const espm::CombineBrowser& GetEspmBrowser();

  void DllMain(HMODULE h);
  bool Query(const SKSEInterface* skse, PluginInfo* info,
             const char* pluginName);
  bool Load(const SKSEInterface* skse);
  void Print(const char* text);
  void Render();
  const char* SetWeaponDrawn(const char* jsonDump);
  void* SendAnimationEvent(TESObjectREFR* refr, void* animEventName);
  void SendAnimationEventLeave(TESObjectREFR* refr, void* animEventName,
                               uint32_t returnValue);
  void SDMain();
  void* GetCefFunctions();

private:
  void PerformChromiumInputs();

  struct Impl;
  std::shared_ptr<Impl> pImpl;
  void OnPaint(const void* buf, size_t size);
};
}