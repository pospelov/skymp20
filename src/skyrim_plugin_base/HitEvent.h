#pragma once
#include <cstdint>

struct HitEvent
{
  uint32_t casterId = 0;
  uint32_t targetId = 0;
  uint32_t sourceFormId = 0;
  uint32_t projectileFormId = 0;
  bool powerAttack = false;
  bool sneakAttack = false;
  bool bash = false;
  bool blocked = false;
};