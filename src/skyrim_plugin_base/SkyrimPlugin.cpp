#include "SkyrimPlugin.h"
#include "DInputHook.h"
#include "Direct3D9Hook.h"
#include <DxErr.h>
#include <atomic>
#include <cassert>
#include <imgui.h>
#include <lib_espm/espm.h>
#include <nlohmann/json.hpp>
#include <process.h> // _beginthread
#include <skse/CommandTable.h>
#include <skse/GameData.h>
#include <skse/GameMenus.h>
#include <skse/Hooks_SaveLoad.h>
#include <skyrim_plugin_base/CefFunctions.h>
#include <skyrim_plugin_base/HitEventSink.h>
#include <skyrim_plugin_base/HookListeners.h>
#include <skyrim_plugin_base/IInputListener.h>
#include <skyrim_plugin_base/InputConverter.h>
#include <skyrim_plugin_base/InputEmitter.h>
#include <skyrim_plugin_base/RemoveSaveFile.h>
#include <skyrim_plugin_base/SmartDxTexture.h>
#include <skyrim_plugin_base/utils/GetMovement.h>
#include <skyrim_plugin_base/utils/Misc.h>
#include <skyrim_plugin_base/utils/SetWeaponDrawn.h>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

#include <lib_espm/Loader.h>

namespace {

std::shared_ptr<espm::Loader> LoadEspAndEsm()
{
  auto dataHandler = DataHandler::GetSingleton();

  if (!dataHandler)
    throw std::runtime_error("dataHandler was nullptr");

  std::vector<std::filesystem::path> newPlugins;

  for (size_t i = 0; i < dataHandler->modList.loadedModCount; ++i)
    newPlugins.push_back(
      std::string(dataHandler->modList.loadedMods[i]->name));

  std::shared_ptr<espm::Loader> loader(new espm::Loader(
    std::filesystem::current_path() / "Data", newPlugins, nullptr));

  return loader;
}

// Exceptions do not work in render thread so we use assertions
auto GetWindowSize(HWND hwnd)
{
  RECT rect;
  if (!hwnd) {
    assert(0);
    return std::pair<float, float>();
  }
  GetWindowRect(hwnd, &rect);
  const auto width = rect.right - rect.left;
  const auto height = rect.bottom - rect.top;
  auto res = std::pair<float, float>((float)width, (float)height);
  if (res.first == 0 || res.second == 0) {
    assert(0);
  }
  return res;
}

static void ExitWithError(const char* e)
{
  MessageBoxA(0, e, "Unhandled exception", MB_ICONERROR);
  ExitProcess(-1);
}

void StartFrida(HMODULE skymp2hooks)
{
  _beginthread(
    [](void* skymp2hooks) {
      typedef int(SetUpHooks)(void);
      auto setUpHooks = (SetUpHooks*)GetProcAddress(
        reinterpret_cast<HMODULE>(skymp2hooks), "SetUpHooks");
      setUpHooks();
    },
    1024 * 1024, skymp2hooks);
}
}

struct SkyrimPlugin::Plugin::Impl
{
  std::atomic<float> mouseWheel;
  std::atomic<bool> isOverlayActive = true;

  HMODULE hModule = nullptr;
  void* device = nullptr;
  std::list<clock_t> renderMoments;

  std::unique_ptr<CefFunctions> cefFunctions;

  struct Chrome
  {
    IDirect3DDevice9* chromiumDevice = nullptr;
    std::optional<std::pair<float, float>> displaySize;
    SmartDXTexture chromiumTexture;
    void* chromiumSprite = nullptr; // LPD3DXSPRITE
    bool loadUrlStarted = false;
  };
  std::unique_ptr<Chrome> chrome;
  std::recursive_mutex chromeM;

  struct ChromeRender
  {
    std::unique_ptr<IInputListener> inputListener;
    InputEmitter inputEmitter;
    InputConverter inputConverter;
    bool switchLangDownWas = false;
  } chromeRender;

  std::shared_ptr<espm::Loader> espmLoader;
};

void SkyrimPlugin::GOnRender(void* ctx, void* device)
{
  auto plugin = reinterpret_cast<SkyrimPlugin::Plugin*>(ctx);
  plugin->pImpl->device = device;

  plugin->pImpl->renderMoments.push_back(clock());
  while (plugin->pImpl->renderMoments.size() > 100)
    plugin->pImpl->renderMoments.pop_front();

  int fps = 0;

  if (plugin->pImpl->renderMoments.size() >= 100) {
    float totalSeconds = float(plugin->pImpl->renderMoments.back() -
                               plugin->pImpl->renderMoments.front()) /
      CLOCKS_PER_SEC;
    float framePerMs = plugin->pImpl->renderMoments.size() / totalSeconds;
    fps = int(framePerMs);
  }

  static int lastFps = 0;
  static clock_t lastTime = 0;
  if (lastFps != fps && clock() - lastTime > CLOCKS_PER_SEC) {
    lastFps = fps;
    lastTime = clock();
    Console_Print("%d", fps);
  }
}

inline void StartChromium(CefFunctions cefFunctions)
{
  std::thread thr([=] {
    assert(cefFunctions.cefMain);
    if (!cefFunctions.cefMain)
      return;
    cefFunctions.cefMain();
    while (1) {
      assert(cefFunctions.cefTick);
      Sleep(1);
      cefFunctions.cefTick();
    }
  });
  thr.detach();
}

SkyrimPlugin::Plugin::Plugin()
{
  pImpl.reset(new Impl);
  pImpl->chrome.reset(new Impl::Chrome);
}

void SkyrimPlugin::Plugin::DllMain(HMODULE h)
{
  pImpl->hModule = h;

  auto list = this->CreateInputListener();

  list.onMouseWheelRotate = [list, this](float delta) {
    pImpl->mouseWheel = pImpl->mouseWheel + delta;
    if (list.onMouseWheelRotate)
      list.onMouseWheelRotate(delta);
  };

  bool success = DInputHook::HookDirectInput(list);
  if (!success)
    return ExitWithError("HookDirectInput failed");

  auto error = Direct3D9Hook::HookDirect3D9(GOnRender, this);
  if (error)
    return ExitWithError(error->what());
}

bool SkyrimPlugin::Plugin::Query(const SKSEInterface* skse, PluginInfo* info,
                                 const char* pluginName)
{
  info->infoVersion = PluginInfo::kInfoVersion;
  info->name = pluginName;
  info->version = 1;

  if (skse->isEditor) {
    _MESSAGE("loaded in editor, marking as incompatible");
    return false;
  }
  if (skse->runtimeVersion != RUNTIME_VERSION_1_9_32_0) {
    _MESSAGE("unsupported runtime version %08X", skse->runtimeVersion);
    return false;
  }
  return true;
}

bool SkyrimPlugin::Plugin::Load(const SKSEInterface* skse)
{
  _MESSAGE("load");

  try {
    auto skymp2hooks = LoadLibraryA(HOOKS_DLL_NAME);
    if (!skymp2hooks)
      throw std::runtime_error("LoadLibraryA " HOOKS_DLL_NAME " failed");
    StartFrida(skymp2hooks);

    sd::DragonPluginInit(pImpl->hModule);

    if (!pImpl->cefFunctions) {
      pImpl->cefFunctions.reset(new CefFunctions(CefFunctions::Setup(
        [this](auto buf, auto size) { this->OnPaint(buf, size); })));
    }
  } catch (std::exception& e) {
    MessageBoxA(0, e.what(), "SkyMP startup error", MB_ICONERROR);
    return false;
  }
  _beginthread(
    [](void* this_) {
      static const auto fsMainMenu = new BSFixedString("Main Menu");

      while (1) {
        if (auto mm = MenuManager::GetSingleton())
          if (mm->IsMenuOpen(fsMainMenu))
            break;
        Sleep(1);
      }

      Hooks_SaveLoad_Commit();

      auto plugin = reinterpret_cast<Plugin*>(this_);

      HitEventSink::Install([plugin](const HitEvent& e) { plugin->OnHit(e); });

      plugin->pImpl->espmLoader = LoadEspAndEsm();
      StartChromium(
        *reinterpret_cast<CefFunctions*>(plugin->GetCefFunctions()));
      plugin->HelperThreadMain();
    },
    1024 * 1024, this);
  return true;
}

void SkyrimPlugin::Plugin::Print(const char* text)
{
  Console_Print("%s", text);
}

void SkyrimPlugin::Plugin::Render()
{
  if (pImpl->device)
    OnRender(pImpl->device);
}

const char* SkyrimPlugin::Plugin::SetWeaponDrawn(const char* jsonDump)
{
  return SetWeaponDrawn::RunHookHandler(jsonDump);
}

void* SkyrimPlugin::Plugin::SendAnimationEvent(TESObjectREFR* refr,
                                               void* animEventName)
{
  for (auto& hook : HookListeners::SendAnimEvent()) {
    auto newAnimEventName = hook(refr, animEventName);
    if (newAnimEventName != animEventName) {
      if (!newAnimEventName)
        newAnimEventName = Misc::FixedStr("");
      return newAnimEventName;
    }
  }
  return animEventName;
}

void SkyrimPlugin::Plugin::SendAnimationEventLeave(TESObjectREFR* refr,
                                                   void* animEventName,
                                                   uint32_t returnValue)
{
  for (auto& hook : HookListeners::SendAnimEventLeave()) {
    hook(refr, animEventName, returnValue);
  }
}

void SkyrimPlugin::Plugin::SDMain()
{
  try {
    RemoveEss::Run();
  } catch (std::exception& e) {
    return ExitWithError(e.what());
  };

  GameThreadMain();
}

void* SkyrimPlugin::Plugin::GetCefFunctions()
{
  CefFunctions* res = pImpl->cefFunctions.get();
  return res;
}

void SkyrimPlugin::Plugin::DrawChromium(void* device)
{
  if (!ImGui::IsWindowFocused(ImGuiFocusedFlags_AnyWindow))
    this->PerformChromiumInputs();

  std::unique_lock l(pImpl->chromeM);

  if (!pImpl->chrome)
    return;

  if (!pImpl->chrome->loadUrlStarted) {
    assert(pImpl->cefFunctions);
    if (pImpl->cefFunctions)
      pImpl->cefFunctions->cefLoadUrl(L"http://sirokos.com/GPUAsteroids/");
    pImpl->chrome->loadUrlStarted = true;
  }

  pImpl->chrome->chromiumDevice = reinterpret_cast<IDirect3DDevice9*>(device);

  static auto wnd = FindWindowA("Skyrim", "Skyrim");
  if (!wnd) {
    assert(0);
  } else {

    auto size = GetWindowSize(wnd);
    pImpl->chrome->displaySize = size;
  }

  if (pImpl->chrome->chromiumSprite == nullptr) {
    D3DXCreateSprite(pImpl->chrome->chromiumDevice,
                     (LPD3DXSPRITE*)&pImpl->chrome->chromiumSprite);
    assert(pImpl->chrome->chromiumSprite);
  }

  if (pImpl->chrome->chromiumSprite) {
    auto sprite =
      reinterpret_cast<LPD3DXSPRITE>(pImpl->chrome->chromiumSprite);
    sprite->Begin(D3DXSPRITE_ALPHABLEND);
    const D3DXVECTOR3 position(0, 0, 0);
    D3DCOLOR color = -1;
    const auto tex = pImpl->chrome->chromiumTexture.GetTexture();
    // tex == nullptr means ChromiumOnPaint didn't called yet
    if (tex) {
      const auto hr = (sprite->Draw(tex.get(), NULL, NULL, &position, color));
      if (FAILED(hr)) {
        char errbuf[1024] = { 0 };
        sprintf_s(errbuf, "Error: %s error description: %s\n",
                  DXGetErrorString(hr), DXGetErrorDescription(hr));
        assert(0);
      }
    }
    sprite->End();
  }
}

const espm::CombineBrowser& SkyrimPlugin::Plugin::GetEspmBrowser()
{
  if (!pImpl->espmLoader)
    throw std::runtime_error("espmLoader was nullptr");

  return pImpl->espmLoader->GetBrowser();
}

void SkyrimPlugin::Plugin::OnPaint(const void* buf, size_t size)
{
  auto& i = pImpl;

  std::unique_lock l(i->chromeM);

  if (!i->chrome)
    return;
  if (!i->chrome->chromiumDevice)
    return;
  if (!i->chrome->displaySize)
    return;

  const auto res = i->chrome->chromiumTexture.Edit(
    i->chrome->chromiumDevice, (uint8_t*)buf,
    (int)i->chrome->displaySize->first, (int)i->chrome->displaySize->second);
  assert(res == SmartDXTexture::EditRes::Success);
  if (res != SmartDXTexture::EditRes::Success) {
    i->chrome.reset();
  }
};

void SkyrimPlugin::Plugin::PerformChromiumInputs()
{
  auto& ch = pImpl->chromeRender;

  if (!pImpl->chromeRender.inputListener) {
    class CefInputListener : public IInputListener
    {
    public:
      CefInputListener(CefFunctions* funcs_,
                       std::atomic<bool>* isOverlayActive_)
        : funcs(*funcs_)
        , isOverlayActive(*isOverlayActive_)
      {
      }

      void OnKeyStateChange(uint8_t vkCode, bool down) noexcept override
      {
        if (this->funcs.cefOnKeyStateChange)
          this->funcs.cefOnKeyStateChange(vkCode, down);
      }

      void OnChar(wchar_t ch) noexcept override
      {
        if (this->funcs.cefOnChar && this->isOverlayActive)
          this->funcs.cefOnChar(ch);
      }

      void OnMouseWheel(int32_t delta) noexcept override
      {
        if (this->funcs.cefOnMouseWheel)
          this->funcs.cefOnMouseWheel(delta);
      }

      void OnMouseMove(float newX, float newY) noexcept override
      {
        if (this->funcs.cefOnMouseMove)
          this->funcs.cefOnMouseMove(newX, newY);
      }

      void OnMouseStateChange(float clickX, float clickY,
                              MouseButton mouseButton,
                              bool down) noexcept override
      {
        if (mouseButton != MouseButton::Left)
          return;

        if (this->funcs.cefOnMouseStateChange)
          this->funcs.cefOnMouseStateChange(clickX, clickY,
                                            uint8_t(mouseButton), down);
      }

      CefFunctions& funcs;
      std::atomic<bool>& isOverlayActive;
    };
    ch.inputListener.reset(new CefInputListener(pImpl->cefFunctions.get(),
                                                &pImpl->isOverlayActive));
  }

  InputEmitter::Data data;
  data.mouseDown = ImGui::GetIO().MouseDown;
  data.mouseWheelAbsolute = pImpl->mouseWheel;
  data.mouseX = ImGui::GetIO().MousePos.x;
  data.mouseY = ImGui::GetIO().MousePos.y;
  data.vkCodesDown = ImGui::GetIO().KeysDown;

  ch.inputEmitter.Tick(*ch.inputListener, data, ch.inputConverter);

  // Switch keyboard layout if need
  const bool switchLangDown =
    (ImGui::IsKeyDown(VK_CONTROL) || ImGui::IsKeyDown(VK_MENU)) &&
    ImGui::IsKeyDown(VK_SHIFT);
  const bool switchLangPressed = switchLangDown && !ch.switchLangDownWas;
  ch.switchLangDownWas = switchLangDown;
  if (switchLangPressed) {
    ch.inputConverter.SwitchLayout();
  }
}