#include "RemoveSaveFile.h"
#include <skyrim_plugin_base\utils\LoadGame.h>

void RemoveEss::Run() noexcept
{
  std::filesystem::path path =
    LoadGame::GetPathToMyDocuments() + L"\\My Games\\Skyrim\\Saves\\";

  for (auto& file : std::filesystem::directory_iterator(path)) {
    if (file.path().filename().generic_string().find("SKYMP2020-") !=
        std::string::npos)
      std::filesystem::remove(file);
  }
}
