#pragma once
#include <vector>

class TESObjectREFR;

namespace HookListeners {
typedef void* (*SendAnimationEventFn)(TESObjectREFR* refr,
                                      void* fsAnimEventName);

typedef void (*SendAnimationEventLeaveFn)(TESObjectREFR* refr,
                                          void* fsAnimEventName,
                                          uint32_t returnValue);

std::vector<SendAnimationEventFn>& SendAnimEvent();
std::vector<SendAnimationEventLeaveFn>& SendAnimEventLeave();
}

#define HOOK_LISTENERS_ADD(category, func)                                    \
  namespace HookListeners_ {                                                  \
  namespace category {                                                        \
  static int dummy = ([]() {                                                  \
    HookListeners::category().push_back(func);                                \
    return 0;                                                                 \
  })();                                                                       \
  }                                                                           \
  }