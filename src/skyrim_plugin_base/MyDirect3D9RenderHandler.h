#pragma once

class MyDirect3D9RenderHandler
{
public:
  virtual void OnPresent(void* device) = 0;
  virtual void OnReset(void* device) = 0;

  virtual ~MyDirect3D9RenderHandler() = default;
};