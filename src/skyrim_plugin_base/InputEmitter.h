#pragma once
#include <cstdint>
#include <array>
#include <ctime>
#include "IInputListener.h"
#include "IInputConverter.h"

class InputEmitter {
public:
	struct Data {
		bool *vkCodesDown = nullptr; // bool[256]
		bool *mouseDown = nullptr; // bool[3] = { Left, Right, Middle }
		float mouseX = 0;
		float mouseY = 0;
		int64_t mouseWheelAbsolute = 0;
	};

	InputEmitter() noexcept;
	void Tick(IInputListener &listener, const Data &data, IInputConverter &inputConverter) noexcept;

private:
	std::array<clock_t, 256> vkCodeDownDur;
	std::array<bool, 3> mouseDown;
	int64_t mouseWheelAbsolute = 0;
	float mouseX = -9999.f, mouseY = -9999.f;
};