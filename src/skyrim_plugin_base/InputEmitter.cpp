#include <Windows.h>
#include "InputEmitter.h"

InputEmitter::InputEmitter() noexcept {
	this->vkCodeDownDur.fill(0);
	this->mouseDown.fill(false);
}

void InputEmitter::Tick(IInputListener &listener, const Data &in, IInputConverter &inputConverter) noexcept {
	const bool capitalLetters = in.vkCodesDown[VK_SHIFT];

	for (int i = 0; i != 256; ++i) {
		const bool down = in.vkCodesDown[i];
		const bool wasDown = this->vkCodeDownDur[i] > 0;
		if (down != wasDown) {
			this->vkCodeDownDur[i] = down ? clock() : 0;
			listener.OnKeyStateChange(i, down);
			if (down) {
				const wchar_t ch = inputConverter.VkCodeToChar(i, capitalLetters);
				if (ch) listener.OnChar(ch);
			}
		}
	}

	// Repeat the character until the key isn't released
	for (int i = 0; i < 256; ++i) {
		const auto pressMoment = this->vkCodeDownDur[i];
		if (pressMoment && clock() - pressMoment > CLOCKS_PER_SEC / 2) {
			if (i == VK_BACK || i == VK_RIGHT || i == VK_LEFT) {
				listener.OnKeyStateChange(i, true);
				listener.OnKeyStateChange(i, false);
			}
			else {
				const wchar_t ch = inputConverter.VkCodeToChar(i, capitalLetters);
				if (ch) listener.OnChar(ch);
			}
		}
	}

	for (int i = 0; i < 3; ++i) {
		const bool down = in.mouseDown[i];
		bool &downWas = this->mouseDown[i];
		if (down != downWas) {
			downWas = down;
			// Ignore RBUTTON. We don't want to see context menu
			// TODO: delete or move to better place 
			if (i != 1) {
				listener.OnMouseStateChange(in.mouseX, in.mouseY, (IInputListener::MouseButton)i, down);
			}
		}
	}

	const int32_t diff = in.mouseWheelAbsolute - this->mouseWheelAbsolute;
	if (diff != 0) {
		listener.OnMouseWheel(diff);
		this->mouseWheelAbsolute += diff;
	}

	if (abs(in.mouseX - this->mouseX) > 0.5f || abs(in.mouseY - this->mouseY) > 0.5f) {
		this->mouseX = in.mouseX;
		this->mouseY = in.mouseY;
		listener.OnMouseMove(in.mouseX, in.mouseY);
	}
}