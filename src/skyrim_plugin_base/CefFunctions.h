#pragma once
#include <cstdint>
#include <functional>

typedef void(OnPaint)(void* ctx, const void* paintData, uint32_t size);

typedef void(SkympCef_Configure)(void);
typedef void(SkympCef_Tick)(void);
typedef void(SkympCef_SetOnPaint)(void* ctx, OnPaint* callback);
typedef void(SkympCef_SetFocus)(bool focus);
typedef void(SkympCef_OnMouseStateChange)(float x, float y,
                                          uint8_t mouseButton, bool down);
typedef void(SkympCef_OnMouseMove)(float x, float y);
typedef void(SkympCef_OnMouseWheel)(int wheelDelta);
typedef void(SkympCef_OnKeyStateChange)(uint8_t vkCode, bool down);
typedef void(SkympCef_OnChar)(wchar_t character);
typedef void(SkympCef_LoadUrl)(const wchar_t* url);
typedef void(SkympCef_ExecuteJs)(const char* jsSrc);
typedef uint32_t(SkympCef_GetCPRequiredBufSize)(void);
typedef void(SkympCef_NextOutCP)(char* outBuf, bool* outSuccess,
                                 uint32_t bufSize);

struct CefFunctions
{
  SkympCef_Configure* cefMain = nullptr;
  SkympCef_Tick* cefTick = nullptr;
  SkympCef_SetOnPaint* cefSetOnPaint = nullptr;
  SkympCef_SetFocus* cefSetFocus = nullptr;
  SkympCef_OnMouseStateChange* cefOnMouseStateChange = nullptr;
  SkympCef_OnMouseMove* cefOnMouseMove = nullptr;
  SkympCef_OnMouseWheel* cefOnMouseWheel = nullptr;
  SkympCef_OnKeyStateChange* cefOnKeyStateChange = nullptr;
  SkympCef_OnChar* cefOnChar = nullptr;
  SkympCef_LoadUrl* cefLoadUrl = nullptr;
  SkympCef_ExecuteJs* cefExecuteJs = nullptr;
  SkympCef_GetCPRequiredBufSize* cefGetCPRequiredBufSize = nullptr;
  SkympCef_NextOutCP* cefNextOutCP = nullptr;

  using OnPaintF = std::function<void(const void* buffer, uint32_t size)>;

  static CefFunctions Setup(OnPaintF onPaint);
};