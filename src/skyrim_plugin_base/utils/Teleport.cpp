#include "Teleport.h"
#include <common/IPrefix.h>
#include <scriptdragon/skyscript.h>
#include <skse/GameObjects.h>
#include <skse/GameReferences.h>
#include <sstream>
#include <stdexcept>

void Teleport::Run(TESObjectREFR* refr, uint32_t cellOrWorld,
                   const NiPoint3& pos, const NiPoint3& rot,
                   Wait waitForTeleport)
{
  auto* cell = (TESObjectCELL*)LookupFormByID(cellOrWorld);
  if (cell) {
    if (cell->formType != kFormType_Cell)
      cell = nullptr;
    else if (!cell->IsInterior())
      cell = nullptr;
  }

  auto* world = (TESWorldSpace*)LookupFormByID(cellOrWorld);
  if (world && world->formType != kFormType_WorldSpace)
    world = nullptr;

  if (!world && !cell) {
    std::stringstream ss;
    ss << std::hex << "0x" << cellOrWorld << " is not a valid Cell/WorldSpace";
    throw std::runtime_error(ss.str());
  }

  auto nullHandle = *g_invalidRefHandle;
  MoveRefrToPosition(refr, &nullHandle, cell, world,
                     const_cast<NiPoint3*>(&pos), const_cast<NiPoint3*>(&rot));

  auto id = refr->formID;

  while (waitForTeleport == Wait::True) {
    auto ref = (TESObjectREFR*)LookupFormByID(id);
    if (!ref ||
        (ref->formType != kFormType_Character &&
         ref->formType != kFormType_Reference))
      break;
    if ((ref->pos - pos).Length() < 256.f) {
      if (world && ref->GetWorldspace() == world)
        break;
      if (cell && ref->parentCell == cell)
        break;
    }
    sd::Wait(100);
  }
}