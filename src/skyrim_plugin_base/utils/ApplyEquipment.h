#pragma once
#include <common/IPrefix.h>
#include <cstdint>
#include <skse/GameFormComponents.h>
#include <skse/GameReferences.h>
#include <foo/PropEquipment.h>
#include <skyrim_plugin_base/utils/WeaponCreator.h>

class ApplyEquipment
{
public:
  static void Run(Actor* actor, Equipment equipment, WeaponCreator& cache);

private:
  static void AddWeaponByID(Actor* actor, uint32_t weapID, bool abLeftHand);
  static void EquipItems(Actor* actor, const Equipment& equipment,
                         WeaponCreator& weapCreator);
};