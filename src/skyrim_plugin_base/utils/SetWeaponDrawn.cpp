#include "SetWeaponDrawn.h"
#include <common/IPrefix.h>
#include <mutex>
#include <nlohmann/json.hpp>
#include <set>
#include <skse/GameReferences.h>
#include <skyrim_plugin_base/utils/SetWeaponDrawn.h>
#include <sstream>

namespace {
struct
{
  std::map<uint32_t, bool> overrides;
  std::mutex m;
} g_share;
}

void SetWeaponDrawn::Run(Actor* actor, bool drawn)
{
  {
    std::lock_guard l(g_share.m);
    g_share.overrides[actor->formID] = drawn;
  }
  actor->DrawSheatheWeapon(1);
}

const char* SetWeaponDrawn::RunHookHandler(const char* jsonDump)
{
  static const std::string g_resultDraw =
    nlohmann::json{ { "draw", "0x1" } }.dump();
  static const std::string g_resultSheathe =
    nlohmann::json{ { "draw", "0x0" } }.dump();

  auto j = nlohmann::json::parse(jsonDump);

  std::string ecxS = j["ecx"];
  uint32_t ecxU = 0;
  std::stringstream ss;
  ss << std::hex << ecxS;
  ss >> ecxU;
  const auto actor = reinterpret_cast<Actor*>(ecxU);
  const uint32_t formId = actor ? actor->formID : 0;

  const auto& jDraw = j["draw"];
  const bool draw = jDraw == "0x1" || jDraw == 1 || jDraw == "1";

  const bool resDraw = GetWeapDrawnOverride(formId, draw);
  return resDraw ? g_resultDraw.data() : g_resultSheathe.data();
}

bool SetWeaponDrawn::GetWeapDrawnOverride(uint32_t refrId, bool draw)
{
  std::lock_guard l(g_share.m);
  auto it = g_share.overrides.find(refrId);
  if (it == g_share.overrides.end())
    return draw;
  return it->second;
}