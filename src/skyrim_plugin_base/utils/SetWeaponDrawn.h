#pragma once
#include <cstdint>

class Actor;

class SetWeaponDrawn
{
public:
  static void Run(Actor* actor, bool drawn);

  static const char* RunHookHandler(const char* jsonDump);

private:
  static bool GetWeapDrawnOverride(uint32_t refrId, bool draw);
};