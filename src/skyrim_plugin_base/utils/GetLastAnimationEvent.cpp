#include "GetLastAnimationEvent.h"
#include <algorithm>
#include <chrono>
#include <common/IPrefix.h>
#include <list>
#include <mutex>
#include <skse/GameReferences.h>
#include <skyrim_plugin_base/HookListeners.h>
#include <unordered_set>

using namespace std::chrono_literals;

namespace {
struct AnimEventData
{
  uint32_t refrId = 0;
  std::string name;
  bool success = false;
  std::chrono::steady_clock::time_point moment;
  std::unordered_set<std::string> channelsAlreadyRead;

  bool NeedsToBeDeleted() const noexcept
  {
    return moment + GetLastAnimationEvent::StoringDuration() <
      std::chrono::steady_clock::now();
  }
};
}

struct
{
  std::mutex m;
  std::list<AnimEventData> animEvents;
} g_share;

void GetLastAnimationEvent_SendAnimEventLeave(TESObjectREFR* refr,
                                              void* fsAnimEventName_,
                                              uint32_t returnValue)
{
  auto fsAnimEventName = reinterpret_cast<BSFixedString*>(fsAnimEventName_);

  std::lock_guard l(g_share.m);
  g_share.animEvents.push_back(
    { refr->formID, std::string(fsAnimEventName->data), returnValue != 0,
      std::chrono::steady_clock::now() });

  auto i = g_share.animEvents.begin();
  while (i != g_share.animEvents.end())
    i->NeedsToBeDeleted() ? i = g_share.animEvents.erase(i) : ++i;
}

HOOK_LISTENERS_ADD(SendAnimEventLeave,
                   GetLastAnimationEvent_SendAnimEventLeave);

std::string GetLastAnimationEvent::Run(TESObjectREFR* refr,
                                       const char* channelId, bool* outSuccess)
{
  std::lock_guard l(g_share.m);

  auto it = std::find_if(g_share.animEvents.begin(), g_share.animEvents.end(),
                         [&](const AnimEventData& data) {
                           return data.refrId == refr->formID &&
                             !data.NeedsToBeDeleted() &&
                             !data.channelsAlreadyRead.count(channelId);
                         });
  if (it == g_share.animEvents.end())
    return "";

  if (outSuccess)
    *outSuccess = it->success;

  it->channelsAlreadyRead.insert(channelId);
  return it->name;
}