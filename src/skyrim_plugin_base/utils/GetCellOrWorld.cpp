#include "GetCellOrWorld.h"

#include <common/IPrefix.h>
#include <scriptdragon/skyscript.h>
#include <skse/GameReferences.h>

uint32_t GetCellOrWorld::Run(TESObjectREFR* refr)
{
  auto cell = sd::GetParentCell(refr);
  if (cell && cell->IsInterior())
    return cell->formID;

  auto world = sd::GetWorldSpace(refr);
  if (world)
    return world->formID;

  return 0;
}