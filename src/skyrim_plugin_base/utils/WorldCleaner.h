#pragma once
#include <viet/Logging.h>

class TESObjectREFR;

// Note that this class is not tested by our test suite because it was copied
// from the hand-tested version. Please consider adding a test.

// Use only in game thread

class WorldCleaner
{
public:
  WorldCleaner() noexcept;
  ~WorldCleaner();
  void TickGame(Viet::Logging::Logger* optionalLogger) noexcept;
  void ModProtection(uint32_t formid, int mod) noexcept;

private:
  void OnPlayerCellChange() noexcept;
  void DealWithRef(TESObjectREFR* ref) noexcept;

  struct Impl;
  Impl* const pImpl;
};