#include "Wait.h"
#include <common/IPrefix.h>
#include <scriptdragon/skyscript.h>
#include <skse/GameMenus.h>
#include <skyrim_plugin_base/DInputHook.h>

void Wait::Run()
{
  int waitMs = 0;

  auto ui = UIStringHolder::GetSingleton();
  auto mm = MenuManager::GetSingleton();
  if (mm && ui) {
    if (mm->IsMenuOpen(&ui->inventoryMenu) || mm->IsMenuOpen(&ui->magicMenu)) {
      waitMs = DInputHook::WasDangerousButtonDown(100) ? 200 : 100;
    }
  }

  sd::Wait(waitMs);
}