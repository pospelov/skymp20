#pragma once
#include <foo/PropActorValues.h>

class Actor;

class GetHealth
{
public:
  static ActorValueEntry Run(Actor *actor);
};