#include "GetEquipment.h"
#include <scriptdragon/enums.h>
#include <scriptdragon/skyscript.h>
#include <skse/GameForms.h>
#include <skse/GameObjects.h>
#include <skse/PapyrusObjectReference.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <stdexcept>

Equipment GetEquipment::Run(Actor* actor)
{
  if (!actor)
    throw std::runtime_error("Actor == nullptr");

  Equipment equipment;

  FillEquipWeaponID(actor, equipment);
  FillEquipArmorID(actor, equipment);

  return equipment;
}

void GetEquipment::FillEquipWeaponID(Actor* actor, Equipment& equipment)
{
  equipment.equippedWeapon.rightHand = actor->GetEquippedObject(false)
    ? actor->GetEquippedObject(false)->formID
    : 0;

  equipment.equippedWeapon.leftHand = actor->GetEquippedObject(true)
    ? actor->GetEquippedObject(true)->formID
    : 0;

  if (equipment.equippedWeapon.leftHand != 0 &&
      equipment.equippedWeapon.rightHand == 0)
    equipment.equippedWeapon.rightHand = ID_TESObjectWEAP::Unarmed;
}

void GetEquipment::FillEquipArmorID(Actor* actor, Equipment& equipment)
{
  equipment.equippedArmor.clear();
  auto numItems = papyrusObjectReference::GetNumItems(actor);

  for (size_t i = 0; i < numItems; ++i) {
    auto form = papyrusObjectReference::GetNthForm(actor, i);

    if (IsSuppotedTypeForm(form) && sd::IsEquipped(actor, form))
      equipment.equippedArmor.push_back(form->formID);
  }
}

bool GetEquipment::IsSuppotedTypeForm(TESForm* form)
{
  if (!form)
    return false;

  if (form->formType == FormType::kFormType_Armor ||
      form->formType == FormType::kFormType_Arrow ||
      form->formType == FormType::kFormType_Ammo ||
      form->formType == FormType::kFormType_Light)
    return true;

  return false;
}
