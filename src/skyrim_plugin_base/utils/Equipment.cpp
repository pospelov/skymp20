#include "Equipment.h"

bool Equipment_::IsPresentInEquipment(const Equipment& equipment,
                                      uint32_t formId)
{
  if (equipment.equippedWeapon.leftHand == formId ||
      equipment.equippedWeapon.rightHand == formId)
    return true;
  for (auto& id : equipment.equippedArmor) {
    if (id == formId)
      return true;
  }
  return false;
}
