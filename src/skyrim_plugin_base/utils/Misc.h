#pragma once
#include <common/IPrefix.h>
#include <scriptdragon/skyscript.h>
#include <skse/GameReferences.h>
#include <unordered_map>

namespace Misc {
inline BSFixedString* FixedStr(const char* str)
{
  thread_local std::unordered_map<std::string, BSFixedString*> g_storage;
  auto it = g_storage.find(str);
  if (it == g_storage.end())
    it = g_storage.insert({ str, new BSFixedString(str) }).first;
  return it->second;
}

template <class T>
inline T GetAnimVar(TESObjectREFR* refr, const char* name);

template <>
inline float GetAnimVar<float>(TESObjectREFR* refr, const char* name)
{
  float ret = 0.f;
  if (refr)
    refr->animGraphHolder.GetVariableFloat(FixedStr(name), &ret);
  return ret;
}

template <>
inline bool GetAnimVar<bool>(TESObjectREFR* refr, const char* name)
{
  uint8_t ret = 0;
  if (refr)
    refr->animGraphHolder.GetVariableBool(FixedStr(name), &ret);
  return ret > 0;
}

template <>
inline uint32_t GetAnimVar<uint32_t>(TESObjectREFR* refr, const char* name)
{
  UInt32 ret = 0;
  if (refr)
    refr->animGraphHolder.GetVariableInt(FixedStr(name), &ret);
  return ret;
}

inline float GetCurrentAV(Actor* actor, const char* name)
{
  return sd::GetActorValue(actor, const_cast<char*>(name));
}

inline void SetCurrentAV(Actor* actor, const char* name, float newValue)
{
  sd::ForceActorValue(actor, const_cast<char*>(name), newValue);
}

inline bool IsEquip(const std::string& s)
{
  return (s.find("Equip") != std::string::npos ||
          s.find("equip") != std::string::npos) &&
    s.find("Unequip") == std::string::npos &&
    s.find("unequip") == std::string::npos;
}

inline bool IsUnequip(const std::string& s)
{
  return (s.find("Unequip") != std::string::npos ||
          s.find("unequip") != std::string::npos);
}

inline bool IsTorch(const std::string& s)
{
  return s.find("torch") != std::string::npos ||
    s.find("Torch") != std::string::npos;
}
}