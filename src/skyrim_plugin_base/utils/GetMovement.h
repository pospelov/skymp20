#pragma once
#include <chrono>
#include <foo/PropMovement.h>
#include <list>
#include <mutex>
#include <optional>
#include <skyrim_plugin_base/utils/SmartRef.h>
#include <string>
#include <variant>

class Actor;

class GetMovement
{
public:
  struct ActorCache
  {
    class Blocker
    {
    public:
      int m_blocker = -1;
      std::chrono::steady_clock::time_point m_blockMoment;

      template <class Duration>
      void ProcessValue(bool& realValue, Duration blockDuration)
      {
        if (static_cast<int>(realValue) == m_blocker ||
            std::chrono::steady_clock::now() > m_blockMoment + blockDuration)
          m_blocker = -1;
        else if (m_blocker != -1)
          realValue = m_blocker > 0;
      }

      void BlockIfNeed(bool& realValue, bool newValue)
      {
        if (realValue != newValue) {
          realValue = newValue;
          m_blocker = newValue ? 1 : 0;
          m_blockMoment = std::chrono::steady_clock::now();
        }
      }
    };
    Blocker weapDrawn, blocking, sneaking;
  };

  static Movement Run(Actor* actor, ActorCache& cache);
};