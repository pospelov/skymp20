#pragma once
#include <unordered_map>
#include <unordered_set>

class TESForm;
class TESObjectCELL;
class TESObjectREFR;
class NiPoint3;

class FindClosestRef
{
public:
  using BaseForms = std::unordered_set<TESForm*>;

  struct Cache
  {
    std::unordered_map<TESObjectCELL*, BaseForms> basesByCellPtr;
  };

  static TESObjectREFR* Run(const NiPoint3& pos, Cache& cache);

private:
  static BaseForms GetCellBaseForms(TESObjectCELL* cell);
};