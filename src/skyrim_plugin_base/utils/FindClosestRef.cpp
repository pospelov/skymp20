#include "FindClosestRef.h"
#include <common/IPrefix.h>
#include <scriptdragon/skyscript.h>
#include <skse/GameForms.h>
#include <skse/GameReferences.h>
#include <skyrim_plugin_base/utils/Cast.h>

FindClosestRef::BaseForms FindClosestRef::GetCellBaseForms(TESObjectCELL* cell)
{
  BaseForms basesFound;

  const int depth = 40;
  for (int i = -depth; i != depth; ++i) {
    auto formId = uint32_t(int64_t(cell->formID) + i);
    auto cell = Cast::Run<TESObjectCELL>(LookupFormByID(formId));
    if (!cell)
      continue;

    for (size_t j = 0; j != cell->objectList.count; ++j) {
      auto obj = cell->objectList[j];
      if (!obj || obj->formType != kFormType_Reference)
        continue;
      if (obj->formID >= 0xff000000)
        continue;
      auto base = obj->baseForm;
      if (!base)
        continue;
      basesFound.insert(base);
    }
  }

  return basesFound;
}

TESObjectREFR* FindClosestRef::Run(const NiPoint3& pos, Cache& cache)
{
  auto cell = sd::GetParentCell(sd::GetPlayer());
  if (!cell)
    return nullptr;

  BaseForms* set = nullptr;
  auto it = cache.basesByCellPtr.find(cell);
  if (it == cache.basesByCellPtr.end()) {
    auto& v = cache.basesByCellPtr[cell];
    v = GetCellBaseForms(cell);
    set = &v;
  } else {
    set = &it->second;
  }

  auto wineList = (BGSListForm*)LookupFormByID(0x72ea3);
  for (auto form : *set)
    CALL_MEMBER_FN(wineList, AddFormToList)(form);

  return sd::FindClosestReferenceOfAnyTypeInList(wineList, pos.x, pos.y, pos.z,
                                                 7000.f);
}