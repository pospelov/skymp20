#include "GetHealth.h"
#include <scriptdragon/skyscript.h>
#include <stdexcept>

ActorValueEntry GetHealth::Run(Actor* actor)
{
  if (!actor)
    throw std::runtime_error("actor == nullptr");

  ActorValueEntry res;
  res.base = sd::GetBaseActorValue(actor, "Health");
  res.currentPercentage = sd::GetActorValuePercentage(actor, "Health");
  return res;
}