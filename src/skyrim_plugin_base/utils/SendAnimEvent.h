#pragma once

class TESObjectREFR;

class SendAnimEvent
{
public:
  static void Run(TESObjectREFR* actor, const char* animEventName);

  static bool IsAnimationEventSentByPlugin(void* fsAnimEventName);
};