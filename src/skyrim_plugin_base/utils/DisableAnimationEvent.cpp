#include "DisableAnimationEvent.h"
#include <common/IPrefix.h>
#include <mutex>
#include <skse/GameReferences.h>
#include <skyrim_plugin_base/HookListeners.h>
#include <skyrim_plugin_base/utils/SendAnimEvent.h>
#include <string>
#include <unordered_set>

namespace {
struct GlobalCond
{
  DisableAnimationEvent::Condition cond;
  bool ffOnly = false;
};

struct
{
  std::vector<GlobalCond> condsGlobal;
  std::mutex m;
} g_share;
}

void* DisableAnimationEvent_SendAnimEvent(TESObjectREFR* refr,
                                          void* fsAnimEventName)
{
  if (!SendAnimEvent::IsAnimationEventSentByPlugin(fsAnimEventName)) {
    auto fs = reinterpret_cast<BSFixedString*>(fsAnimEventName);
    std::lock_guard l(g_share.m);

    for (auto& cond : g_share.condsGlobal) {
      if (cond.cond(fs->data)) {
        if (!cond.ffOnly || refr->formID >= 0xff000000)
          return nullptr;
      }
    }
  }
  return fsAnimEventName;
}

HOOK_LISTENERS_ADD(SendAnimEvent, DisableAnimationEvent_SendAnimEvent);

void DisableAnimationEvent::Run(DisableAnimationEvent::Condition c,
                                bool ffOnly)
{
  std::lock_guard l(g_share.m);
  g_share.condsGlobal.push_back({ c, ffOnly });
}