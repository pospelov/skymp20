#pragma once
#include <utility>

class GetMousePos
{
public:
  static std::pair<float, float> Run();
};