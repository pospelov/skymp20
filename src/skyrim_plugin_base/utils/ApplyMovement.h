#pragma once

class Actor;
struct Movement;

class ApplyMovement
{
public:
  static void Run(Actor* actor, const Movement& movement,
                  float timeInSeconds = 0.2f);
};