#include "GetMovement.h"

#include <common/IPrefix.h>
#include <ctime>
#include <mutex>
#include <scriptdragon/obscript.h>
#include <scriptdragon/skyscript.h>
#include <skse/GameInput.h>
#include <skse/GameReferences.h>
#include <skse/PapyrusObjectReference.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/GetCellOrWorld.h>
#include <skyrim_plugin_base/utils/GetLastAnimationEvent.h>
#include <skyrim_plugin_base/utils/Misc.h>
#include <unordered_map>

using namespace std::chrono_literals;

namespace {

bool IsSneaking(Actor* actor)
{
  return sd::IsSneaking(actor) || Misc::GetAnimVar<bool>(actor, "IsSneaking");
}

RunMode GetRunMode(Actor* actor)
{
  if (sd::IsSprinting(actor))
    return RunMode::Sprinting;

  float speed = Misc::GetAnimVar<float>(actor, "SpeedSampled");
  if (!speed)
    return RunMode::Standing;

  auto playerControls = PlayerControls::GetSingleton();
  bool isPlayerRunning = playerControls ? playerControls->runMode : true;

  bool isRunning = true;
  if (actor == sd::GetPlayer()) {
    if (!isPlayerRunning || speed < 150.f)
      isRunning = false;
  } else {
    if (!sd::IsRunning(actor))
      isRunning = false;
  }
  if (sd::Obscript::IsBlocking(actor)) {
    if (IsSneaking(actor))
      isRunning = true;
    else
      isRunning = false;
  }

  const float carryWeight = Misc::GetCurrentAV(actor, "CarryWeight");
  const float totalItemWeight =
    papyrusObjectReference::GetTotalItemWeight(actor);
  if (carryWeight < totalItemWeight)
    isRunning = false;

  return isRunning ? RunMode::Running : RunMode::Walking;
}
}

Movement GetMovement::Run(Actor* actor, ActorCache& cache)
{
  Movement res;
  res.pos = { sd::GetPositionX(actor), sd::GetPositionY(actor),
              sd::GetPositionZ(actor) };
  res.angleZ = sd::GetAngleZ(actor);
  res.runMode = GetRunMode(actor);
  res.direction = res.runMode != RunMode::Standing
    ? 360.f * Misc::GetAnimVar<float>(actor, "Direction")
    : 0.f;
  res.cellOrWorld = GetCellOrWorld::Run(actor);
  res.isInJumpState = Misc::GetAnimVar<bool>(actor, "bInJumpState");

  bool animEventSuccess = false;
  auto animEvent =
    GetLastAnimationEvent::Run(actor, "GetMovement", &animEventSuccess);
  std::transform(animEvent.begin(), animEvent.end(), animEvent.begin(),
                 tolower);
  if (!animEventSuccess)
    animEvent.clear();

  res.isWeapDrawn = sd::IsWeaponDrawn(actor);

  cache.weapDrawn.ProcessValue(res.isWeapDrawn, 750ms);
  if (!Misc::IsTorch(animEvent)) {
    if (Misc::IsEquip(animEvent)) {
      cache.weapDrawn.BlockIfNeed(res.isWeapDrawn, true);
    } else if (Misc::IsUnequip(animEvent)) {
      cache.weapDrawn.BlockIfNeed(res.isWeapDrawn, false);
    }
  }

  if (Misc::GetAnimVar<bool>(actor, "IsBashing"))
    res.isBlocking = true;
  else {
    res.isBlocking = sd::Obscript::IsBlocking(actor) > 0;

    cache.blocking.ProcessValue(res.isBlocking, 300ms);

    if (animEvent == "blockstart") {
      cache.blocking.BlockIfNeed(res.isBlocking, true);
    } else if (animEvent == "blockstop") {
      cache.blocking.BlockIfNeed(res.isBlocking, false);
    }
  }

  res.isSneaking = IsSneaking(actor);
  cache.sneaking.ProcessValue(res.isSneaking, 300ms);
  if (animEvent == "sneakstart") {
    cache.sneaking.BlockIfNeed(res.isSneaking, true);
  } else if (animEvent == "sneakstop") {
    cache.sneaking.BlockIfNeed(res.isSneaking, false);
  }

  return res;
}