#pragma once
#include <cstdint>

class NiPoint3;
class TESObjectREFR;

class Teleport
{
public:
  enum class Wait
  {
    False,
    True
  };

  static void Run(TESObjectREFR* refr, uint32_t cellOrWorld,
                       const NiPoint3& pos, const NiPoint3& rot,
                       Wait waitForTeleport);
};