#pragma once
#include <cstdint>

class GetFreeFormId
{
public:
  static uint32_t Run();
};