#pragma once
#include <foo/PropActorValues.h>
#include <skyrim_plugin_base/utils/SmartRef.h>

class ApplyHealth
{
public:
  static constexpr float g_realBaseHealth = 1'000'000.f;

  static void Run(std::shared_ptr<SmartRef>& smartRef,
                  const ActorValueEntry& health);

  static void RunOnPlayer(const ActorValueEntry& health);
};