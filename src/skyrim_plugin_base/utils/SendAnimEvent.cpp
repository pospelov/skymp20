#include "SendAnimEvent.h"
#include <common/IPrefix.h>
#include <skse/GameReferences.h>
#include <skyrim_plugin_base/utils/Misc.h>
#include <stdexcept>
#include <unordered_map>

namespace {
BSFixedString* g_fs = nullptr;
}

void SendAnimEvent::Run(TESObjectREFR* refr, const char* animEventName)
{
  if (!refr)
    throw std::runtime_error("refr == nullptr");

  g_fs = Misc::FixedStr(animEventName);
  refr->animGraphHolder.SendAnimationEvent(g_fs);
  g_fs = nullptr;
}

bool SendAnimEvent::IsAnimationEventSentByPlugin(void* fsAnimEventName)
{
  return g_fs == fsAnimEventName;
}