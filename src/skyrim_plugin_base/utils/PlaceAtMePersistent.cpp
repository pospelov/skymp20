#include "PlaceAtMePersistent.h"
#include "WeaponCreator.h"
#include <common/IPrefix.h>
#include <scriptdragon/enums.h>
#include <scriptdragon/skyscript.h>
#include <skse/GameForms.h>
#include <skse/GameObjects.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/GetFreeFormId.h>
#include <skyrim_plugin_base/utils/SetFormId.h>
#include <stdexcept>

void PlaceAtMePersistent::ChangeOutfitNPC(TESNPC* npc,
                                          std::optional<Equipment> equipment)
{
  if (equipment.has_value()) {

    auto newOutfit =
      reinterpret_cast<BGSOutfit*>(new uint8_t[sizeof(BGSOutfit)]);

    auto defaultOutfit = npc->defaultOutfit
      ? npc->defaultOutfit
      : Cast::Run<TESNPC>(sd::GetBaseObject(sd::GetPlayer()))->defaultOutfit;

    memcpy(newOutfit, defaultOutfit, sizeof(*npc->defaultOutfit));

    newOutfit->armorOrLeveledItemArray.Allocate(
      equipment->equippedArmor.size());

    for (size_t i = 0; i < equipment->equippedArmor.size(); ++i) {
      auto form = sd::GetFormById(equipment->equippedArmor[i]);

      if (form && form->IsArmor())
        newOutfit->armorOrLeveledItemArray[i] = form;
    }
    npc->defaultOutfit = newOutfit;
  }
}

uint32_t PlaceAtMePersistent::Run(TESObjectREFR* me, TESForm* base,
                                  std::optional<Equipment> equipment)
{
  if (!base)
    throw std::runtime_error("base == nullptr");
  if (!me)
    throw std::runtime_error("me == nullptr");

  TESNPC* npc = Cast::Run<TESNPC>(base);

  if (!npc)
    throw std::runtime_error("Cast TESForm to TESNPC == nullptr");

  ChangeOutfitNPC(npc, equipment);

  int count = 1;
  bool isPersistent = true, isDisabled = false;

  npc->combatStyle->flags = TESCombatStyle::kFlag_AllowDualWielding;

  if (auto refr = sd::PlaceAtMe(me, base, count, isPersistent, isDisabled)) {
    for (size_t i = 0; i < npc->container.numEntries; ++i) {
      if (auto form = npc->container.entries[i]->form) {
        auto item = sd::GetFormById(form->formID);
        if (item && sd::GetItemCount(refr, item) > 0) {
          sd::RemoveItem(refr, item, -1, true, nullptr);
        }
      }
    }

    return sd::GetFormID((TESForm*)refr);
  }

  throw std::runtime_error("PlaceAtMe returned nullptr");
}