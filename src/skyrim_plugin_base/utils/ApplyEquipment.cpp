#include "ApplyEquipment.h"
#include <PapyrusObjectReference.h>
#include <scriptdragon/enums.h>
#include <scriptdragon/skyscript.h>
#include <skse/GameForms.h>
#include <skse/GameObjects.h>
#include <skse/PapyrusWeapon.h>
#include <skyrim_plugin_base/DInputHook.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/Equipment.h>
#include <skyrim_plugin_base/utils/GetEquipment.h>
#include <stdexcept>

namespace {
bool IsLeftHandWeaponRequired(const Equipment& equipment)
{
  auto leftHandForm = LookupFormByID(equipment.equippedWeapon.leftHand);
  return leftHandForm && leftHandForm->formType == kFormType_Weapon &&
    equipment.equippedWeapon.leftHand != ID_TESObjectWEAP::Unarmed;
}
}

void ApplyEquipment::Run(Actor* actor, Equipment equipment,
                         WeaponCreator& weapCreator)
{
  if (!actor)
    throw std::runtime_error("actor == nullptr");

  if (!equipment.equippedWeapon.rightHand)
    equipment.equippedWeapon.rightHand = ID_TESObjectWEAP::Unarmed;

  weapCreator.Run(equipment);

  AddWeaponByID(actor, equipment.equippedWeapon.rightHand, false);
  AddWeaponByID(actor, equipment.equippedWeapon.leftHand, true);

  EquipItems(actor, equipment, weapCreator);

  bool combat =
    IsLeftHandWeaponRequired(equipment) && !sd::GetEquippedWeapon(actor, true);
  if (combat) {
    sd::StartCombat(actor, sd::GetPlayer());
  } else {
    sd::StopCombat(actor);
  }
}

void ApplyEquipment::AddWeaponByID(Actor* actor, uint32_t weapID,
                                   bool abLeftHand)
{
  auto form = LookupFormByID(weapID);
  auto equipedObject = actor->GetEquippedObject(abLeftHand);

  if (!form && equipedObject) {
    if (equipedObject->formType == FormType::kFormType_Spell) {
      sd::UnequipSpell(actor, Cast::Run<SpellItem>(equipedObject), abLeftHand);
    }
  }
  if (!form)
    return;

  if ((form->IsWeapon() || form->formType == FormType::kFormType_Light) &&
      sd::GetItemCount(actor, form) == 0) {
    sd::AddItem(actor, form, 1, false);
  }

  if ((form->IsWeapon() || form->formType == FormType::kFormType_Light) &&
      equipedObject) {
    if (equipedObject->formType == FormType::kFormType_Spell) {
      sd::UnequipSpell(actor, Cast::Run<SpellItem>(equipedObject), abLeftHand);
    }
  }

  if (form->formType == FormType::kFormType_Spell) {

    auto _equipedObject = actor->GetEquippedObject(abLeftHand);

    if (!_equipedObject) {
      sd::EquipSpell(actor, Cast::Run<SpellItem>(form), abLeftHand);
    }

    if (_equipedObject && _equipedObject->formID != form->formID) {
      sd::EquipSpell(actor, Cast::Run<SpellItem>(form), abLeftHand);
    }
  }
}

void ApplyEquipment::EquipItems(Actor* actor, const Equipment& equipment,
                                WeaponCreator& weapCreator)
{
  auto numItems = papyrusObjectReference::GetNumItems(actor);

  for (size_t i = 0; i < numItems; ++i) {
    auto form = papyrusObjectReference::GetNthForm(actor, i);

    if (!form)
      continue;

    if (GetEquipment::IsSuppotedTypeForm(form)) {
      if (Equipment_::IsPresentInEquipment(equipment, form->formID)) {
        if (!sd::IsEquipped(actor, form)) {
          sd::EquipItem(actor, form, true, true);
        }
      } else {
        sd::UnequipItem(actor, form, true, true);
        if (!sd::IsEquipped(actor, form) ||
            form->formType == FormType::kFormType_Light)
          sd::RemoveItem(actor, form, -1, true, nullptr);
      }
    } else {
      int requiredCount =
        Equipment_::IsPresentInEquipment(equipment, form->formID) ? 1 : 0;
      sd::RemoveItem(actor, form,
                     sd::GetItemCount(actor, form) - requiredCount, true,
                     nullptr);
    }
  }

  for (auto& armor : equipment.equippedArmor) {
    auto item = sd::GetFormById(armor);

    if (GetEquipment::IsSuppotedTypeForm(item) &&
        !sd::IsEquipped(actor, item)) {
      sd::EquipItem(actor, item, true, true);
    }
  }
}