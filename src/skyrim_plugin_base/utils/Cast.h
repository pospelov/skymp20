#pragma once
#include <cstdint>

class TESForm;

class Cast
{
public:
  template <class Form>
  static Form* Run(TESForm* form)
  {
    return (Form*)Cast::Run(Form::kTypeID, form);
  }

  template <class Form>
  static Form* Run(uint32_t formId)
  {
    return Cast::Run<Form>(GetFormById(formId));
  }

private:
  static TESForm* Run(int typeId, TESForm* form);
  static TESForm* GetFormById(uint32_t formId);
};