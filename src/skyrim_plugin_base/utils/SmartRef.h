#pragma once
#include <common/IPrefix.h>
#include <foo/PropEquipment.h>
#include <optional>
#include <skse/NiTypes.h>
#include <skyrim_plugin_base/utils/FindClosestRef.h>

class WorldCleaner;

class SmartRef
{
public:
  SmartRef(const NiPoint3& spawnPoint, FindClosestRef::Cache& cache,
           std::optional<Equipment> equipment = std::nullopt,
           std::shared_ptr<WorldCleaner> worldCleaner = nullptr);

  ~SmartRef();

  TESObjectREFR* GetRef() const noexcept;

  SmartRef(const SmartRef&) = delete;
  SmartRef& operator=(const SmartRef&) = delete;

private:
  uint32_t m_refId = 0;
  TESForm* m_base = nullptr;
  std::shared_ptr<WorldCleaner> m_worldCleaner;
};