#pragma once
#include <cstdint>

class TESObjectREFR;

class GetCellOrWorld
{
public:
  static uint32_t Run(TESObjectREFR* ref);
};