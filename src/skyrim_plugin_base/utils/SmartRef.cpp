#pragma once
#include "SmartRef.h"
#include <cstdint>
#include <scriptdragon/enums.h>
#include <scriptdragon/obscript.h>
#include <scriptdragon/skyscript.h>
#include <skse/GameReferences.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/GetCellOrWorld.h>
#include <skyrim_plugin_base/utils/Misc.h>
#include <skyrim_plugin_base/utils/PlaceAtMePersistent.h>
#include <skyrim_plugin_base/utils/SetWeaponDrawn.h>
#include <skyrim_plugin_base/utils/Teleport.h>
#include <skyrim_plugin_base/utils/WorldCleaner.h>

namespace {
enum class KeepOffsetMode
{
  Running,
  Walking
};

void KeepOffset(Actor* ref, NiPoint3 offsetPos, float offsetAngle,
                KeepOffsetMode mode)
{
  sd::KeepOffsetFromActor(ref, ref, offsetPos.x, offsetPos.y, offsetPos.z, 0.f,
                          0.f, offsetAngle,
                          mode == KeepOffsetMode::Walking ? 2048.f : 1.f, 1.f);
}
}

SmartRef::SmartRef(const NiPoint3& spawnPoint, FindClosestRef::Cache& cache,
                   std::optional<Equipment> equipment,
                   std::shared_ptr<WorldCleaner> worldCleaner)
  : m_worldCleaner(worldCleaner)
{

  auto spawner = FindClosestRef::Run(spawnPoint, cache);
  if (!spawner)
    spawner = sd::GetPlayer();

  auto base = sd::GetFormById(ID_TESNPC::AADeleteWhenDoneTestJeremyRegular);

  m_refId = PlaceAtMePersistent::Run(spawner, base, equipment);
  m_base = base;

  if (m_worldCleaner)
    m_worldCleaner->ModProtection(m_refId, 1);
}

SmartRef::~SmartRef()
{
  if (m_worldCleaner)
    m_worldCleaner->ModProtection(m_refId, -1);
  if (auto ref = Cast::Run<TESObjectREFR>(m_refId)) {
    if (sd::GetBaseObject(ref) == m_base) {
      sd::Disable(ref, false);
      sd::Delete(ref);
    }
  }
}

TESObjectREFR* SmartRef::GetRef() const noexcept
{
  return Cast::Run<TESObjectREFR>(m_refId);
}