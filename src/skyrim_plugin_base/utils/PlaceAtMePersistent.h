#pragma once
#include <common/IPrefix.h>
#include <cstdint>
#include <optional>
#include <skse/GameFormComponents.h>
#include <skse/GameReferences.h>
#include <foo/PropEquipment.h>
class TESObjectREFR;
class TESObjectWEAP;
class TESForm;
class TESNPC;
class BGSOutfit;

class PlaceAtMePersistent
{
public:
  static uint32_t Run(TESObjectREFR* me, TESForm* base,
                      std::optional<Equipment> equipment = std::nullopt);

private:
  static void ChangeOutfitNPC(TESNPC* npc, std::optional<Equipment> equipment);
  
  enum PackageFlags
  {
    IgnoreCombat = 0x100000,
    NoCombatAlert = 0x8000000
  };
};