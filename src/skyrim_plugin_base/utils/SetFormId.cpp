#include "SetFormId.h"
#include <common/IPrefix.h>
#include <mutex>
#include <skse/GameForms.h>

#pragma optimize("", off)

namespace {
uint32_t g_newId = 0;
std::mutex g_mutex;

void SetFormIdReal(TESForm* arg)
{
  typedef void(__stdcall * _SetFormID)(UInt32 id, bool bGenerateID);
  auto setFormFoform = _SetFormID(0x00451D50);
  auto x = arg;
  x->formID = 0;
  __asm mov ecx, x;
  setFormFoform(g_newId, true);
}

typedef void (*SetFormId_)(TESForm*);
SetFormId_ g_setFormId = SetFormIdReal;

#pragma optimize("", on)
}

void SetFormId::Run(TESForm* form, uint32_t newId)
{
  std::lock_guard l(g_mutex);
  g_newId = newId;
  g_setFormId(form);
}