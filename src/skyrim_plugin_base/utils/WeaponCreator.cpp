#include "WeaponCreator.h"
#include <PapyrusWeapon.h>
#include <scriptdragon/skyscript.h>
#include <skse/GameForms.h>
#include <skse/GameObjects.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <stdexcept>

Equipment::EquippedWeapon WeaponCreator::GetCreatedIdByOriginId(
  uint32_t originId, WeaponCreator& weaponCreator)
{
  Equipment::EquippedWeapon info;

  if (weaponCreator.weaponIdCash.find(originId) !=
      weaponCreator.weaponIdCash.end()) {
    info.rightHand = weaponCreator.weaponIdCash[originId].right;
    info.leftHand = weaponCreator.weaponIdCash[originId].left;
  }
  return info;
}

uint32_t WeaponCreator::GetOriginIdByCreatedId(uint32_t _createdId,
                                               WeaponCreator& weaponCreator)
{
  uint32_t info = 0;

  for (auto& [originId, createdId] : weaponCreator.weaponIdCash) {
    if (createdId.left == _createdId || createdId.right == _createdId) {
      info = originId;
      break;
    }
  }

  return info;
}

bool WeaponCreator::IsWeaponTwoHanded(TESObjectWEAP* weapon)
{
  if (!weapon)
    return false;

  return (weapon->type() == TESObjectWEAP::GameData::kType_Bow ||
          weapon->type() == TESObjectWEAP::GameData::kType_TwoHandAxe ||
          weapon->type() == TESObjectWEAP::GameData::kType_TwoHandSword ||
          weapon->type() == TESObjectWEAP::GameData::kType_CrossBow);
}

void WeaponCreator::Run(Equipment& equipment)
{
    FindOrCreateWeaponForHand(this->weaponIdCash,
                              equipment.equippedWeapon.rightHand, false);

    FindOrCreateWeaponForHand(this->weaponIdCash,
                              equipment.equippedWeapon.leftHand, true);
}

void WeaponCreator::FindOrCreateWeaponForHand(
  std::map<uint32_t, SlotType>& weaponIdCache, uint32_t& weaponId,
  bool abLeftHand)
{
  auto weapon = sd::GetFormById(weaponId);

  if (weapon && weapon->formType == FormType::kFormType_Light) {
    SaveTorchId(weaponIdCache, weaponId);
    return;
  }

  if (!weapon)
    return;

  if (weapon->IsWeapon()) {
    if (weaponIdCache.find(weaponId) == weaponIdCache.end()) {

      auto myWeaponRight =
        reinterpret_cast<TESObjectWEAP*>(new uint8_t[sizeof(TESObjectWEAP)]);
      auto myWeaponLeft =
        reinterpret_cast<TESObjectWEAP*>(new uint8_t[sizeof(TESObjectWEAP)]);

      memcpy(myWeaponRight, weapon, sizeof(TESObjectWEAP));
      memcpy(myWeaponLeft, weapon, sizeof(TESObjectWEAP));

      BGSEquipSlot* equipTypeRight =
        Cast::Run<BGSEquipSlot>(sd::GetFormById(FormIdEquipTypeRight));

      BGSEquipSlot* equipTypeLeft =
        Cast::Run<BGSEquipSlot>(sd::GetFormById(FormIdEquipTypeLeft));

      BGSEquipSlot* equipTypeBoth =
        Cast::Run<BGSEquipSlot>(sd::GetFormById(FormIdEquipTypeBoth));

      if (!equipTypeRight || !equipTypeLeft || !equipTypeBoth)
        return;

      BGSEquipSlot* desiredEquipTypeR =
        IsWeaponTwoHanded(myWeaponRight) ? equipTypeBoth : equipTypeRight;

      BGSEquipSlot* desiredEquipTypeL =
        IsWeaponTwoHanded(myWeaponLeft) ? equipTypeBoth : equipTypeLeft;

      myWeaponRight->equipType.SetEquipSlot(desiredEquipTypeR);
      myWeaponLeft->equipType.SetEquipSlot(desiredEquipTypeL);

      papyrusWeapon::SetBaseDamage(myWeaponRight, 100);
      papyrusWeapon::SetBaseDamage(myWeaponLeft, 50);

      SetFormId::Run(myWeaponLeft, GetFreeFormId::Run());
      SetFormId::Run(myWeaponRight, GetFreeFormId::Run());

      weaponIdCache[weaponId].right = sd::GetFormID(myWeaponRight);
      weaponIdCache[weaponId].left = sd::GetFormID(myWeaponLeft);
    }
  }
    if (weapon->formType == FormType::kFormType_Spell) {

    if (weaponIdCache.find(weaponId) == weaponIdCache.end()) {

      auto myWeaponRight =
        reinterpret_cast<SpellItem*>(new uint8_t[sizeof(SpellItem)]);
      auto myWeaponLeft =
        reinterpret_cast<SpellItem*>(new uint8_t[sizeof(SpellItem)]);

      memcpy(myWeaponRight, weapon, sizeof(SpellItem));
      memcpy(myWeaponLeft, weapon, sizeof(SpellItem));

      BGSEquipSlot* equipTypeRight =
        Cast::Run<BGSEquipSlot>(sd::GetFormById(FormIdEquipTypeRight));

      BGSEquipSlot* equipTypeLeft =
        Cast::Run<BGSEquipSlot>(sd::GetFormById(FormIdEquipTypeLeft));

      if (!equipTypeRight || !equipTypeLeft)
        return;

      myWeaponRight->equipType.SetEquipSlot(equipTypeRight);
      myWeaponLeft->equipType.SetEquipSlot(equipTypeLeft);

      SetFormId::Run(myWeaponLeft, GetFreeFormId::Run());
      SetFormId::Run(myWeaponRight, GetFreeFormId::Run());

      weaponIdCache[weaponId].right = sd::GetFormID(myWeaponRight);
      weaponIdCache[weaponId].left = sd::GetFormID(myWeaponLeft);
    }
  }

  if (!abLeftHand)
    weaponId = weaponIdCache[weaponId].right;

  if (abLeftHand)
    weaponId = weaponIdCache[weaponId].left;
}

void WeaponCreator::SaveTorchId(std::map<uint32_t, SlotType>& weaponIdCache,
                                uint32_t& torchId)
{
  weaponIdCache[torchId].left = torchId;
  weaponIdCache[torchId].right = torchId;
}
