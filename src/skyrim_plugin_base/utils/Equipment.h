#pragma once
#include <cstdint>
#include <foo/PropEquipment.h>

namespace Equipment_ {
	bool IsPresentInEquipment(const Equipment &equipment, uint32_t formId);
}
