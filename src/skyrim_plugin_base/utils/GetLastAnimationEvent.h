#pragma once
#include <chrono>
#include <string>

class TESObjectREFR;

class GetLastAnimationEvent
{
public:
  static std::string Run(TESObjectREFR* refr, const char* channel,
                         bool* outSuccess = nullptr);

  static std::chrono::steady_clock::duration StoringDuration()
  {
    using namespace std::chrono_literals;
    return 333ms;
  }

  static int StoringDurationMs()
  {
    return static_cast<int>(
      std::chrono::duration_cast<std::chrono::milliseconds>(StoringDuration())
        .count());
  }
};