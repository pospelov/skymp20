#include "GetFreeFormId.h"
#include <common/IPrefix.h>
#include <skse/GameForms.h>

uint32_t GetFreeFormId::Run()
{
  uint32_t id = 0xFFFFFFFF;
  while (LookupFormByID(id))
    --id;
  return id;
}