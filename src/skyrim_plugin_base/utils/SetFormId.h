#pragma once
#include <cstdint>

class TESForm;

class SetFormId
{
public:
  static void Run(TESForm* form, uint32_t newId);
};