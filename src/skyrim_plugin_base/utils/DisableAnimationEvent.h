#pragma once
#include <functional>

class DisableAnimationEvent
{
public:
  using Condition = std::function<bool(const char*)>;

  static void Run(Condition condition, bool ffOnly);

  static void DisableNpcIdles()
  {
    DisableAnimationEvent::Run(
      [](const char* animEventName) {
        return !memcmp(animEventName, "attack", 6) || !memcmp(animEventName, "Attack", 6);
      },
      true);
    DisableAnimationEvent::Run(
      [](const char* animEventName) {
        return !strcmp(animEventName, "OffsetBoundStandingPlayerInstant");
      },
      false);
    DisableAnimationEvent::Run(
      [](const char* animEventName) {
        if (!memcmp(animEventName, "Idle", 4)) { // Idle*, IdleCombat*
          if (strcmp(animEventName, "IdleStop") != 0 &&
              strcmp(animEventName, "IdleForceDefaultState") != 0) {
            return true;
          }
        }
        if (!strcmp(animEventName, "MotionDrivenIdle")) {
          return true;
        }
        return false;
      },
      true);
  }
};