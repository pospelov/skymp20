#include "ApplyHealth.h"
#include <common/IPrefix.h>
#include <scriptdragon/skyscript.h>
#include <skse/GameReferences.h>
#include <skyrim_plugin_base/utils/Cast.h>

namespace {
void SetBaseAV(Actor* actor, const char* name, float v)
{
  sd::SetActorValue(actor, const_cast<char*>(name), v);
}

void SetCurrentAV(Actor* actor, const char* name, float v)
{
  sd::ForceActorValue(actor, const_cast<char*>(name), v);
}

void ApplyPercentage(Actor* actor, char* av, float realBase,
                     float newPercentage)
{
  const auto currentValue = sd::GetActorValue(actor, av);
  const auto currentPercentage = sd::GetActorValuePercentage(actor, av);
  const auto targetValue = currentValue / currentPercentage * newPercentage;
  const auto diff = abs(targetValue - currentValue);
  if (targetValue > currentValue) {
    sd::RestoreActorValue(actor, av, diff);
  } else if (currentValue > targetValue) {
    sd::DamageActorValue(actor, av, diff);
  }
}
}

void ApplyHealth::Run(std::shared_ptr<SmartRef>& smartRef,
                      const ActorValueEntry& health)
{
  static const auto av = (char*)"Health";

  if (auto actor = Cast::Run<Actor>(smartRef->GetRef())) {

    const bool isAlive = !sd::IsDead(actor);
    const bool shouldBeAlive = health.currentPercentage > 0;

    if (isAlive) {
      if (shouldBeAlive) {
        sd::StartDeferredKill(actor);

        const float realBase = ApplyHealth::g_realBaseHealth;
        SetBaseAV(actor, av, realBase);

        ApplyPercentage(actor, av, realBase, health.currentPercentage);

      } else {
        sd::EndDeferredKill(actor);

        SetCurrentAV(actor, av, 0.f);
      }
    } else {
      if (shouldBeAlive) {
        smartRef.reset();
      }
    }
  }
}
void ApplyHealth::RunOnPlayer(const ActorValueEntry& health)
{

  ApplyPercentage(sd::GetPlayer(), (char*)"health", health.base,
                  health.currentPercentage);
}