#pragma once
#include <common/IPrefix.h>
#include <cstdint>
#include <skse/GameFormComponents.h>
#include <skse/GameReferences.h>
#include <foo/PropEquipment.h>
#include <skyrim_plugin_base/utils/GetFreeFormId.h>
#include <skyrim_plugin_base/utils/SetFormId.h>
#include <skyrim_plugin_base/utils/PlaceAtMePersistent.h>

class WeaponCreator
{
  friend class ApplyEquipment;

public:
  struct SlotType
  {
    uint32_t left = 0;
    uint32_t right = 0;
  };

  std::map<uint32_t, SlotType> weaponIdCash;

  static Equipment::EquippedWeapon GetCreatedIdByOriginId(
    uint32_t originId, WeaponCreator& weaponCreator);

  static uint32_t WeaponCreator::GetOriginIdByCreatedId(
    uint32_t _createdId, WeaponCreator& weaponCreator);

  static bool IsWeaponTwoHanded(TESObjectWEAP* weapon);

private:
  enum
  {
    FormIdEquipTypeRight = 0x00013F42,
    FormIdEquipTypeLeft = 0x00013F43,
    FormIdEquipTypeBoth = 0x00013F45,
  };

  void Run(Equipment& equipment);
  void FindOrCreateWeaponForHand(std::map<uint32_t, SlotType>& weaponIdCash,
                                 uint32_t& weaponId, bool abLeftHand);

  void SaveTorchId(std::map<uint32_t, SlotType>& weaponIdCache,
                                  uint32_t& torchId);
};