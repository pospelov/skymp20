#include "GetLoadedRefs.h"

#include <common/IPrefix.h>
#include <scriptdragon/skyscript.h>
#include <skse/GameObjects.h>
#include <skse/GameReferences.h>
#include <skyrim_plugin_base/utils/Cast.h>

// It's legacy from 2016. You don't want to modify this without adding a test.

std::vector<TESObjectREFR*> GetLoadedRefs::Run(
  uint32_t optionalCellId) noexcept
{
  const auto targetCell = Cast::Run<TESObjectCELL>(optionalCellId);

  std::vector<TESObjectREFR*> res;
  res.reserve(65536);
  auto currentCell = sd::GetParentCell(sd::GetPlayer());
  if (currentCell) {
    const auto formID = currentCell->formID;
    enum
    {
      CELL_SEARCH_DEEPTH = 40
    };
    for (auto tmpFormID =
           (formID > CELL_SEARCH_DEEPTH) ? formID - CELL_SEARCH_DEEPTH : 0;
         tmpFormID != formID + CELL_SEARCH_DEEPTH; ++tmpFormID) {
      auto tmpCell = (TESObjectCELL*)LookupFormByID(tmpFormID);
      if (tmpCell && tmpCell->formType == kFormType_Cell) {
        if (!targetCell || tmpCell == targetCell) {
          for (size_t i = 0; i != tmpCell->objectList.count; ++i) {
            auto refr = tmpCell->objectList[i];
            if (refr &&
                !(refr->flags & TESForm::kFlagUnk_0x800)) { // not disabled
              if (refr->formID < 0xff000000) {
                res.push_back(refr);
              }
            }
          }
        }
      }
    }
  }

  return res;
}