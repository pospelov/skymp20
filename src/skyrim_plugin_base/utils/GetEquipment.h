#pragma once
#include <common/IPrefix.h>
#include <cstdint>
#include <skse/GameFormComponents.h>
#include <skse/GameReferences.h>
#include <foo/PropEquipment.h>

class GetEquipment
{
public:

  static Equipment Run(Actor* me);
  static bool IsSuppotedTypeForm(TESForm* form);

private:
  static void FillEquipWeaponID(Actor * actor, Equipment& equipment);
  static void FillEquipArmorID(Actor* actor,  Equipment& equipment);
  
};