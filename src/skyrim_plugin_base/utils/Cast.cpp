#include "Cast.h"
#include <common/IPrefix.h>
#include <skse/GameObjects.h>
#include <skse/GameReferences.h>

TESForm* Cast::Run(int typeId, TESForm* form)
{
  if (form && typeId == kFormType_Reference) {
    return (form->formType == typeId || form->formType == kFormType_Character)
      ? form
      : nullptr;
  }
  return (form && form->formType == typeId) ? form : nullptr;
}

TESForm* Cast::GetFormById(uint32_t formId)
{
  return LookupFormByID(formId);
}