#include "WorldCleaner.h"
#include <ScriptDragon/enums.h>
#include <ScriptDragon/skyscript.h>
#include <common/IPrefix.h>
#include <skse/GameObjects.h>
#include <skse/GameReferences.h>
#include <skse/PapyrusObjectReference.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/GetLoadedRefs.h>
#include <unordered_map>

namespace {

TESForm* GetProduce(TESForm* pRefOrBase) noexcept
{
  if (auto ref = Cast::Run<TESObjectREFR>(pRefOrBase)) {
    return GetProduce(ref->baseForm);
  }

  if (auto tree = Cast::Run<TESFlora>(pRefOrBase)) {
    return tree->produce.produce;
  }

  if (auto tree = Cast::Run<TESObjectTREE>(pRefOrBase)) {
    return tree->produce.produce;
  }

  return nullptr;
}
}

struct WorldCleaner::Impl
{
  uint32_t cellId = 0;

  std::unordered_map<uint32_t, int> protection;
  Viet::Logging::Logger* logger = nullptr;
};

WorldCleaner::WorldCleaner() noexcept
  : pImpl(new Impl)
{
}

WorldCleaner::~WorldCleaner()
{
  delete pImpl;
}

void WorldCleaner::TickGame(Viet::Logging::Logger* logger) noexcept
{
  auto cell = sd::GetParentCell(sd::GetPlayer());
  auto cellId = cell ? cell->formID : 0;

  pImpl->logger = logger;
  if (cellId != pImpl->cellId) {
    if (logger)
      logger->Info("Cell changed from %x to %x", pImpl->cellId, cellId);
    pImpl->cellId = cellId;
    this->OnPlayerCellChange();
  }
  const NiPoint3 pos = { sd::GetPositionX(sd::GetPlayer()),
                         sd::GetPositionY(sd::GetPlayer()),
                         sd::GetPositionZ(sd::GetPlayer()) };
  for (int i = 0; i != 50; ++i)
    if (auto ac = sd::FindRandomActor(pos.x, pos.y, pos.z, 4096))
      this->DealWithRef(ac);
}

void WorldCleaner::ModProtection(uint32_t formid, int mod) noexcept
{
  bool erased = false;

  auto it = pImpl->protection.find(formid);
  if (it != pImpl->protection.end()) {
    it->second += mod;
    if (it->second == 0) {
      pImpl->protection.erase(it);
      erased = true;
    }
  } else {
    pImpl->protection[formid] = mod;
  }

  if (pImpl->logger)
    pImpl->logger->Info("Mod protection %x %d (erased=%d)", formid, mod,
                        int(erased));

  // Forces OnPlayerCellChange() on next TickGame()
  pImpl->cellId = 0;
}

void WorldCleaner::OnPlayerCellChange() noexcept
{
  auto cell = Cast::Run<TESObjectCELL>(LookupFormByID(pImpl->cellId));
  if (!cell)
    return;
  auto refs = GetLoadedRefs::Run();
  for (auto& ref : refs) {
    this->DealWithRef(ref);
  }
}

void WorldCleaner::DealWithRef(TESObjectREFR* ref) noexcept
{
  if (!ref || ref->formID == sd::GetPlayer()->formID)
    return;

  const auto type = ref->formType;
  const auto base = ref->baseForm;
  const auto baseType = base->formType;
  const auto baseId = base->formID;
  const auto id = ref->formID;

  if (type == kFormType_Character) {
    sd::BlockActivation(ref, true);
    if (pImpl->protection[id] <= 0) {
      sd::Delete(ref);
    }
    return;
  }

  if (type == kFormType_Water)
    return;
  if (baseType == kFormType_Water)
    return;
  if (baseId >= 0xff000000)
    return; // To prevent water deletion

  if (id == 0x000af671) { /* helgen exit blocker */
    sd::Delete(ref);
    return;
  }

  static BSFixedString g_emptyString = "";

  switch (baseType) {
    case kFormType_Door:
      sd::SetLockLevel(ref, 0);
      sd::BlockActivation(ref, true);
      papyrusObjectReference::SetDisplayName(ref, g_emptyString, true);
      break;
    case kFormType_Container:
      sd::SetLockLevel(ref, 0);
      sd::BlockActivation(ref, true);
      if (auto anyItem = LookupFormByID(ID_TESObjectMISC::Gold001))
        if (!sd::GetItemCount(ref, anyItem))
          sd::AddItem(ref, anyItem, 1, true);
      break;
    case kFormType_Tree:
    case kFormType_Flora:
    case kFormType_Ammo:
    case kFormType_Armor:
    case kFormType_Book:
    case kFormType_Potion:
    case kFormType_Ingredient:
    case kFormType_Key:
    case kFormType_Misc:
    case kFormType_SoulGem:
    case kFormType_Weapon:
    case kFormType_Light:
    case kFormType_ScrollItem:
      if (baseType == kFormType_Flora || baseType == kFormType_Tree) {
        if (!GetProduce(ref)) {
          break;
        } else {
          sd::BlockActivation(ref, true);
          papyrusObjectReference::SetDisplayName(ref, g_emptyString, true);
        }
      } else {
        sd::DisableNoWait(ref, false);
      }
      break;
    case kFormType_MovableStatic:
      enum
      {
        HandCart01Wheel = 0x1C0C3
      };
      if (baseId == HandCart01Wheel)
        sd::Delete(ref);
      break;
  }
}