#pragma once
#include <cstdint>
#include <vector>

// Note that this class is not tested by our test suite because it was copied
// from the hand-tested version. Please consider adding a test.

class TESObjectREFR;

class GetLoadedRefs
{
public:
  // Use returned pointers only after calling this function
  static std::vector<TESObjectREFR*> Run(uint32_t optionalCellId = 0) noexcept;
};