#pragma once
#include "ApplyMovement.h"
#include <common/IPrefix.h>
#include <foo/PropMovement.h>
#include <scriptdragon/obscript.h>
#include <scriptdragon/skyscript.h>
#include <skse/GameReferences.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/GetCellOrWorld.h>
#include <skyrim_plugin_base/utils/Misc.h>
#include <skyrim_plugin_base/utils/SetWeaponDrawn.h>
#include <skyrim_plugin_base/utils/Teleport.h>

#ifdef max
#  undef max
#endif

namespace {
enum class KeepOffsetMode
{
  Running,
  Walking
};

void KeepOffset(Actor* ref, NiPoint3 offsetPos, float offsetAngle,
                KeepOffsetMode mode)
{
  sd::KeepOffsetFromActor(ref, ref, offsetPos.x, offsetPos.y, offsetPos.z, 0.f,
                          0.f, offsetAngle,
                          mode == KeepOffsetMode::Walking ? 2048.f : 1.f, 1.f);
}
constexpr float g_rotSpeedInfinity = 0.f;
}

void ApplyMovement::Run(Actor* actor, const Movement& mov, float timeInSeconds)
{
  if (sd::IsDead(actor)) {
    return;
  }

  const NiPoint3 pos = { mov.pos[0], mov.pos[1], mov.pos[2] };

  if (GetCellOrWorld::Run(actor) != mov.cellOrWorld) {
    Teleport::Run(actor, mov.cellOrWorld, pos, { 0, 0, mov.angleZ },
                  Teleport::Wait::False);
  }

  auto distanceToPlayer = (actor->pos - sd::GetPlayer()->pos).Length();
  if (distanceToPlayer > 4096.f) {
    sd::SetPosition(actor, pos.x, pos.y, pos.z);
    return;
  }

  if (sd::IsWeaponDrawn(actor) != mov.isWeapDrawn) {
    SetWeaponDrawn::Run(actor, mov.isWeapDrawn);
  }

  float distance = (actor->pos - pos).Length();
  float speed = distance <= 512.f ? distance / timeInSeconds
                                  : std::numeric_limits<float>::max();

  if (!!sd::Obscript::IsBlocking(actor) != mov.isBlocking) {
    actor->animGraphHolder.SendAnimationEvent(
      Misc::FixedStr(mov.isBlocking ? "BlockStart" : "BlockStop"));
    actor->animGraphHolder.SendAnimationEvent(
      Misc::FixedStr(mov.isSneaking ? "SneakStart" : "SneakStop"));
  }
  if (Misc::GetAnimVar<bool>(actor, "IsSneaking") != mov.isSneaking) {
    actor->animGraphHolder.SendAnimationEvent(
      Misc::FixedStr(mov.isSneaking ? "SneakStart" : "SneakStop"));
  }
  if (sd::IsSprinting(actor) != (mov.runMode == RunMode::Sprinting)) {
    actor->animGraphHolder.SendAnimationEvent(Misc::FixedStr(
      mov.runMode == RunMode::Sprinting ? "SprintStart" : "SprintStop"));
  }

  sd::SetHeadTracking(actor, false);

  if (mov.runMode == RunMode::Standing) {
    float realAngle = sd::GetAngleZ(actor);
    float angleDelta = mov.angleZ - realAngle;

    if (abs(angleDelta) < 5)
      angleDelta = 0;

    KeepOffset(actor, { 0, 0, 0 }, angleDelta, KeepOffsetMode::Running);

    if (mov.isInJumpState || distance > 64 || abs(angleDelta) > 80.f)
      sd::TranslateTo(actor, pos.x, pos.y, pos.z, 0, 0, mov.angleZ, speed,
                      g_rotSpeedInfinity);
  } else {
    sd::TranslateTo(actor, pos.x, pos.y, pos.z, 0, 0, mov.angleZ, speed,
                    g_rotSpeedInfinity);

    float offsetZ = 0.f;
    if (mov.runMode == RunMode::Walking)
      offsetZ = -512.f;
    else if (mov.runMode == RunMode::Running)
      offsetZ = -1024.f;

    const NiPoint3 offset(
      3.f * std::sin(mov.direction / 180.f * std::acosf(-1)),
      3.f * std::cos(mov.direction / 180.f * std::acosf(-1)), offsetZ);

    KeepOffset(actor, offset, 0,
               mov.runMode == RunMode::Walking ? KeepOffsetMode::Walking
                                               : KeepOffsetMode::Running);
  }
}
