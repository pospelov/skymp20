#pragma once
#include "SkyrimPlugin.h"
#include <functional>

class InputListener;

namespace DInputHook {
bool HookDirectInput(const InputListener& listener) noexcept;
void SetClickFloodActive(bool active) noexcept;
bool WasDangerousButtonDown(int ms) noexcept;
}