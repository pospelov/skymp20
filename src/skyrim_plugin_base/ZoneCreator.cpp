#include "ZoneCreator.h"
#include <algorithm>
#include <cassert>
#include <ctime>
#include <math.h>
#include <mutex>
#include <optional>
#include <scriptdragon/enums.h>
#include <scriptdragon/skyscript.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <stdexcept>
#include <thread>

namespace {
void SetWeather(TESWeather* weather)
{
  if (weather && weather->formType == FormType::kFormType_Weather &&
      sd::GetCurrentWeather() != weather) {
    sd::ReleaseOverride();
    sd::ForceActive(weather, true);
  }
}

__forceinline float sqr(float x)
{
  return x * x;
}

float LengthNoZ(const NiPoint3& l, const NiPoint3& r)
{
  return std::sqrt(sqr(l.x - r.x) + sqr(l.y - r.y));
}

NiPoint3 FindClosestNavMeshPos(NiPoint3 pos, uint32_t worldSpaceId,
                               const espm::CombineBrowser& espmBrowser)
{
  NiPoint3 bestVertice;
  float minDistance = std::numeric_limits<float>::infinity();

  for (int16_t i = -2; i <= 2; ++i) {
    for (int16_t j = -2; j <= 2; ++j) {
      espm::CellOrGridPos gridPos;
      gridPos.pos.x = i + int16_t(pos.x / 4096);
      gridPos.pos.y = j + int16_t(pos.y / 4096);

      auto [navmeshesBegin, navmeshesSize] =
        espmBrowser.FindNavMeshes(worldSpaceId, gridPos);
      for (auto it = navmeshesBegin; it != navmeshesBegin + navmeshesSize;
           ++it) {

        auto& rec = *it;

        if (!rec) {
          continue;
        }

        if (!(rec->GetType() == "NAVM")) {
          continue;
        }

        auto recordNavmesh = reinterpret_cast<const espm::NAVM*>(rec);

        auto& vertices =
          recordNavmesh->GetData(espmBrowser.GetCache()).vertices;
        if (!vertices) {
          continue;
        }

        for (auto& p : *vertices) {
          const NiPoint3& vertice = *(NiPoint3*)&p;

          float distance = LengthNoZ(vertice, pos);
          if (distance < minDistance) {
            minDistance = distance;
            bestVertice = vertice;
          }
        }
      }
    }
  }

  return bestVertice;
}
}

FireWallRef::FireWallRef(TESForm* base)
{
  auto spawner = sd::GetPlayer();
  auto me = sd::PlaceAtMe(spawner, base, 1, true, true);

  m_refId = me ? me->formID : 0;
  m_base = base;
}

FireWallRef::~FireWallRef()
{
  if (auto ref = Cast::Run<TESObjectREFR>(m_refId)) {
    if (sd::GetBaseObject(ref) == m_base) {
      sd::Disable(ref, false);
      sd::Delete(ref);
    }
  }
}

TESObjectREFR* FireWallRef::GetRef() const noexcept
{
  return Cast::Run<TESObjectREFR>(m_refId);
}

void FireWallRef::Enable(bool abFadeIn) const noexcept
{
  if (auto me = this->GetRef())
    sd::Enable(me, abFadeIn);
}

NiPoint3 ZoneCreator::GetPositionPointOnCircle(float radius,
                                               const NiPoint3& centrPos,
                                               float degrees)
{
  float radian = degrees * (acosf(-1) / 180);
  float x = radius * cos(radian) + centrPos.x;
  float y = radius * sin(radian) + centrPos.y;

  return { x, y, centrPos.z };
}

NiPoint3 ZoneCreator::GetRotationPointOnCircle(float x, float y,
                                               const NiPoint3& centrPos)
{
  float z =
    ((atan2f(y - centrPos.y, x - centrPos.x) * (180 / acosf(-1))) * -1) + 45;
  return { 0, 0, z };
}

std::vector<ZoneCreator::WallPosition> ZoneCreator::GetRequiredFireWalls(
  const NiPoint3& center, float radius)
{
  std::vector<WallPosition> res;

  float circleLength = 2 * acosf(-1) * radius;
  float numNeedPoints = circleLength / 475.f;
  float needSteps = numNeedPoints * 475.f;

  res.reserve(numNeedPoints);

  for (float i = 0.f; i < needSteps; i += 475.f) {
    float degrees = (i / circleLength) * 360;

    WallPosition wallPos;

    wallPos.pos = GetPositionPointOnCircle(radius, center, degrees);
    wallPos.rot =
      GetRotationPointOnCircle(wallPos.pos.x, wallPos.pos.y, center);

    res.push_back(wallPos);
  }

  return res;
}

bool ZoneCreator::PointInCircle(const NiPoint3& point, float radius,
                                const NiPoint3& centrPos)
{

  float x = point.x - centrPos.x;
  float y = point.y - centrPos.y;
  float hypotenuse = sqrt(x * x + y * y);
  return hypotenuse > radius ? false : true;
}

float ZoneCreator::GetCurrentRadius(const NiPoint3& centr,
                                    const NiPoint3& point)
{
  return sqrt(pow(point.x - centr.x, 2) + pow(point.y - centr.y, 2));
}

void ZoneCreator::MoveToNavMeshZ(ZoneCreator::FireWallState& wallsState,
                                 const espm::CombineBrowser& espmBrowser,
                                 uint32_t worldSpaceId)
{
  for (auto& wall : wallsState.walls) {
    if (wall.wallRef && LengthNoZ(wall.lastPos, wall.position) > 128.f) {
      wall.lastPos = wall.position;
      auto navmeshPos =
        FindClosestNavMeshPos(wall.position, worldSpaceId, espmBrowser);
      if (navmeshPos.Length()) {
        float waterZ = FireWallState::WaterHeight;
        wall.position.z = max(waterZ, navmeshPos.z);
      }
    }
  }
}

void ZoneCreator::CreateFireWalls(ZoneCreator::FireWallState& wallsState,
                                  float radius, const NiPoint3& center)
{
  wallsState.walls.clear();

  auto points = GetRequiredFireWalls(center, radius);
  for (auto& point : points) {
    FireWallInfo fwInfo;
    fwInfo.position = point.pos;

    fwInfo.rotation = point.rot;
    wallsState.walls.push_back(fwInfo);
  }
}

void ZoneCreator::CreateNewPositionForFireWalls(
  ZoneCreator::FireWallState& wallsState, float radius, float previousRadius,
  const NiPoint3& centrPos)
{
  assert(previousRadius >= 0);
  assert(radius <= previousRadius);

  float deltaRadius = radius - previousRadius;

  auto points = GetRequiredFireWalls(centrPos, radius);

  for (size_t i = 0; i < wallsState.walls.size(); ++i) {
    auto& wall = wallsState.walls[i];
    if (i < points.size()) {
      wall.position = { points[i].pos.x, points[i].pos.y, wall.position.z };
      wall.rotation.z = points[i].rot.z;
    } else {
      wall.position = { 0, 0, 0 };
      wall.rotation = { 0, 0, 0 };
      wall.wallRef.reset();
    }
  }
}

void ZoneCreator::Run(FireWallState& wallsState, float radius,
                      const NiPoint3& centrPos, TESForm* base,
                      const espm::CombineBrowser& espmBrowser,
                      TESWeather* weather)
{
  auto wp = sd::GetWorldSpace(sd::GetPlayer());
  if (!wp)
    return;

  if (!base)
    return;

  if (radius > wallsState.previousRadius) {
    wallsState = {};
  }

  if (wallsState.walls.empty()) {

    CreateFireWalls(wallsState, radius, centrPos);

    wallsState.previousRadius = radius;
    wallsState.previousCenter = centrPos;
    return;
  }

  const NiPoint3 playerPos = { sd::GetPositionX(sd::GetPlayer()),
                               sd::GetPositionY(sd::GetPlayer()),
                               sd::GetPositionZ(sd::GetPlayer()) };

  for (auto& wall : wallsState.walls) {

    const bool needsToBeVisible = LengthNoZ(playerPos, wall.position) < 7000.f;

    if (needsToBeVisible) {
      if (!wall.wallRef) {
        wall.wallRef.reset(new FireWallRef(base));

        auto navmeshPos =
          FindClosestNavMeshPos(wall.position, wp->formID, espmBrowser);

        if (navmeshPos.Length()) {
          float waterZ = FireWallState::WaterHeight;
          wall.position.z = max(waterZ, navmeshPos.z);
        }

        wall.lastPos = wall.position;
        wall.FallIntoPlace();
      }
    } else {
      wall.wallRef.reset();
    }
  }

  wallsState.walls.erase(
    std::unique(wallsState.walls.begin(), wallsState.walls.end(),
                [](const FireWallInfo& left, const FireWallInfo& right) {
                  return (left.position - right.position).Length() < 1.f;
                }),
    wallsState.walls.end());

  MoveToNavMeshZ(wallsState, espmBrowser, wp->formID);

  if (wallsState.previousRadius > radius && radius >= 500) {
    CreateNewPositionForFireWalls(wallsState, radius,
                                  wallsState.previousRadius, centrPos);
    wallsState.previousRadius = radius;
    wallsState.previousCenter = centrPos;
    wallsState.StartMoving();
  }

  wallsState.isPlayerInside = PointInCircle(
    playerPos,
    GetCurrentRadius(centrPos, wallsState.GetPositionFirstWall()) - 128.f,
    centrPos);

  if (!wallsState.isPlayerInside) {
    if (SpellItem* torchBashFireSpell = Cast::Run<SpellItem>(
          LookupFormByID(ID_SpellItem::TorchBashFireSpell))) {

      auto& effects = torchBashFireSpell->effectItemList;
      bool hasEffect = false;

      for (size_t i = 0; i < effects.count; ++i) {
        if (effects[i] && effects[i]->mgef &&
            sd::HasMagicEffect(sd::GetPlayer(), effects[i]->mgef)) {
          hasEffect = true;
          break;
        }
      }

      if (!hasEffect)
        sd::Cast(torchBashFireSpell, sd::GetPlayer(), sd::GetPlayer());
    }

    SetWeather(Cast::Run<TESWeather>(
      LookupFormByID(ID_TESWeather::HelgenAttackWeather)));
  }

  if (wallsState.isPlayerInside) {
    SetWeather(weather);
  }
}

void ZoneCreator::FireWallInfo::FallIntoPlace()
{
  if (!wallRef)
    return;

  if (auto me = wallRef->GetRef()) {
    sd::SetPosition(me, position.x, position.y, position.z);

    sd::SetAngle(me, me->rot.x, me->rot.y, rotation.z);
    wallRef->Enable(false);
  }
}

void ZoneCreator::FireWallInfo::StartMoving()
{
  const float rotSpeed = 0.f;

  if (!wallRef)
    return;
  if (auto me = wallRef->GetRef()) {

    NiPoint3 posWas = me->pos;
    const float movSpeed = (me->pos - position).Length() / (0.2f);
    sd::TranslateTo(me, position.x, position.y, position.z, me->rot.x,
                    me->rot.y, rotation.z, movSpeed, rotSpeed);
  }
}

void ZoneCreator::FireWallState::StartMoving()
{
  for (auto& wall : walls) {
    wall.StartMoving();
  }
}

NiPoint3 ZoneCreator::FireWallState::GetPositionFirstWall()
{
  NiPoint3 res;

  if (!walls.empty())
    res = walls[0].position;

  return res;
}
