#include "CefFunctions.h"
#include <Windows.h>
#include <cassert>
#include <stdexcept>
#include <thread>

void MyOnPaint(void* ctx, const void* buffer, uint32_t size)
{
  if (!ctx)
    return;

  auto onPaint = *(CefFunctions::OnPaintF*)ctx;
  onPaint(buffer, size);
}

CefFunctions CefFunctions::Setup(OnPaintF onPaint)
{
  CefFunctions cefFunctions;

  auto skymp_cef_renderer = LoadLibraryA("Data/Multiplayer/skymp_cef_renderer.dll");
  if (!skymp_cef_renderer)
    throw std::runtime_error("LoadLibrary skymp_cef_renderer.dll failed");

  auto cefMain = (SkympCef_Configure*)GetProcAddress(skymp_cef_renderer,
                                                     "SkympCef_Configure");
  if (!cefMain)
    throw std::runtime_error("cefMain is nullptr");

  cefFunctions.cefMain = cefMain;

  cefFunctions.cefTick =
    (SkympCef_Tick*)GetProcAddress(skymp_cef_renderer, "SkympCef_Tick");
  cefFunctions.cefSetOnPaint = (SkympCef_SetOnPaint*)GetProcAddress(
    skymp_cef_renderer, "SkympCef_SetOnPaint");
  cefFunctions.cefSetFocus = (SkympCef_SetFocus*)GetProcAddress(
    skymp_cef_renderer, "SkympCef_SetFocus");
  cefFunctions.cefOnMouseStateChange =
    (SkympCef_OnMouseStateChange*)GetProcAddress(
      skymp_cef_renderer, "SkympCef_OnMouseStateChange");
  cefFunctions.cefOnMouseMove = (SkympCef_OnMouseMove*)GetProcAddress(
    skymp_cef_renderer, "SkympCef_OnMouseMove");
  cefFunctions.cefOnMouseWheel = (SkympCef_OnMouseWheel*)GetProcAddress(
    skymp_cef_renderer, "SkympCef_OnMouseWheel");
  cefFunctions.cefOnKeyStateChange =
    (SkympCef_OnKeyStateChange*)GetProcAddress(skymp_cef_renderer,
                                               "SkympCef_OnKeyStateChange");
  cefFunctions.cefOnChar =
    (SkympCef_OnChar*)GetProcAddress(skymp_cef_renderer, "SkympCef_OnChar");
  cefFunctions.cefLoadUrl =
    (SkympCef_LoadUrl*)GetProcAddress(skymp_cef_renderer, "SkympCef_LoadUrl");
  cefFunctions.cefExecuteJs = (SkympCef_ExecuteJs*)GetProcAddress(
    skymp_cef_renderer, "SkympCef_ExecuteJs");
  cefFunctions.cefGetCPRequiredBufSize =
    (SkympCef_GetCPRequiredBufSize*)GetProcAddress(
      skymp_cef_renderer, "SkympCef_GetCPRequiredBufSize");
  cefFunctions.cefNextOutCP = (SkympCef_NextOutCP*)GetProcAddress(
    skymp_cef_renderer, "SkympCef_NextOutCP");

  if (!cefFunctions.cefSetOnPaint) {
    throw std::runtime_error("cefSetOnPaint is nullptr");
  }

  cefFunctions.cefSetOnPaint(new OnPaintF(onPaint), MyOnPaint);

  return cefFunctions;
}