#pragma once
#include "HitEvent.h"
#include "HookListeners.h"
#include <common/IPrefix.h>
#include <functional>
#include <skse/GameEvents.h>
#include <skse/GameReferences.h>

class HitEventSink : public BSTEventSink<TESHitEvent>
{
public:
  using Callback = std::function<void(const HitEvent&)>;

  static void Install(Callback onHit)
  {
    g_hitEventDispatcher->AddEventSink(new HitEventSink(onHit));
  }

  HitEventSink(Callback onHit_) { onHit = onHit_; }

  virtual ~HitEventSink() override {}

private:
  Callback onHit;

  virtual EventResult ReceiveEvent(
    TESHitEvent* evn, EventDispatcher<TESHitEvent>* source) override
  {
    const HitEvent data{ (evn->caster ? evn->caster->formID : 0),
                         (evn->target ? evn->target->formID : 0),
                         (evn->sourceFormID),
                         (evn->projectileFormID),
                         !!(evn->flags & TESHitEvent::kFlag_PowerAttack),
                         !!(evn->flags & TESHitEvent::kFlag_SneakAttack),
                         !!(evn->flags & TESHitEvent::kFlag_Bash),
                         !!(evn->flags & TESHitEvent::kFlag_Blocked) };
    onHit(data);
    return EventResult::kEvent_Continue;
  }
};