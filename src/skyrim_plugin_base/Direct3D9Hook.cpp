#include "Direct3D9Hook.h"
#include "MyDirect3D9.h"
#include "MyDirect3DDevice9.h"
#include <cassert>
#include <d3d9.h>
#include <d3dx9.h>
#include <mhook-lib/mhook.h>

namespace {
class Handler final : public MyDirect3D9RenderHandler
{
public:
  Handler(void* ctx, Direct3D9Hook::OnRender onPresent)
    : m_onPresent(onPresent)
    , m_ctx(ctx)
  {
  }

  void OnPresent(void* device) override
  {
    m_onPresent(m_ctx, reinterpret_cast<IDirect3DDevice9*>(device));
  }

  void OnReset(void* device) override {}

private:
  const Direct3D9Hook::OnRender m_onPresent;
  void* const m_ctx;
};
}

MyDirect3DDevice9* g_pIDirect3DDevice9 = nullptr;
MyDirect3D9* g_pIDirect3D9 = nullptr;
std::shared_ptr<Handler> g_handler;

typedef IDirect3D9*(WINAPI* tDirect3DCreate9)(UINT);
tDirect3DCreate9 Direct3DCreate9_RealFunc;

IDirect3D9* WINAPI FakeDirect3DCreate9(UINT SDKVersion)
{
  IDirect3D9* pIDirect3D9 = Direct3DCreate9_RealFunc(SDKVersion);
  g_pIDirect3D9 = new MyDirect3D9(pIDirect3D9, g_handler);
  return (g_pIDirect3D9);
}

std::optional<std::runtime_error> Direct3D9Hook::HookDirect3D9(
  const OnRender& onRender, void* ctx) noexcept
{

  HMODULE d3d9 = GetModuleHandleA("d3d9.dll");
  if (!d3d9) {
    return std::runtime_error(
      "GetModuleHandleA(\"d3d9.dll\") failed with code " +
      std::to_string(GetLastError()));
  }

  Direct3DCreate9_RealFunc =
    (tDirect3DCreate9)GetProcAddress(d3d9, "Direct3DCreate9");
  if (!Direct3DCreate9_RealFunc) {
    return std::runtime_error(
      "GetProcAddress(d3d9, \"Direct3DCreate9\") failed with code " +
      std::to_string(GetLastError()));
  }

  g_handler.reset(new Handler(ctx, onRender));

  BOOL success =
    Mhook_SetHook((PVOID*)&Direct3DCreate9_RealFunc, FakeDirect3DCreate9);
  if (!success)
    return std::runtime_error("Mhook_SetHook failed");
  return std::nullopt;
}