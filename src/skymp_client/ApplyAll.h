#pragma once
#include <cstdint>
#include <map>
#include <memory>
#include <string>
#include <viet/Client.h>

class SmartRef;
class WeaponCreator;
class TESObjectREFR;

class ApplyAll
{
public:
  struct State
  {
    struct PerProp
    {
      uint64_t numChanges = 0;
    };

    std::map<std::string, PerProp> perProp;
  };

  struct Arguments
  {
    TESObjectREFR* const ref;
    const Viet::IWorldState::Instance& vietActor;
    ApplyAll::State& st;
    WeaponCreator& weaponCreator;
    std::shared_ptr<SmartRef>& smartRef;
  };

  static void Run(Arguments& arguments);
};