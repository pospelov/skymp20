#include "ApplyAll.h"
#include <common/IPrefix.h>
#include <foo/PropAnimEvent.h>
#include <foo/PropEquipment.h>
#include <foo/PropMovement.h>
#include <skse/GameReferences.h>
#include <skyrim_plugin_base/utils/ApplyEquipment.h>
#include <skyrim_plugin_base/utils/ApplyHealth.h>
#include <skyrim_plugin_base/utils/ApplyMovement.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/Misc.h>
#include <skyrim_plugin_base/utils/SendAnimEvent.h>
#include <skyrim_plugin_base/utils/SmartRef.h>

namespace {

void RunApplyMovement(ApplyAll::Arguments& args)
{
  auto numChanges = args.vietActor.GetValue(PMovement::name).numChanges;
  auto& perProp = args.st.perProp[PMovement::name];
  if (perProp.numChanges != numChanges) {

    if (auto mov = args.vietActor.GetValue<PMovement>()) {
      auto v = *mov;
      if (args.vietActor.IsMe()) {
        v.pos[0] += 128;
        v.pos[1] += 128;
      }

      if (auto actor = Cast::Run<Actor>(args.ref))
        ApplyMovement::Run(actor, v);
    }

    perProp.numChanges = numChanges;
  }
}

void RunApplyAnimation(ApplyAll::Arguments& args)
{
  auto numChanges = args.vietActor.GetValue(PAnimEvent::name).numChanges;
  auto& perProp = args.st.perProp[PAnimEvent::name];
  if (perProp.numChanges != numChanges) {
    if (auto anim = args.vietActor.GetValue<PAnimEvent>()) {
      SendAnimEvent::Run(args.ref, anim->value.data());
    }
    perProp.numChanges = numChanges;
  }
}

void RunApplyEquipment(ApplyAll::Arguments& args)
{
  auto actor = Cast::Run<Actor>(args.ref);
  if (!actor)
    return;

  if (auto eq = args.vietActor.GetValue<PEquipment>())
    ApplyEquipment::Run(actor, *eq, args.weaponCreator);
}

void RunApplyHealth(ApplyAll::Arguments& args)
{
  auto actor = Cast::Run<Actor>(args.ref);
  if (!actor)
    return;

  if (auto health = args.vietActor.GetValue<PAVHealth>()) {
    ApplyHealth::Run(args.smartRef, *health);
  }
}
}

void ApplyAll::Run(Arguments& args)
{
  RunApplyMovement(args);
  RunApplyAnimation(args);
  RunApplyEquipment(args);
  RunApplyHealth(args);
}