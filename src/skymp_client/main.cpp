#define SKYRIM_PLUGIN_NAME "skymp_client"

#include <common/IPrefix.h>
#include <skse/GameMenus.h>
#include <skse/GameObjects.h>
#include <skyrim_plugin_base/MyDirect3DDevice9.h>
#include <skyrim_plugin_base/ZoneCreator.h>
#include <skyrim_plugin_base/utils/ApplyEquipment.h>
#include <skyrim_plugin_base/utils/ApplyHealth.h>
#include <skyrim_plugin_base/utils/ApplyMovement.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/DisableAnimationEvent.h>
#include <skyrim_plugin_base/utils/FindClosestRef.h>
#include <skyrim_plugin_base/utils/GetCellOrWorld.h>
#include <skyrim_plugin_base/utils/GetEquipment.h>
#include <skyrim_plugin_base/utils/GetLastAnimationEvent.h>
#include <skyrim_plugin_base/utils/GetMousePos.h>
#include <skyrim_plugin_base/utils/GetMovement.h>
#include <skyrim_plugin_base/utils/LoadGame.h>
#include <skyrim_plugin_base/utils/Misc.h>
#include <skyrim_plugin_base/utils/PlaceAtMePersistent.h>
#include <skyrim_plugin_base/utils/SmartRef.h>
#include <skyrim_plugin_base/utils/Teleport.h>
#include <skyrim_plugin_base/utils/Wait.h>
#include <skyrim_plugin_base/utils/WeaponCreator.h>
#include <skyrim_plugin_base/utils/WorldCleaner.h>

#include "ApplyAll.h"
#include "Sender.h"
#include <atomic>
#include <foo/LogsStorage.h>
#include <foo/PropActorValues.h>
#include <foo/PropAnimEvent.h>
#include <foo/PropHit.h>
#include <foo/RandomString.h>
#include <foo/VietApiWrapper.h>
#include <foo/VietClientWrapper.h>
#include <foo/VietServerWrapper.h>
#include <foo/foo.h>
#include <scriptdragon/skyscript.h>
#include <skyrim_plugin_base/SkyrimPluginMain.h>
#include <sstream>
#include <viet/Client.h>

#include <imgui.h>
#include <imgui_dx9_app_base/ImGuiContext.h>
#include <imgui_widgets/DebugView.h>
#include <imgui_widgets/MovementComparison.h>
#include <skse/CommandTable.h>
#include <skse/GameForms.h>
#include <skse/GameInput.h>
#include <skse/GameReferences.h>

#include <scriptdragon/enums.h>

struct HardcodedFeaturesData
{
  std::atomic<float> wheelAbsolute;
};

class HardcodedFeatures
{
public:
  void Update(HardcodedFeaturesData& hardcodedFeaturesData)
  {
    UpdateControls();
    UpdateSkills();
    UpdateItems();
    UpdateEquippedFormIndex(hardcodedFeaturesData);
  }

private:
  void UpdateControls()
  {
    sd::DisablePlayerControls(false, false, true, false, false, true, false,
                              true, 0);
    if (sd::IsDead(sd::GetPlayer()) == false)
      sd::ForceFirstPerson();
  }

  void UpdateSkills()
  {
    auto skillsData = (*g_thePlayer)->skills->data;
    for (auto& skill : skillsData->levelData)
      skill.points = 0;
  }

  void UpdateItems()
  {
    bool leftHand = true;
    if (auto weap = sd::GetEquippedWeapon(sd::GetPlayer(), leftHand))
      sd::UnequipItem(sd::GetPlayer(), weap, false, true);

    if (m_equippedForm.index >= m_equipableForms.size())
      m_equippedForm.index = 0;

    auto weap = sd::GetEquippedWeapon(sd::GetPlayer(), !leftHand);
    auto weapId = weap ? weap->formID : 0;

    auto spell = sd::GetEquippedSpell(sd::GetPlayer(), 1);
    auto spellId = spell ? spell->formID : 0;

    if (weapId != m_equipableForms[m_equippedForm.index] &&
        spellId != m_equipableForms[m_equippedForm.index]) {
      if (weap)
        sd::UnequipItem(sd::GetPlayer(), weap, false, true);
      if (spell)
        sd::UnequipSpell(sd::GetPlayer(), spell, 1);

      auto requiredForm =
        LookupFormByID(m_equipableForms[m_equippedForm.index]);
      if (auto requiredSpell = Cast::Run<SpellItem>(requiredForm)) {
        sd::EquipSpell(sd::GetPlayer(), requiredSpell, 1);
      }
      if (auto requiredWeap = Cast::Run<TESObjectWEAP>(requiredForm)) {
        sd::EquipItem(sd::GetPlayer(), requiredWeap, false, true);
      }
    }
  }

  void UpdateEquippedFormIndex(HardcodedFeaturesData& hardcodedFeaturesData)
  {
    const float delta =
      hardcodedFeaturesData.wheelAbsolute - m_mouseWheelAbsolute;

    if (m_mouseWheelAbsolute != 0) {

      if (delta > 0) {
        TryIncrementEquippedFormIndex();
      }

      if (delta < 0) {
        TryDecrementEquippedFormIndex();
      }
    }

    m_mouseWheelAbsolute = hardcodedFeaturesData.wheelAbsolute;
  }

  void TryIncrementEquippedFormIndex()
  {
    if (clock() - m_equippedForm.lastChange <
        m_equippedForm.changeMinimumInterval)
      return;
    m_equippedForm.lastChange = clock();

    ++m_equippedForm.index;
    if (m_equippedForm.index >= m_equipableForms.size())
      m_equippedForm.index = 0;
  }

  void TryDecrementEquippedFormIndex()
  {
    if (clock() - m_equippedForm.lastChange <
        m_equippedForm.changeMinimumInterval)
      return;
    m_equippedForm.lastChange = clock();

    if (m_equippedForm.index == 0)
      m_equippedForm.index = m_equipableForms.size() - 1;
    else
      --m_equippedForm.index;
  }

  struct
  {
    size_t index = 0;
    clock_t lastChange = 0;
    const int changeMinimumInterval = 1 * CLOCKS_PER_SEC;
  } m_equippedForm;

  float m_mouseWheelAbsolute = 0;

  enum
  {
    IronSword = 0x12eb7,
    Flames = 0x12fcd
  };

  std::vector<uint32_t> m_equipableForms = { IronSword, Flames };
};

namespace {
void ExitWithError(std::string e)
{
  MessageBoxA(0, e.data(), "Unhandled exception", MB_ICONERROR);
  ExitProcess(1);
}
}

class SkympClient final : public SkyrimPlugin::Plugin
{
public:
  struct PropValue
  {
    std::unique_ptr<Viet::IPropertyValue> value;
    std::optional<uint64_t> numChanges;
  };

  enum class GameState
  {
    MainMenu,
    SpawningStarted
  };

  struct
  {
    std::mutex m;
    std::shared_ptr<VietServerWrapper> server;
    std::shared_ptr<VietApiWrapper> api;
  } m_share;

  struct
  {
    std::mutex m;
    std::shared_ptr<VietClientWrapper> client;
    std::string nickname;
    std::map<std::string, PropValue> myPropValues;
    GameState gameState = GameState::MainMenu;
  } m_share2;

  struct
  {
    std::mutex m;
    std::list<HitEvent> hitEvents;
  } m_share3;

  std::shared_ptr<DebugView> m_debugView;
  bool m_showDebug = false;
  bool m_showDebugPressedWas = false;
  LogsStorage m_logsStorage;

  HardcodedFeaturesData m_hardcodedFeaturesData;

  volatile uint32_t m_sdThread = 0;

  auto Catch()
  {
    return [](const char* err) { ExitWithError(err); };
  }

  void OnHit(const HitEvent& hitEvent) override
  {
    std::lock_guard l(m_share3.m);
    m_share3.hitEvents.push_back(hitEvent);
  }

  void HelperThreadMain() override
  {
    try {
      while (1) {
        Sleep(1);
        this->TickClientHelperThread();

        {
          std::lock_guard l(m_share.m);
          if (m_share.server)
            m_share.server->Tick();
          if (m_share.api)
            m_share.api->Tick();
        }

        {
          std::lock_guard l(m_share2.m);
          if (m_share2.client) {
            m_share2.client->Get().Tick();
          }
        }
      }
    } catch (std::exception& e) {
      ExitWithError(e.what());
    }
  }

  void TickClientHelperThread()
  {
    std::lock_guard l(m_share2.m);

    if (m_share2.gameState == GameState::SpawningStarted)
      return;

    if (!m_share2.client)
      return;

    m_share2.client->Get().ProcessWorldState(
      [&](const Viet::IWorldState* wst) {
        auto mov = wst->GetMyPropertyValue<PMovement>();
        if (mov) {
          auto n = wst->GetMyPropertyValue(PMovement::name).numChanges;
          auto& prevMov = m_share2.myPropValues[PMovement::name];
          if (!prevMov.numChanges || *prevMov.numChanges != n) {
            prevMov.numChanges = n;
            prevMov.value = mov->Clone();

            if (m_share2.gameState == GameState::MainMenu) {
              m_share2.gameState = GameState::SpawningStarted;

              std::stringstream ss;
              ss << "Spawning at " << Movement::ToJson(*mov).dump();
              auto str = ss.str();
              m_logsStorage.WriteLogs("Skymp")(Viet::Logging::Severity::Notice,
                                               str.data());

              LoadGame::Run(mov->pos, { 0, 0, mov->angleZ }, mov->cellOrWorld);
            }
          }
        }
      });
  }

  struct ActorData
  {
    std::shared_ptr<SmartRef> smartRef;
    ApplyAll::State applyAllState;
  };

  void GameThreadMain() override
  {
    m_sdThread = GetCurrentThreadId();

    using namespace std::chrono_literals;

    FindClosestRef::Cache findClosestRefCache;
    WeaponCreator weaponCreator;
    std::shared_ptr<WorldCleaner> worldCleaner(new WorldCleaner);
    std::shared_ptr<HardcodedFeatures> hardcoded(new HardcodedFeatures);

    Sender sender;

    std::vector<ActorData> ac;
    bool showMe = false;
    bool pressedWas = false;
    bool pressedWas4 = false;

    ZoneCreator::FireWallState fireWall;
    auto form = sd::GetFormById(0xEF77F);
    float radius = 5000;
    auto centerPos = NiPoint3(sd::GetPositionX(sd::GetPlayer()),
                              sd::GetPositionY(sd::GetPlayer()),
                              sd::GetPositionZ(sd::GetPlayer()));

    TESWeather* weatherForFireZone =
      Cast::Run<TESWeather>(LookupFormByID(ID_TESWeather::SkyrimStormRainFF));

    try {
      DisableAnimationEvent::DisableNpcIdles();

      while (1) {
        Wait::Run();

        hardcoded->Update(m_hardcodedFeaturesData);
        worldCleaner->TickGame(nullptr);

        decltype(m_share3.hitEvents) hitEvents;
        {
          std::lock_guard l(m_share3.m);
          hitEvents = std::move(m_share3.hitEvents);
          m_share3.hitEvents.clear();
        }

        std::lock_guard l(m_share2.m);

        sender.Tick(m_share2.client->Get());

        if (sd::GetKeyPressed(VK_F1))
          for (auto& v : ac)
            v.smartRef.reset();

        {
          bool pressed = sd::GetKeyPressed(VK_F2);
          if (pressed != pressedWas) {
            pressedWas = pressed;
            if (pressed)
              showMe = !showMe;
          }
        }
        ZoneCreator::Run(fireWall, radius, centerPos, form,
                         this->GetEspmBrowser(), weatherForFireZone);

        if (sd::GetKeyPressed(VK_F3)) {
          radius = radius - 10;
          Console_Print("Radius %f", radius);
        }

        {
          bool pressed = sd::GetKeyPressed(VK_F4);
          if (pressed != pressedWas4) {
            pressedWas4 = pressed;
            if (pressed)
              radius = radius + 500;
          }
        }

        if (sd::GetKeyPressed('5')) {
          centerPos = NiPoint3(sd::GetPositionX(sd::GetPlayer()),
                               sd::GetPositionY(sd::GetPlayer()),
                               sd::GetPositionZ(sd::GetPlayer()));
        }

        m_share2.client->Get().ProcessWorldState(
          [&](const Viet::IWorldState* wst) {
            auto myHealth = wst->GetMyPropertyValue<PAVHealth>();
            if (myHealth) {
              auto n = wst->GetMyPropertyValue(PAVHealth::name).numChanges;
              auto& prev = m_share2.myPropValues[PAVHealth::name];
              if (!prev.numChanges || *prev.numChanges != n) {
                prev.numChanges = n;
                prev.value = myHealth->Clone();

                auto myHealth = wst->GetMyPropertyValue(PAVHealth::name);
                if (auto myHealthValue = myHealth.As<PAVHealth::Value>()) {
                  ApplyHealth::RunOnPlayer(*myHealthValue);
                }
              }
            }

            auto actors = wst->GetInstances(Entities::actor);
            for (uint32_t i = 0; i < actors.GetPoolSize(); ++i) {
              if (ac.size() <= i)
                ac.resize(i + 1);

              auto actor = actors[i];
              if (!actor.IsValid() || (!showMe && actor.IsMe())) {
                ac[i] = {};
                continue;
              }

              auto mov = actor.GetValue<PMovement>();
              if (!mov ||
                  mov->cellOrWorld != GetCellOrWorld::Run(sd::GetPlayer())) {
                ac[i] = {};
                continue;
              }

              if (!ac[i].smartRef) {
                std::optional<Equipment> equipment;
                if (auto v = actor.GetValue<PEquipment>())
                  equipment = *v;

                ac[i].smartRef.reset(new SmartRef(
                  NiPoint3({ mov->pos[0], mov->pos[1], mov->pos[2] }),
                  findClosestRefCache, equipment, worldCleaner));
              }

              auto ref = Cast::Run<Actor>(ac[i].smartRef->GetRef());
              if (!ref) {
                ac[i] = {};
                continue;
              }

              ApplyAll::Arguments args{ ref, actor, ac[i].applyAllState,
                                        weaponCreator, ac[i].smartRef };
              ApplyAll::Run(args);

              for (auto& hit : hitEvents) {
                if (hit.casterId == sd::GetPlayer()->formID &&
                    hit.targetId == ref->formID) {
                  if (auto hash = actor.GetHash()) {
                    PHit::Value hitInfo;
                    hitInfo.target = *hash;
                    m_share2.client->Get().SendPropertyChange<PHit>(hitInfo);
                  }
                }
              }
            }
          });
      }
    } catch (std::exception& e) {
      ExitWithError(e.what());
    }
  }

  InputListener CreateInputListener() override
  {
    InputListener listener;
    listener.onMouseWheelRotate = [this](float delta) {
      m_hardcodedFeaturesData.wheelAbsolute =
        m_hardcodedFeaturesData.wheelAbsolute + delta;
    };
    return listener;
  }

  void OnRender(void* device) override
  {
    auto wnd = FindWindowA("Skyrim", "Skyrim");
    assert(wnd);

    bool allowInputs = true;

    auto menuManager = MenuManager::GetSingleton();
    static const auto fsCursorMenu = new BSFixedString("Cursor Menu");
    if (menuManager && !menuManager->IsMenuOpen(fsCursorMenu)) {
      allowInputs = false;
    }

    static MyImGuiContext ctx(device, wnd);
    auto [x, y] = GetMousePos::Run();
    auto onRender = [this, device] {
      if (m_share2.gameState == GameState::MainMenu)
        DrawChromium(device);
      DrawImGuiWidgets();
    };
    ctx.Render(device, onRender, x, y, allowInputs);
  }

  void DrawImGuiWidgets()
  {
    const bool pressed = ImGui::GetIO().KeysDown['9'] &&
      ImGui::GetIO().KeysDown['O'] && ImGui::GetIO().KeysDown['L'];
    if (m_showDebugPressedWas != pressed) {
      m_showDebugPressedWas = pressed;
      if (pressed)
        m_showDebug = !m_showDebug;
    }

    if (!m_showDebug)
      return;

    if (!m_debugView) {
      DebugPresenter p;
      p.forEachLogLine = [this](auto f) { m_logsStorage.ForEachLogLine(f); };
      p.getNotification = [this] { return ""; };
      p.startServer = [this](const std::vector<std::string>& a) {
        std::string err = StartServer(a);
        if (err.size())
          m_logsStorage.WriteLogs("Console")(Viet::Logging::Severity::Error,
                                             err.data());
      };
      p.connect = [this](const std::vector<std::string>& a) {
        std::string err = StartClient(a);
        if (err.size())
          m_logsStorage.WriteLogs("Console")(Viet::Logging::Severity::Error,
                                             err.data());
      };
      m_debugView.reset(new DebugView(p));
    }
    m_debugView->OnPresent();
  }

  std::string StartClient(const std::vector<std::string>& arguments)
  {
    const int numArgsExpected = 3;
    if (arguments.size() != numArgsExpected + 1)
      return std::to_string(numArgsExpected) +
        " arguments expected (serverIp, serverPort, nickname)";

    std::string err;

    std::string ip = arguments[1];
    int port = atoi(arguments[2].data());
    std::string nickname = arguments[3];

    if (!port)
      return "Bad port";

    if (nickname.size() < 2)
      return "Bad nickname";

    std::thread([&] {
      try {
        std::lock_guard l(m_share2.m);
        m_share2.client.reset(new VietClientWrapper(ip.data(), port));
        m_share2.nickname = nickname;
        // assert(0);
      } catch (std::exception& e) {
        err = e.what();
        // assert(0);
      }
    })
      .join();

    return err;
  }

  std::string StartServer(const std::vector<std::string>& arguments)
  {
    const int numArgsExpected = 2;
    if (arguments.size() != numArgsExpected + 1)
      return std::to_string(numArgsExpected) +
        " arguments expected (mongoUri, databaseName)";

    std::string mongoUri = arguments[1];
    std::string dbName = arguments[2];

    auto devPass = RandomString(32);

    std::string err;

    std::thread([&] {
      try {

        std::lock_guard l(m_share.m);

        {

          VietServerWrapper::Settings s;
          s.devPass = devPass;
          s.gamemodesPort = 7778;
          s.maxPlayers = 10;
          s.mongoDbName = dbName;
          s.mongoDbNameToDropPrefix = "";
          s.mongoUri = mongoUri;
          s.onLogsWrite = m_logsStorage.WriteLogs("Server");
          s.port = 7777;

          m_share.server.reset(new VietServerWrapper(s));
        }

        {
          VietApiWrapper::Settings s;
          s.devPassword = devPass;
          s.ip = "127.0.0.1";
          s.onLogsWrite = m_logsStorage.WriteLogs("Api");
          s.port = 7778;

          s.listeners.onUserEnter = [this](Viet::Instance user) {
            Movement spawnPoint;
            spawnPoint.cellOrWorld = 0x2000800;
            spawnPoint.pos = { 42487.87f, 72619.74f, 11311.24f };
            OnUserEnter(&m_share.api->Get(), user, RandomString(20),
                        spawnPoint, Catch());
          };
          m_share.api.reset(new VietApiWrapper(s));
        }
      } catch (std::exception& e) {
        err = e.what();
      }
    })
      .join();

    return err;
  }
};

SkyrimPlugin::Plugin* SkyrimPlugin::GetPlugin()
{
  static auto plugin = new SkympClient;
  return plugin;
}