#pragma once
#include <memory>

namespace Viet {
class Client;
}

class Sender
{
public:
  Sender();

  void Tick(Viet::Client& cl);

private:
  struct Impl;
  std::shared_ptr<Impl> pImpl;
};