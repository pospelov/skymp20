#include "Sender.h"
#include <chrono>
#include <foo/PropActorValues.h>
#include <foo/PropAnimEvent.h>
#include <foo/PropEquipment.h>
#include <foo/PropMovement.h>
#include <optional>
#include <skyrim_plugin_base/utils/GetEquipment.h>
#include <skyrim_plugin_base/utils/GetHealth.h>
#include <skyrim_plugin_base/utils/GetLastAnimationEvent.h>
#include <skyrim_plugin_base/utils/GetMovement.h>
#include <skyrim_plugin_base/utils/Misc.h>
#include <string>
#include <viet/Client.h>

namespace {
bool AnimationNeedsSend(const std::string& anim)
{
  return anim.size() > 0 && anim != "moveStart" && anim != "moveStop" &&
    anim != "TurnLeft" && anim != "TurnRight" && anim != "turnStop" &&
    anim != "CyclicCrossBlend" && anim != "CyclicFreeze" &&
    anim != "SneakStart" && anim != "SneakStop" && anim != "SprintStart" &&
    anim != "SprintStop" && anim != "BlockStart" && anim != "BlockStop" &&
    !Misc::IsEquip(anim) && !Misc::IsUnequip(anim) && !Misc::IsTorch(anim);
}
}

struct Sender::Impl
{
  Impl()
  {
    m_lastSendMov = std::chrono::steady_clock::now();
    m_lastSendHealth = std::chrono::steady_clock::now();
  }

  Viet::Client* m_cl = nullptr;

  std::optional<Equipment> m_lastEquipment;
  std::chrono::steady_clock::time_point m_lastSendMov, m_lastSendHealth;
  GetMovement::ActorCache m_getMovCache;

  void TickMovement();
  void TickEquipment();
  void TickAnimEvent();
  void TickHealth();
};

Sender::Sender()
{
  pImpl.reset(new Impl);
}

void Sender::Tick(Viet::Client& cl)
{
  pImpl->m_cl = &cl;

  pImpl->TickMovement();
  pImpl->TickEquipment();
  pImpl->TickAnimEvent();
  pImpl->TickHealth();

  pImpl->m_cl = nullptr;
}

void Sender::Impl::TickMovement()
{
  using namespace std::chrono_literals;

  if (std::chrono::steady_clock::now() - m_lastSendMov > 100ms) {
    m_cl->SendPropertyChange<PMovement>(
      GetMovement::Run(sd::GetPlayer(), m_getMovCache));
    m_lastSendMov = std::chrono::steady_clock::now();
  }
}

void Sender::Impl::TickEquipment()
{
  auto equipment = GetEquipment::Run(sd::GetPlayer());
  if (m_lastEquipment != equipment) {
    m_lastEquipment = equipment;
    m_cl->SendPropertyChange<PEquipment>(equipment);
  }
}

void Sender::Impl::TickAnimEvent()
{
  bool success = false;
  auto ae =
    GetLastAnimationEvent::Run(sd::GetPlayer(), "SendAnimation", &success);
  if (success && AnimationNeedsSend(ae))
    m_cl->SendPropertyChange<PAnimEvent>(PAnimEvent::Value(ae));
}

void Sender::Impl::TickHealth()
{
  using namespace std::chrono_literals;

  if (std::chrono::steady_clock::now() - m_lastSendHealth > 100ms) {
    m_cl->SendPropertyChange<PAVHealth>(GetHealth::Run(sd::GetPlayer()));
    m_lastSendHealth = std::chrono::steady_clock::now();
  }
}