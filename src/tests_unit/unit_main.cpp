﻿#include <sstream>
#include <string>
#include <viet/Client.h>

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "foo/Entities.h"
#include "foo/LogsStorage.h"
#include "foo/PropIsCached.h"
#include "foo/PropMovement.h"
#include "foo/PropName.h"
#include "foo/PropVirtualWorld.h"
#include "foo/VietTestContext.h"
#include "foo/foo.h"

TEST_CASE("FindOrCreate", "[viet]")
{
  Movement startingMovement;
  startingMovement.cellOrWorld = 0x3c;
  startingMovement.pos = { 6, 6, 6 };

  static VietTestContext ctx;

  Viet::FindRequest findReqAny({ Viet::FindCondition(), 1 });

  ctx.Cb().onConnect = [=](std::string error,
                           const Viet::ApiClient::ServerEntityList&) {
    ctx.Api()
      .Find(Entities::actor, findReqAny)
      .Then([=](std::vector<Viet::Instance> findResults) {
        REQUIRE(findResults.empty());

        FindOrCreate(&ctx.Api(), findReqAny,
                     GetActorCreateActions(startingMovement))
          .Then([=](Viet::Instance actor) {
            ctx.Api()
              .Find(Entities::actor, findReqAny)
              .Then([=](std::vector<Viet::Instance> findResults) {
                REQUIRE(findResults.size() == 1);
                REQUIRE(findResults[0].uniqueId == actor.uniqueId);
                FindOrCreate(&ctx.Api(), findReqAny,
                             GetActorCreateActions(startingMovement))
                  .Then([=](Viet::Instance actor) {
                    ctx.Api()
                      .Find(Entities::actor, findReqAny)
                      .Then([=](std::vector<Viet::Instance> findResults) {
                        REQUIRE(findResults.size() == 1);
                        REQUIRE(findResults[0].uniqueId == actor.uniqueId);
                        ctx.Pass();
                      })
                      .Catch(ctx.Catch());
                  })
                  .Catch(ctx.Catch());
              })
              .Catch(ctx.Catch());
          })
          .Catch(ctx.Catch());
      })
      .Catch(ctx.Catch());
  };

  ctx.Run();
}

TEST_CASE("See myself", "[viet]")
{
  static const std::string startingName = "Pepelov";

  static VietTestContext ctx;

  static Movement startingMovement;
  startingMovement.cellOrWorld = 0x3c;
  startingMovement.pos = { 35878.8672f, -17024.3711f, -4212.5869f };

  ctx.Cb().onUserEnter = [](Viet::Instance user) {
    OnUserEnter(&ctx.Api(), user, startingName, startingMovement, ctx.Catch());
  };

  ctx.WorldState() = [](const Viet::IWorldState* wst) {
    std::string myEntity = wst->GetMyEntity();
    if (myEntity == Entities::actor) {
      auto mov = wst->GetMyPropertyValue<PMovement>();
      REQUIRE(mov);
      REQUIRE(mov->cellOrWorld == startingMovement.cellOrWorld);
      REQUIRE(mov->pos == startingMovement.pos);

      auto name = wst->GetMyPropertyValue<PName>();
      REQUIRE(name);
      REQUIRE(name->value == startingName);

      ctx.Pass();
    }
  };

  ctx.Run();
}

TEST_CASE("logs storage", "[logs storage]")
{
  using namespace std::string_literals;

  LogsStorage logsStorage;
  REQUIRE(logsStorage.GetMessages().empty());

  logsStorage.WriteLogs("Server")(Viet::Logging::Severity::Notice,
                                  "Hello World");

  REQUIRE(logsStorage.GetMessages().size() == 1);
  REQUIRE(
    logsStorage.GetMessages().front() ==
    std::pair(Viet::Logging::Severity::Notice, "Server -> Hello World"s));
}