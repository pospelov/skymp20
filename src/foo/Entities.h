#pragma once
#include <vector>
#include <viet/Entity.h>

namespace Entities {

constexpr auto actor = "Actor";

std::vector<Viet::Entity> Get();
}