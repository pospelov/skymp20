#include "VietServerWrapper.h"
#include "Entities.h"
#include "LoggerOutput.h"
#include <mutex>
#include <viet-mongocxx/VietMongocxx.h>
#include <viet-raknet/VietRakNet.h>
#include <viet/Server.h>

struct VietServerWrapper::Impl
{
  std::unique_ptr<Viet::Server> server;
  std::mutex logsWriteMutex;
};

VietServerWrapper::VietServerWrapper(const Settings& s)
{
  pImpl.reset(new Impl);

  Viet::Server::Settings settings;
  settings.devPassword = s.devPass.data();
  settings.maxPlayers = 1000;

  settings.netPlugin.serverFactory = [=] {
    int port = 7777;
    int timeoutTimeMs = 4000;
    return new Viet::Networking::RakServer(s.maxPlayers, port, timeoutTimeMs);
  };
  settings.gamemodeNetPlugin.serverFactory = [=] {
    int maxGamemodes = 5;
    int timeoutTimeMs = 4000;
    return new Viet::Networking::RakServer(maxGamemodes, s.gamemodesPort,
                                           timeoutTimeMs);
  };
  settings.seriPlugin.serializerFactory = [] {
    return new Viet::Serialization::RakSerializer;
  };

  settings.loggingPlugin.loggerFactory = [=] {
    return new LoggerOutput(&pImpl->logsWriteMutex, s.onLogsWrite);
  };

  const std::string dbName = s.mongoDbName, uri = s.mongoUri,
                    dbToDropPrefix = s.mongoDbNameToDropPrefix;
  const auto onLogsWrite = s.onLogsWrite;
  settings.nosqlPlugin.databaseFactory = [=] {
    Viet::NoSqlDatabase::Mongo::Settings s;
    s.mongoUri = uri;
    s.dbName = dbName;
    s.startsWith = dbToDropPrefix;
    s.dbIgnore = { dbName };
    s.logger.reset(new Viet::Logging::Logger(
      new LoggerOutput(&pImpl->logsWriteMutex, onLogsWrite)));

    return new Viet::NoSqlDatabase::Mongo(s);
  };

  settings.entities = Entities::Get();

  pImpl->server.reset(new Viet::Server(settings));
}

void VietServerWrapper::Tick()
{
  pImpl->server->Tick();
}