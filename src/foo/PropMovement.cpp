#include "PropMovement.h"

Movement Movement::FromJson(const Viet::json& v)
{
  Movement res;
  if (v.is_object()) {
    if (v.count("cellOrWorld")) {
      auto& cellOrWorld = v.at("cellOrWorld");
      if (cellOrWorld.is_number_unsigned())
        res.cellOrWorld = cellOrWorld.get<uint32_t>();
    }
    if (v.count("pos")) {
      auto& pos = v.at("pos");
      if (pos.is_array() && pos.size() >= 3) {
        for (int i = 0; i < 3; i++)
          res.pos[i] = pos[i];
      }
    }
    if (v.count("angleZ")) {
      auto& angleZ = v.at("angleZ");
      if (angleZ.is_number_float())
        res.angleZ = angleZ.get<float>();
    }
    if (v.count("direction")) {
      auto& direction = v.at("direction");
      if (direction.is_number_float())
        res.direction = direction.get<float>();
    }
    if (v.count("runMode")) {
      auto& runMode = v.at("runMode");
      if (runMode.is_number_integer())
        res.runMode = static_cast<RunMode>(runMode.get<int>());
    }
    if (v.count("isWeapDrawn")) {
      auto& isWeapDrawn = v.at("isWeapDrawn");
      if (isWeapDrawn.is_number_integer())
        res.isWeapDrawn = isWeapDrawn.get<bool>();
    }
    if (v.count("isInJumpState")) {
      auto& isInJumpState = v.at("isInJumpState");
      if (isInJumpState.is_number_integer())
        res.isInJumpState = isInJumpState.get<bool>();
    }
    if (v.count("isBlocking")) {
      auto& isBlocking = v.at("isBlocking");
      if (isBlocking.is_number_integer())
        res.isBlocking = isBlocking.get<bool>();
    }
    if (v.count("isSneaking")) {
      auto& isSneaking = v.at("isSneaking");
      if (isSneaking.is_number_integer())
        res.isSneaking = isSneaking.get<bool>();
    }
  }
  return res;
}

Viet::json Movement::ToJson(const Movement& v)
{
  return Viet::json::object(
    { { "cellOrWorld", v.cellOrWorld },
      { "pos", Viet::json::array({ v.pos[0], v.pos[1], v.pos[2] }) },
      { "angleZ", v.angleZ },
      { "direction", v.direction },
      { "runMode", static_cast<int>(v.runMode) },
      { "isWeapDrawn", v.isWeapDrawn },
      { "isInJumpState", v.isInJumpState },
      { "isBlocking", v.isBlocking },
      { "isSneaking", v.isSneaking } });
}

bool Movement::operator==(const Movement& rhs) const
{
  return this->cellOrWorld == rhs.cellOrWorld && this->pos == rhs.pos &&
    this->angleZ == rhs.angleZ && this->direction == rhs.direction &&
    this->runMode == rhs.runMode && this->isWeapDrawn == rhs.isWeapDrawn &&
    this->isInJumpState == rhs.isInJumpState &&
    this->isBlocking == rhs.isBlocking && this->isSneaking == rhs.isSneaking;
}
bool Movement::operator!=(const Movement& rhs) const
{
  return !operator==(rhs);
}

Viet::Property PMovement::Build()
{
  return Viet::Property()
    .Register<PMovement>()
    .AttachToGrid([](const PMovement::Value& mov) {
      constexpr int exteriorCellSizeInUnits = 4096;
      return std::pair<int16_t, int16_t>(
        int16_t(mov.pos[0] / exteriorCellSizeInUnits),
        int16_t(mov.pos[1] / exteriorCellSizeInUnits));
    })
    .SetSaveCondition(
      [](const PMovement::Value& oldMov, const PMovement::Value& newMov) {
        constexpr float minimumDistanceToSave = 256;
        return GetDistance(oldMov.pos, newMov.pos) > minimumDistanceToSave ||
          oldMov.cellOrWorld != newMov.cellOrWorld;
      })
    .RemoveFlags(Viet::PropertyFlags::ReliableNetworking);
}
