#pragma once
#include <viet/Property.h>

class PVirtualWorld
{
public:
  static constexpr auto name = "virtualWorld";
  using Value = Viet::SimplePropertyValue<uint32_t>;

  static Viet::Property Build()
  {
    return Viet::Property()
      .Register<PVirtualWorld>()
      .AddFlags(Viet::PropertyFlags::AffectsCellProcessSelection)
      .RemoveFlags(Viet::PropertyFlags::ClientWritable);
  }
};