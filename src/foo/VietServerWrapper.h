#pragma once
#include <functional>
#include <memory>
#include <string>
#include <viet/Logging.h>

class VietServerWrapper
{
public:
  using OnLogsWrite =
    std::function<void(Viet::Logging::Severity, const char*)>;

  struct Settings
  {
    int maxPlayers = 0;
    int port = 0;
    int gamemodesPort = 0;
    std::string devPass;
    OnLogsWrite onLogsWrite;
    std::string mongoUri;
    std::string mongoDbName;
    std::string mongoDbNameToDropPrefix;
  };

  VietServerWrapper(const Settings& settings);
  void Tick();

private:
  struct Impl;
  std::shared_ptr<Impl> pImpl;
};