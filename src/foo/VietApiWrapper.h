#pragma once
#include <functional>
#include <memory>
#include <string>
#include <viet/ApiClient.h>

class VietApiWrapper
{
public:
  using OnLogsWrite =
    std::function<void(Viet::Logging::Severity, const char*)>;

  struct Settings
  {
    std::string ip;
    int port = 0;
    std::string devPassword;
    OnLogsWrite onLogsWrite;
    Viet::ApiClient::Listeners listeners;
  };

  VietApiWrapper(const Settings& settings);
  Viet::ApiClient& Get();
  void Tick();

  template <class V>
  Viet::Promise<V> SetImmediate(const V& value)
  {
    Viet::Promise<V> promise;
    this->SetImemdiate([promise, value] { promise.Resolve(value); });
    return promise;
  }

  void SetImemdiate(std::function<void()> resolve);

private:
  struct Impl;
  std::shared_ptr<Impl> pImpl;
};