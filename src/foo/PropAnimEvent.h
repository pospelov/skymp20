#pragma once
#include <viet/JsonWrapper.h>
#include <viet/Property.h>
#include <viet/PropertyValue.h>

class PAnimEvent
{
public:
  static constexpr auto name = "animEvent";

  using Value = Viet::SimplePropertyValue<std::string>;

  static Viet::Property PAnimEvent::Build()
  {
    return Viet::Property().Register<PAnimEvent>().RemoveFlags(
      Viet::PropertyFlags::SavedToDb |
      Viet::PropertyFlags::ReliableNetworking);
  }
};