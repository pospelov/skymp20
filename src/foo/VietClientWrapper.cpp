#include "VietClientWrapper.h"
#include "Entities.h"
#include <viet-mongocxx/VietMongocxx.h>
#include <viet-raknet/VietRakNet.h>
#include <viet/Client.h>

struct VietClientWrapper::Impl
{
  std::unique_ptr<Viet::Client> client;
};

VietClientWrapper::VietClientWrapper(const char* ip_, int port)
{
  pImpl.reset(new Impl);

  std::string ip = ip_;

  Viet::Networking::Plugin netPlugin;
  netPlugin.clientFactory = [=] {
    const int timeoutTimeMs = 4000;
    return new Viet::Networking::RakClient(ip.data(), port, timeoutTimeMs);
  };

  Viet::Serialization::Plugin seriPlugin;
  seriPlugin.serializerFactory = [=] {
    return new Viet::Serialization::RakSerializer;
  };

  pImpl->client.reset(
    new Viet::Client(Entities::Get(), netPlugin, seriPlugin));
}

Viet::Client& VietClientWrapper::Get()
{
  return *pImpl->client;
}