#pragma once
#include <viet/Property.h>

class PName
{
public:
  static constexpr auto name = "name";
  using Value = Viet::SimplePropertyValue<std::string>;

  static Viet::Property Build()
  {
    return Viet::Property().Register<PName>().RemoveFlags(
      Viet::PropertyFlags::ClientWritable);
  }
};