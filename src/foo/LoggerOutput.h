#pragma once
#include <functional>
#include <mutex>
#include <viet/Logging.h>

class LoggerOutput : public Viet::Logging::ILoggerOutput
{
public:
  using F = std::function<void(Viet::Logging::Severity, const char*)>;

  LoggerOutput(std::mutex* m, F f)
    : m_logsWriteMutex(m)
    , m_onLogsWrite(f)
  {
  }

  std::mutex* const m_logsWriteMutex;
  const F m_onLogsWrite;

  void Write(Viet::Logging::Severity severity,
             const char* text) noexcept override
  {
    std::lock_guard l(*m_logsWriteMutex);
    if (m_onLogsWrite)
      m_onLogsWrite(severity, text);
  }
};