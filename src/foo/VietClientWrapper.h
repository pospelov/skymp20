#pragma once
#include <memory>

namespace Viet {
class Client;
}

class VietClientWrapper
{
public:
  VietClientWrapper(const char* ip, int port);
  Viet::Client& Get();

private:
  struct Impl;
  std::shared_ptr<Impl> pImpl;
};