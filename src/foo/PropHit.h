#pragma once
#include "PropActorValues.h"
#include <viet/JsonWrapper.h>
#include <viet/Property.h>
#include <viet/PropertyEventHandler.h>
#include <viet/PropertyValue.h>
#include <viet/Server.h>

struct HitInfo : public Viet::PropertyValue<HitInfo>
{
  uint64_t target = 0;

  static HitInfo FromJson(const Viet::json& v);
  static Viet::json ToJson(const HitInfo& v);
  bool operator==(const HitInfo& rhs) const;
  bool operator!=(const HitInfo& rhs) const;

  template <class Archive>
  void serialize(Archive& ar, int)
  {
    ar& target;
  }
};

class PHit
{
public:
  class HitEventHandler : public Viet::PropertyEventHandler
  {
  public:
    void OnChange(Viet::Server& server,
                  const Viet::IPropertyValue* newValue) override;
  };

  static constexpr auto name = "hit";

  using Value = HitInfo;

  static Viet::Property PHit::Build()
  {
    return Viet::Property()
      .Register<PHit>()
      .RemoveFlags(Viet::PropertyFlags::SavedToDb |
                   Viet::PropertyFlags::VisibleToNeighbour)
      .AddEventHandler(new HitEventHandler());
  }
};