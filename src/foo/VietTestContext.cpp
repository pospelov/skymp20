#include "VietTestContext.h"
#include "RandomString.h"
#include "VietApiWrapper.h"
#include "VietClientWrapper.h"
#include "VietServerWrapper.h"
#include <catch.hpp>
#include <chrono>
#include <iostream>
#include <mutex>
#include <thread>
#include <viet/Client.h>
#include <viet/Server.h>

using namespace std::chrono_literals;

namespace {

void WriteStdout(Viet::Logging::Severity severity, const char* text)
{
  static std::mutex g_mutex;
  std::lock_guard l(g_mutex);

  std::cout << "[" << Viet::Logging::GetSeverityName(severity) << "] " << text
            << std::endl;
}

const char* GetTestUri()
{
  if (auto value = getenv("TEST_MONGO_URI"))
    return value;
  throw std::runtime_error(
    "TEST_MONGO_URI isn't a valid environment variable");
}

const char* GetTestDbName()
{
  static std::string g_dbName;
  g_dbName = "skymp20_test_";
  g_dbName += Catch::getResultCapture().getCurrentTestName();
  g_dbName += "_" + std::to_string(time(0));

  std::transform(g_dbName.begin(), g_dbName.end(), g_dbName.begin(),
                 [](char ch) {
                   if (ch == ' ')
                     return '_';
                   return ch;
                 });

  return g_dbName.data();
}

const char* GetTestDbNameToDropPrefix()
{
  return "skymp20_test_";
}
}

struct VietTestContext::Impl
{
  std::shared_ptr<VietApiWrapper> api;
  std::shared_ptr<VietServerWrapper> server;
  std::shared_ptr<VietClientWrapper> client;

  Viet::ApiClient::Listeners listeners;

  std::function<void(const Viet::IWorldState*)> worldStateCb;

  std::string error;
  bool passed = false;
};

VietTestContext::VietTestContext()
{
  pImpl.reset(new Impl);
}

void VietTestContext::Destroy()
{
  pImpl.reset();
}

void VietTestContext::Run()
{
  const std::string devPass = RandomString(20);
  const int gmPort = 7778;
  const int port = 7777;

  {
    VietApiWrapper::Settings s;
    s.devPassword = devPass;
    s.ip = "127.0.0.1";
    s.onLogsWrite = WriteStdout;
    s.port = gmPort;
    s.listeners = pImpl->listeners;
    pImpl->api.reset(new VietApiWrapper(s));
  }

  {
    VietServerWrapper::Settings s;
    s.devPass = devPass;
    s.gamemodesPort = gmPort;
    s.maxPlayers = 5;
    s.mongoDbName = GetTestDbName();
    s.mongoDbNameToDropPrefix = GetTestDbNameToDropPrefix();
    s.mongoUri = GetTestUri();
    s.onLogsWrite = WriteStdout;
    s.port = port;
    pImpl->server.reset(new VietServerWrapper(s));
  }

  pImpl->client.reset(new VietClientWrapper("127.0.0.1", port));

  try {
    while (1) {
      std::this_thread::sleep_for(1ms);

      auto& cl = pImpl->client->Get();
      cl.Tick();
      if (pImpl->worldStateCb)
        cl.ProcessWorldState(pImpl->worldStateCb);

      if (pImpl->server)
        pImpl->server->Tick();

      if (pImpl->api)
        pImpl->api->Tick();

      if (!pImpl->error.empty())
        throw std::runtime_error(pImpl->error);

      if (pImpl->passed)
        break;
    }
  } catch (...) {
    Destroy();
    throw;
  }
  Destroy();
}

void VietTestContext::Pass()
{
  pImpl->passed = true;
}

Viet::ApiClient::Listeners& VietTestContext::Cb()
{
  return pImpl->listeners;
}

Viet::ApiClient& VietTestContext::Api()
{
  return pImpl->api->Get();
}

std::function<void(const char*)> VietTestContext::Catch()
{
  return [this](const char* err) { pImpl->error = err; };
}
std::function<void(const Viet::IWorldState*)>& VietTestContext::WorldState()
{
  return pImpl->worldStateCb;
}