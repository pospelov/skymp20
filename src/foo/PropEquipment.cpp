#include "PropEquipment.h"

Viet::Property PEquipment::Build()
{
  return Viet::Property().Register<PEquipment>().RemoveFlags(
    Viet::PropertyFlags::SavedToDb);
}