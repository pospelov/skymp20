#pragma once
#include <vector>
#include <viet/JsonWrapper.h>
#include <viet/Property.h>
#include <viet/PropertyValue.h>

struct ActorValueEntry : public Viet::PropertyValue<ActorValueEntry>
{
  float base = 100.f;
  float currentPercentage = 1.f;

  static ActorValueEntry FromJson(const Viet::json& v);
  static Viet::json ToJson(const ActorValueEntry& v);
  bool operator==(const ActorValueEntry& rhs) const;
  bool operator!=(const ActorValueEntry& rhs) const;

  template <class Archive>
  void serialize(Archive& ar, int)
  {
    ar& base& currentPercentage;
  }
};

#define DECLARE_ACTOR_VALUE(className, propName)                              \
  class className                                                             \
  {                                                                           \
  public:                                                                     \
    using Value = ActorValueEntry;                                            \
                                                                              \
    static Viet::Property Build()                                             \
    {                                                                         \
      return Viet::Property().Register<className>();                          \
    }                                                                         \
    static constexpr const char* name = propName;                             \
  };

DECLARE_ACTOR_VALUE(PAVHealth, "health");

class PropActorValues
{
public:
  static std::vector<Viet::Property> Build() { return { PAVHealth::Build() }; }
};