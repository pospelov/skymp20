#pragma once
#include <viet/Property.h>

class PIsCached
{
public:
  static constexpr auto name = "isCached";
  using Value = Viet::SimplePropertyValue<bool>;

  static Viet::Property Build()
  {
    return Viet::Property()
      .Register<PIsCached>()
      .RemoveFlags(Viet::PropertyFlags::ClientWritable)
      .SetValueForcesCaching(PIsCached::Value(true));
  }
};