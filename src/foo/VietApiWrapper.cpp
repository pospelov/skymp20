#include "VietApiWrapper.h"
#include "LoggerOutput.h"
#include <mutex>
#include <viet-raknet/VietRakNet.h>
#include <viet/ApiClient.h>

namespace {
auto FillNullListeners(Viet::ApiClient::Listeners listeners)
{
  if (!listeners.onConnect)
    listeners.onConnect = [](auto, auto) {};
  if (!listeners.onUserCustomPacket)
    listeners.onUserCustomPacket = [](auto, auto) {};
  if (!listeners.onUserEnter)
    listeners.onUserEnter = [](auto) {};
  if (!listeners.onUserExit)
    listeners.onUserExit = [](auto) {};
  return listeners;
}
}

struct VietApiWrapper::Impl
{
  std::mutex m;
  OnLogsWrite onLogsWrite;
  std::unique_ptr<Viet::ApiClient> api;
  std::vector<std::function<void()>> immediatePromises;
};

VietApiWrapper::VietApiWrapper(const Settings& s)
{
  pImpl.reset(new Impl);

  Viet::ApiClient::Settings apicSettings;

  apicSettings.devPassword = s.devPassword;
  apicSettings.serverId = "is it deprecated?";
  apicSettings.loggingPlugin.loggerFactory = [this] {
    return new LoggerOutput(&pImpl->m, pImpl->onLogsWrite);
  };
  apicSettings.seriPlugin.serializerFactory = [] {
    return new Viet::Serialization::RakSerializer;
  };
  apicSettings.gamemodeNetPlugin.clientFactory = [=] {
    int timeoutTimeMs = 4000;
    return new Viet::Networking::RakClient(s.ip.data(), s.port, timeoutTimeMs);
  };

  pImpl->api.reset(
    new Viet::ApiClient(apicSettings, FillNullListeners(s.listeners)));
}

Viet::ApiClient& VietApiWrapper::Get()
{
  return *pImpl->api;
}

void VietApiWrapper::Tick()
{
  pImpl->api->Tick();
  for (auto& p : pImpl->immediatePromises)
    p();
  pImpl->immediatePromises.clear();
}

void VietApiWrapper::SetImemdiate(std::function<void()> resolve)
{
  pImpl->immediatePromises.push_back(resolve);
}