#include "Entities.h"

#include "PropActorValues.h"
#include "PropAnimEvent.h"
#include "PropEquipment.h"
#include "PropHit.h"
#include "PropIsCached.h"
#include "PropMovement.h"
#include "PropName.h"
#include "PropVirtualWorld.h"

std::vector<Viet::Entity> Entities::Get()
{
  std::vector<Viet::Entity> res;

  auto actorProperties = PropActorValues::Build();
  for (auto p : { PMovement::Build(), PVirtualWorld::Build(),
                  PIsCached::Build(), Viet::Property::User(), PName::Build(),
                  Viet::Property::CustomPacket(), PAnimEvent::Build(),
                  PEquipment::Build(), PHit::Build() }) {
    actorProperties.push_back(p);
  }
  res.push_back(Viet::Entity(actor, actorProperties));

  return res;
}