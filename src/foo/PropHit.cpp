#include "PropHit.h"
#include "Entities.h"

HitInfo HitInfo::FromJson(const Viet::json& v)
{
  HitInfo r;
  r.target = v.at("target").get<uint64_t>();
  return r;
}

Viet::json HitInfo::ToJson(const HitInfo& v)
{
  return Viet::json{ { "target", v.target } };
}

bool HitInfo::operator==(const HitInfo& rhs) const
{
  return this->target == rhs.target;
}

bool HitInfo::operator!=(const HitInfo& rhs) const
{
  return !(*this == rhs);
}

void PHit::HitEventHandler::OnChange(Viet::Server& server,
                                     const Viet::IPropertyValue* newValue)
{
  auto hitInfo = dynamic_cast<const HitInfo*>(newValue);
  assert(hitInfo);
  if (!hitInfo)
    return;

  auto v = server.GetPropertyValueLocally(Entities::actor, hitInfo->target,
                                          PAVHealth::name);
  const PAVHealth::Value* hp =
    dynamic_cast<const PAVHealth::Value*>(v ? v.get() : nullptr);
  assert(hp);
  if (!hp)
    return;

  const float damage = 10.f;

  float currentHealth = hp->base * hp->currentPercentage;
  currentHealth -= damage;
  if (currentHealth < 0)
    currentHealth = 0.f;

  PAVHealth::Value newHealth;
  newHealth.base = hp->base;
  newHealth.currentPercentage = currentHealth / hp->base;
  server.SetPropertyValueLocally(Entities::actor, hitInfo->target,
                                 PAVHealth::name, &newHealth);
}