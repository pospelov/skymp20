#include "PropActorValues.h"

ActorValueEntry ActorValueEntry::FromJson(const Viet::json& v)
{
  ActorValueEntry r;
  r.base = v.at("base").get<float>();
  r.currentPercentage = v.at("currentPercentage").get<float>();
  return r;
}

Viet::json ActorValueEntry::ToJson(const ActorValueEntry& v)
{
  return Viet::json{ { "base", v.base },
                     { "currentPercentage", v.currentPercentage } };
}

bool ActorValueEntry::operator==(const ActorValueEntry& rhs) const
{
  return this->base == rhs.base &&
    this->currentPercentage == rhs.currentPercentage;
}

bool ActorValueEntry::operator!=(const ActorValueEntry& rhs) const
{
  return !(*this == rhs);
}