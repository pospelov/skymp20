#pragma once
#include <array>
#include <chrono>
#include <set>
#include <viet/JsonWrapper.h>
#include <viet/Property.h>
#include <viet/PropertyValue.h>

enum class RunMode
{
  Standing = 0,
  Walking = 1,
  Running = 2,
  Sprinting = 3
};

enum class MovementComponent
{
  CellOrWorld,
  Pos,
  AngleZ,
  RunMode,
  Direction,
  IsWeapDrawn,
  IsInJumpState,
  IsBlocking,
  IsSneaking
};

struct Movement : public Viet::PropertyValue<Movement>
{
  uint32_t cellOrWorld = 0;
  std::array<float, 3> pos = { 0.f, 0.f, 0.f };
  float angleZ = 0.f;
  RunMode runMode = RunMode::Standing;
  float direction = 0.f;
  bool isWeapDrawn = false, isInJumpState = false, isBlocking = false,
       isSneaking = false;

  static Movement FromJson(const Viet::json& v);
  static Viet::json ToJson(const Movement& v);
  bool operator==(const Movement& rhs) const;
  bool operator!=(const Movement& rhs) const;

  template <class Archive>
  void serialize(Archive& ar, int)
  {
    ar& cellOrWorld& pos& angleZ& runMode& direction& isWeapDrawn&
      isInJumpState& isBlocking& isSneaking;
  }

  static bool IsDifferent(const std::array<float, 3>& pos1,
                          const std::array<float, 3>& pos2)
  {
    auto sqr = [](auto x) { return x * x; };
    return sqrt(sqr(pos1[0] - pos2[0]) + sqr(pos1[1] - pos2[1]) +
                sqr(pos1[2] - pos2[2])) > 30;
  }

  static bool IsDifferentAngle(float x, float y)
  {
    auto diff = abs(x - y);
    while (diff >= 360)
      diff -= 360;
    if (diff >= 180) {
      diff = abs(diff - 360);
    }
    return diff > 2.5f;
  }

  std::set<MovementComponent> GetDifference(const Movement& rhs) const
  {
    std::set<MovementComponent> res;
    if (this->cellOrWorld != rhs.cellOrWorld)
      res.insert(MovementComponent::CellOrWorld);
    if (IsDifferent(this->pos, rhs.pos))
      res.insert(MovementComponent::Pos);
    if (IsDifferentAngle(this->angleZ, rhs.angleZ))
      res.insert(MovementComponent::AngleZ);
    if (this->runMode != rhs.runMode)
      res.insert(MovementComponent::RunMode);
    if (IsDifferentAngle(this->direction, rhs.direction))
      res.insert(MovementComponent::Direction);
    if (this->isWeapDrawn != rhs.isWeapDrawn)
      res.insert(MovementComponent::IsWeapDrawn);
    if (this->isInJumpState != rhs.isInJumpState)
      res.insert(MovementComponent::IsInJumpState);
    if (this->isBlocking != rhs.isBlocking)
      res.insert(MovementComponent::IsBlocking);
    if (this->isSneaking != rhs.isSneaking)
      res.insert(MovementComponent::IsSneaking);
    return res;
  }
};

class PMovement
{
public:
  static constexpr auto name = "movement";
  using Value = Movement;

  static float GetDistance(const std::array<float, 3>& a,
                           const std::array<float, 3>& b)
  {
    auto sqr = [](auto x) { return x * x; };
    return sqrt(sqr(a[0] - b[0]) + sqr(a[1] - b[1]) + sqr(a[2] - b[2]));
  }

  static Viet::Property Build();
};