#pragma once
#include <viet/ApiClient.h>
#include <viet/Client.h>
#include <viet/Server.h>

namespace Networking {
class SkympServer
{
public:
  SkympServer(int maxPlayers, const char* mongoUri, const char* mongoDbName,
              const char* mongoDbNameToDropPrefix,
              Viet::ApiClient::Listeners listeners);
  ~SkympServer();
  Viet::Server& Viet();
  Viet::ApiClient& ApiClient();

private:
  struct Impl;
  Impl* const pImpl;
};

class SkympClient
{
public:
  SkympClient(const char* ip, int port);
  ~SkympClient();
  Viet::Client& Viet();

private:
  struct Impl;
  Impl* const pImpl;
};
}