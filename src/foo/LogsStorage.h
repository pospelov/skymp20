#pragma once
#include <list>
#include <mutex>
#include <string>
#include <viet/Logging.h>

class LogsStorage
{
public:
  auto WriteLogs(const std::string& componentName)
  {
    return [=](Viet::Logging::Severity severity, const char* text) {
      if (severity == Viet::Logging::Severity::Debug)
        return;

      char buf[2048] = { 0 };
      if (componentName.size() + strlen(text) + 16 > std::size(buf))
        return;
      sprintf(buf, "%s -> %s", componentName.data(), text);

      std::lock_guard l(m_share2.m);
      m_share2.newLogs.push_back({ severity, buf });
      if (m_share2.newLogs.size() > 128)
        m_share2.newLogs.pop_front();
    };
  }

  void ForEachLogLine(
    std::function<void(Viet::Logging::Severity, const char*)> f)
  {
    {
      std::lock_guard l(m_share2.m);
      for (auto& p : m_share2.newLogs)
        m_logs.push_back(p);
      m_share2.newLogs.clear();
    }

    for (auto& [severity, str] : m_logs)
      f(severity, str.data());
  }

  auto GetMessages()
  {
    List messages;
    ForEachLogLine([&](Viet::Logging::Severity sev, const char* text) {
      messages.push_back({ sev, text });
    });
    return messages;
  }

private:
  using List = std::list<std::pair<Viet::Logging::Severity, std::string>>;

  List m_logs;

  struct
  {
    std::mutex m;
    List newLogs;
  } m_share2;
};