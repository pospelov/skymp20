#pragma once
#include <cassert>
#include <cstdint>
#include <vector>
#include <viet/JsonWrapper.h>
#include <viet/Property.h>
#include <viet/PropertyValue.h>

struct Equipment : public Viet::PropertyValue<Equipment>
{
  struct EquippedWeapon
  {
    uint32_t rightHand = 0;
    uint32_t leftHand = 0;
  };

  std::vector<uint32_t> equippedArmor;
  EquippedWeapon equippedWeapon;

  static Equipment FromJson(const Viet::json& v)
  {
    assert(0 && "Not implemented");
    return {};
  }

  static Viet::json ToJson(const Equipment& v)
  {
    assert(0 && "Not implemented");
    return {};
  }

  bool operator==(const Equipment& rhs) const
  {
    return this->equippedWeapon.rightHand == rhs.equippedWeapon.rightHand &&
      this->equippedWeapon.leftHand == rhs.equippedWeapon.leftHand &&
      this->equippedArmor == rhs.equippedArmor;
  }

  bool operator!=(const Equipment& rhs) const { return !(*this == rhs); }

  template <class Archive>
  void serialize(Archive& ar, int)
  {
    ar& equippedWeapon.rightHand& equippedWeapon.leftHand& equippedArmor;
  }
};

class PEquipment
{
public:
  static constexpr auto name = "equipment";
  using Value = Equipment;

  static Viet::Property Build();
};
