#pragma once
#include <string>

#include "Entities.h"
#include "PropIsCached.h"
#include "PropMovement.h"
#include "PropName.h"
#include "PropVirtualWorld.h"

inline Viet::Actions GetActorCreateActions(Movement startingMovement)
{
  return Viet::Actions()
    .Set(PMovement::name, Movement::ToJson(startingMovement))
    .Set(PIsCached::name, true)
    .Set(PVirtualWorld::name, 0);
}

inline Viet::Promise<Viet::Instance> FindOrCreate(Viet::ApiClient* api,
                                                  Viet::FindRequest findReq,
                                                  Viet::Actions createActions)
{
  Viet::Promise<Viet::Instance> promise;

  api->Find(Entities::actor, findReq)
    .Then([=](std::vector<Viet::Instance> v) {
      if (v.size()) {
        promise.Resolve(v[0]);
      } else {
        api->Create(Entities::actor, createActions)
          .Then([=](Viet::ActionsResult res) {
            res.instance ? promise.Resolve(*res.instance)
                         : promise.Reject("nullopt instance");
          })
          .Catch([=](const char* err) { promise.Reject(err); });
      }
    })
    .Catch([=](const char* err) { promise.Reject(err); });

  return promise;
}

using CatchFn = std::function<void(const char*)>;

inline Viet::Promise<Viet::Void> OnUserEnter(Viet::ApiClient* api,
                                             Viet::Instance user,
                                             std::string name,
                                             Movement spawnPoint, CatchFn c)
{
  Viet::Promise<Viet::Void> p;

  FindOrCreate(
    api,
    Viet::FindRequest{
      Viet::FindCondition().Compare(
        PName::name, Viet::FindCondition::ComparisonOperator::Equal, name),
      1 },
    GetActorCreateActions(spawnPoint).Set(PName::name, name))
    .Then([=](Viet::Instance actor) {
      api->SetAttachedInstance(user, Entities::actor, actor).Then(p);
    })
    .Catch(c);

  return p;
}