#pragma once
#include <memory>
#include <viet/ApiClient.h>

namespace Viet {
class IWorldState;
}

class VietTestContext
{
public:
  VietTestContext();

  void Destroy();
  void Run();
  void Pass();
  Viet::ApiClient::Listeners& Cb();
  Viet::ApiClient& Api();
  std::function<void(const char*)> Catch();
  std::function<void(const Viet::IWorldState*)>& WorldState();

private:
  struct Impl;
  std::shared_ptr<Impl> pImpl;
};