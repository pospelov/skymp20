#include "SkyrimTestContext.h"
#include <atomic>
#include <mutex>

struct SkyrimTestContext::Impl
{
  std::atomic<bool> passed = false;

  struct
  {
    std::mutex m;
    std::vector<std::function<void()>> gameThreadTasks;
  } share;

  struct
  {
    std::mutex m;
    std::function<void()> onRender;
  } share2;
};

SkyrimTestContext::SkyrimTestContext()
{
  pImpl.reset(new Impl);
}

SkyrimTestContext::~SkyrimTestContext()
{
}

void SkyrimTestContext::TickByGameThread()
{
  decltype(pImpl->share.gameThreadTasks) tasksCopy;

  {
    std::lock_guard l(pImpl->share.m);
    tasksCopy = std::move(pImpl->share.gameThreadTasks);
    pImpl->share.gameThreadTasks.clear();
  }

  for (auto& t : tasksCopy)
    t();
}

void SkyrimTestContext::TickByHelperThread()
{
}

void SkyrimTestContext::TickByRenderThread()
{
  std::function<void()> f;
  {
    std::lock_guard l(pImpl->share2.m);
    f = pImpl->share2.onRender;
  }
  if (f)
    f();
}

void SkyrimTestContext::TaskGameThread(std::function<void()> f)
{
  std::lock_guard l(pImpl->share.m);
  pImpl->share.gameThreadTasks.push_back(f);
}

void SkyrimTestContext::SetOnRender(std::function<void()> f)
{
  std::lock_guard l(pImpl->share2.m);
  pImpl->share2.onRender = f;
}

void SkyrimTestContext::Pass()
{
  pImpl->passed = true;
}

bool SkyrimTestContext::IsPassed() const noexcept
{
  return pImpl->passed;
}