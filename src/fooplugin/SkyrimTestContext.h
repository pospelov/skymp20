#pragma once
#include "SkyrimTestFailure.h"
#include <functional>
#include <map>
#include <memory>
#include <string>

class SkyrimTestContext
{
public:
  SkyrimTestContext();
  ~SkyrimTestContext();

  void TickByGameThread();
  void TickByHelperThread();
  void TickByRenderThread();

  void TaskGameThread(std::function<void()> f);

  void SetOnRender(std::function<void()> f);

  void Pass();

  bool IsPassed() const noexcept;

private:
  struct Impl;
  std::shared_ptr<Impl> pImpl;
};

using TestFunction =
  std::function<void(std::shared_ptr<SkyrimTestContext> ctx)>;

#define SKYRIM_ASSERT(x)                                                      \
  if (!(x))                                                                   \
    throw SkyrimTestFailure("Assertion failed: " + (std::string) #x,          \
                            __FILE__, __func__, __LINE__);

class TestFunctions
{
public:
  static std::map<std::string, TestFunction>& Get()
  {
    static std::map<std::string, TestFunction> data;
    return data;
  }
};

class TestAdder
{
public:
  TestAdder(const char* name, TestFunction f)
  {
    TestFunctions::Get()[name] = f;
  }
};

#define SKYRIM_TEST_CASE(name)                                                \
  void name(std::shared_ptr<SkyrimTestContext> ctx);                          \
  namespace TestAdders {                                                      \
  static TestAdder name(#name, ::name);                                       \
  }                                                                           \
  void name(std::shared_ptr<SkyrimTestContext> ctx)