#pragma once
#include <stdexcept>
#include <string>

class SkyrimTestFailure : public std::runtime_error
{
public:
  SkyrimTestFailure(std::string msg, const char* fileName_,
                    const char* funcName_, int line_)
    : runtime_error(msg)
    , fileName(fileName_)
    , funcName(funcName_)
    , line(line_)
  {
  }

  const char* const fileName;
  const char* const funcName;
  const int line;
};