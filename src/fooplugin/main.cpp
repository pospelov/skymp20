#define SKYRIM_PLUGIN_NAME "fooplugin"
#include <skyrim_plugin_base/SkyrimPluginMain.h>

#include "SkyrimTestContext.h"
#include <common/IPrefix.h>
#include <foo/PropMovement.h>
#include <fstream>
#include <imgui.h>
#include <imgui_dx9_app_base/ImGuiContext.h>
#include <skse/GameAPI.h>
#include <sstream>

class FooPlugin final : public SkyrimPlugin::Plugin
{
public:
  FooPlugin() { m_testContext.reset(new SkyrimTestContext); }

  void HelperThreadMain() override
  {
    Console_Print("Hello Fooplugin");

    while (!IsDebuggerPresent())
      Sleep(10);

    std::string testName;
    std::ifstream("_ctest_temp/test_name.txt") >> testName;
    TestFunction f = TestFunctions::Get()[testName];
    m_testName = testName;
    try {
      if (!f) {
        throw std::logic_error("Test with name '" + testName + "' not found");
      }
      f(m_testContext);
      while (!m_testContext->IsPassed()) {
        m_testContext->TickByHelperThread();
        Sleep(1);
      }
    } catch (std ::exception& e) {
      Fail(e);
    }

    Console_Print("Test finished");

    std::exit(0);
  }

  void GameThreadMain() override
  {
    try {
      while (1) {
        if (m_testContext)
          if (!m_testContext->IsPassed()) {
            m_testContext->TickByGameThread();
          }
        sd::Wait(0);
      }
    } catch (std::exception& e) {
      Fail(e);
    }
  }

  void Fail(std::exception& e)
  {
    std::stringstream ss;
    ss << e.what();
    if (auto assertError = dynamic_cast<SkyrimTestFailure*>(&e)) {
      ss << std::endl
         << "File name: " << assertError->fileName << std::endl
         << "Func name: " << assertError->funcName << std::endl
         << "Line: " << assertError->line;
    }

    std::ofstream("_ctest_temp/" + m_testName + ".txt") << ss.str();

    std::exit(-1);
  }

  InputListener CreateInputListener() override
  {
    InputListener listener;
    return listener;
  }

  void OnRender(void* device) override
  {
    static MyImGuiContext ctx(device, FindWindowA("Skyrim", "Skyrim"));
    ctx.Render(device, [this] { m_testContext->TickByRenderThread(); }, 0, 0,
               true);
  }

private:
  std::shared_ptr<SkyrimTestContext> m_testContext;
  std::string m_testName;
};

SkyrimPlugin::Plugin* SkyrimPlugin::GetPlugin()
{
  static auto plugin = new FooPlugin;
  return plugin;
}