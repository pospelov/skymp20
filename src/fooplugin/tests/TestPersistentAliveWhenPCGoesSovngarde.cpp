#include "Tests.h"
#include <common/IPrefix.h>
#include <skse/GameReferences.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/LoadGame.h>
#include <skyrim_plugin_base/utils/PlaceAtMePersistent.h>
#include <skyrim_plugin_base/utils/Teleport.h>

SKYRIM_TEST_CASE(TestPersistentAliveWhenPCGoesSovngarde)
{
  LoadGame::Run({ 165643.8281f, -69763.3125f, 8209.8496f }, { 0, 0, 0 }, 0x3c);

  ctx->TaskGameThread([=] {
    auto baseForm = (TESNPC*)sd::GetBaseObject(sd::GetPlayer());
    uint32_t id = PlaceAtMePersistent::Run(sd::GetPlayer(),
                                           sd::GetBaseObject(sd::GetPlayer()));

    auto refrWas = Cast::Run<TESObjectREFR>(id);

    SKYRIM_ASSERT(refrWas);
    SKYRIM_ASSERT(sd::GetParentCell(refrWas) ==
                  sd::GetParentCell(sd::GetPlayer()));
    SKYRIM_ASSERT(sd::GetWorldSpace(refrWas) ==
                  sd::GetWorldSpace(sd::GetPlayer()));

    Teleport::Run(sd::GetPlayer(), 0x2ee41,
                  { 58943.4922f, 56734.8516f, 11677.5156f }, { 0, 0, 0 },
                  Teleport::Wait::True);

    SKYRIM_ASSERT(sd::GetWorldSpace(sd::GetPlayer()));
    SKYRIM_ASSERT(sd::GetWorldSpace(sd::GetPlayer())->formID == 0x2ee41);

    SKYRIM_ASSERT(Cast::Run<TESObjectREFR>(id));

    ctx->Pass();
  });
}