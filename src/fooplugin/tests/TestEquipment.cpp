#include "Tests.h"
#include <PapyrusWeapon.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/LoadGame.h>
#include <skyrim_plugin_base/utils/PlaceAtMePersistent.h>
#include <skyrim_plugin_base\utils\WeaponCreator.h>

SKYRIM_TEST_CASE(TestEquipmentArmor)
{
  LoadGame::Run({ -86642.1875f, 8655.1689f, -3890.1050f },
                { 0.0000f, -0.0000f, 317.9684f }, Tamriel);

  ctx->TaskGameThread([=] {
    auto base = Cast::Run<TESNPC>(ID_TESNPC::Astrid);

    Equipment equip;
    equip.equippedArmor = { ID_TESObjectARMO::ArmorSteelPlateCuirass,
                           ID_TESObjectARMO::ArmorSteelPlateBoots,
                           ID_TESObjectARMO::ArmorSteelPlateGauntlets,
                           ID_TESObjectARMO::ArmorSteelPlateHelmet };

    auto refr =
      Cast::Run<Actor>(PlaceAtMePersistent::Run(sd::GetPlayer(), base, equip));

    SKYRIM_ASSERT(refr);

    SKYRIM_ASSERT(sd::IsEquipped(
      refr, sd::GetFormById(ID_TESObjectARMO::ArmorSteelPlateCuirass)));

    SKYRIM_ASSERT(sd::IsEquipped(
      refr, sd::GetFormById(ID_TESObjectARMO::ArmorSteelPlateBoots)));

    SKYRIM_ASSERT(sd::IsEquipped(
      refr, sd::GetFormById(ID_TESObjectARMO::ArmorSteelPlateGauntlets)));

    SKYRIM_ASSERT(sd::IsEquipped(
      refr, sd::GetFormById(ID_TESObjectARMO::ArmorSteelPlateHelmet)));

    SKYRIM_ASSERT(
      !sd::IsEquipped(refr, sd::GetFormById(ID_TESObjectARMO::DBArmorBoots)));

    SKYRIM_ASSERT(
      !sd::IsEquipped(refr, sd::GetFormById(ID_TESObjectARMO::DBArmor)));

    SKYRIM_ASSERT(
      !sd::IsEquipped(refr, sd::GetFormById(ID_TESObjectARMO::DBArmorGloves)));

    SKYRIM_ASSERT(
      !sd::IsEquipped(refr, sd::GetFormById(ID_TESObjectARMO::DBArmorHelmet)));

    ctx->Pass();
  });
}

SKYRIM_TEST_CASE(TestEquipmentDeletedArmor)
{
  LoadGame::Run({ -86642.1875f, 8655.1689f, -3890.1050f },
                { 0.0000f, -0.0000f, 317.9684f }, Tamriel);

  ctx->TaskGameThread([=] {
    auto base = Cast::Run<TESNPC>(ID_TESNPC::Astrid);

    Equipment equip;

    auto refr =
      Cast::Run<Actor>(PlaceAtMePersistent::Run(sd::GetPlayer(), base, equip));

    SKYRIM_ASSERT(
      !sd::IsEquipped(refr, sd::GetFormById(ID_TESObjectARMO::DBArmorBoots)));

    SKYRIM_ASSERT(
      !sd::IsEquipped(refr, sd::GetFormById(ID_TESObjectARMO::DBArmor)));

    SKYRIM_ASSERT(
      !sd::IsEquipped(refr, sd::GetFormById(ID_TESObjectARMO::DBArmorGloves)));

    SKYRIM_ASSERT(
      !sd::IsEquipped(refr, sd::GetFormById(ID_TESObjectARMO::DBArmorHelmet)));

    ctx->Pass();
  });
}

SKYRIM_TEST_CASE(TestEquipmentDontChangeEquipment)
{
  LoadGame::Run({ -86642.1875f, 8655.1689f, -3890.1050f },
                { 0.0000f, -0.0000f, 317.9684f }, Tamriel);

  ctx->TaskGameThread([=] {
    auto base = Cast::Run<TESNPC>(ID_TESNPC::Astrid);

    auto refr =
      Cast::Run<Actor>(PlaceAtMePersistent::Run(sd::GetPlayer(), base));

    SKYRIM_ASSERT(
      sd::IsEquipped(refr, sd::GetFormById(ID_TESObjectARMO::DBArmorBoots)));

    SKYRIM_ASSERT(
      sd::IsEquipped(refr, sd::GetFormById(ID_TESObjectARMO::DBArmor)));

    SKYRIM_ASSERT(
      sd::IsEquipped(refr, sd::GetFormById(ID_TESObjectARMO::DBArmorGloves)));

    SKYRIM_ASSERT(
      sd::IsEquipped(refr, sd::GetFormById(ID_TESObjectARMO::DBArmorHelmet)));

    ctx->Pass();
  });
}