#include "Tests.h"
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/GetFreeFormId.h>
#include <skyrim_plugin_base/utils/LoadGame.h>
#include <skyrim_plugin_base\utils\SetFormId.h>

SKYRIM_TEST_CASE(TestCreateFreeFormId)
{
  SKYRIM_ASSERT(0xffffffff == GetFreeFormId::Run());
  SKYRIM_ASSERT(0xffffffff == GetFreeFormId::Run());
  SKYRIM_ASSERT(0xffffffff == GetFreeFormId::Run());

  auto x = Cast::Run<TESNPC>(ID_TESNPC::Alva);
  SetFormId::Run(x, 0xffffffff);

  SKYRIM_ASSERT(0xfffffffe == GetFreeFormId::Run());

  SetFormId::Run(x, ID_TESNPC::Alva);

  // SetFormId implementation keeps 0xffffffff used
  SKYRIM_ASSERT(0xfffffffe == GetFreeFormId::Run());

  ctx->Pass();
}