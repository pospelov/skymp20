#pragma once
#include <fooplugin/SkyrimTestContext.h>

#include <array>
#include <atomic>
#include <chrono>
#include <common/IPrefix.h>
#include <ctime>
#include <mutex>
#include <savefile\SaveFileStructure.h>
#include <scriptdragon/enums.h>
#include <scriptdragon/skyscript.h>
#include <skse/GameData.h>
#include <skse/GameMenus.h>
#include <sstream>

enum
{
  Tamriel = 0x3c,
  Sovngarde_HallOfValor = 0x95C44,
  Sovngarde = 0x2EE41,
  Solstheim = 0x2000800,
  SolstheimInterior = 0x2017EBE,
  SovngardeClimate = 0x90A91,
  SovngardeClearWeather = 0x10D9EC,
  SovngardeDark = 0x10FEF8,
  HelgenAttackWeather = 0xD9329,
  HairColor09DarkBrown = 0xA0433,
  FaceMaleHeadBreton = 0x51633,
  MaleEyesHumanBrown = 0x51632,
  HairMaleImperial1 = 0x51405,
  HumanBeard11 = 0xC3CDC,
  BrowsMaleHumanoid06 = 0xC7123,
  MarksMaleHumanoid00NoScar = 0x8555F,
  SkinHeadMaleBretonComplexion_Rough01 = 0xD071F,
  HumanSkinBaseWhite03 = 0xE6E08,
  BretonRace = 0x13741,
  Male = 0,
  Female = 1
};

inline int16_t GetLoadedModIndex(const std::string& name)
{
  std::vector<std::string> myModsOrder;
  auto dataHandler = DataHandler::GetSingleton();

  if (!dataHandler)
    throw std::runtime_error("DataHandler::GetSingleton = nullptr");

  uint32_t numMods = dataHandler->modList.loadedModCount;

  for (size_t i = 0; i < numMods; ++i)
    myModsOrder.push_back(
      std::string(dataHandler->modList.loadedMods[i]->name));

  for (size_t i = 0; i < numMods; ++i) {
    if (myModsOrder[i] == name)
      return i;
  }
  return -1;
}

template <class T = std::array<float, 3>>
inline T GetPlayerPos(TESObjectREFR* ref = nullptr)
{
  if (!ref)
    ref = sd::GetPlayer();

  return { sd::GetPositionX(ref), sd::GetPositionY(ref),
           sd::GetPositionZ(ref) };
}

inline std::array<float, 3> GetPlayerRot()
{
  auto player = sd::GetPlayer();

  return { sd::GetAngleX(player), sd::GetAngleY(player),
           sd::GetAngleZ(player) };
}

inline void StartCombatWithPlayer(Actor* actor)
{
  sd::SetGodMode(true);
  sd::StartCombat(actor, sd::GetPlayer());
  while (!sd::IsWeaponDrawn(actor))
    sd::Wait(0);
  sd::Wait(2000);
}