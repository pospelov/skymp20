#include "Tests.h"
#include <skyrim_plugin_base/utils/Teleport.h>

SKYRIM_TEST_CASE(TestTeleportToInvalidWorld)
{
  std::string err;
  try {
    Teleport::Run(sd::GetPlayer(), 0xDEADBEEF, { 0, 0, 0 }, { 0, 0, 0 },
                  Teleport::Wait::True);
  } catch (std::exception& e) {
    err = e.what();
  }
  SKYRIM_ASSERT(err == "0xdeadbeef is not a valid Cell/WorldSpace");

  ctx->Pass();
}