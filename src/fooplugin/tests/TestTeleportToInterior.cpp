#include "Tests.h"
#include <skyrim_plugin_base/utils/Teleport.h>

SKYRIM_TEST_CASE(TestTeleportToInterior)
{
  auto player = sd::GetPlayer();

  NiPoint3 pos = { 2496.0000f, 2304.0000f, 6976.0000f };
  uint32_t cellId = 0x32ae7;

  Teleport::Run(player, cellId, pos, { 0, 0, 0 }, Teleport::Wait::True);

  ctx->TaskGameThread([=] {
    auto cell = sd::GetParentCell(sd::GetPlayer());

    std::array<float, 3> realPos = { sd::GetPositionX(player),
                                     sd::GetPositionY(player),
                                     sd::GetPositionZ(player) };

    SKYRIM_ASSERT(abs(pos.x - realPos[0]) < 50.f);
    SKYRIM_ASSERT(abs(pos.y - realPos[1]) < 50.f);
    SKYRIM_ASSERT(abs(pos.z - realPos[2]) < 50.f);
    SKYRIM_ASSERT(cell);
    SKYRIM_ASSERT(cell->formID == cellId);

    ctx->Pass();
  });
}