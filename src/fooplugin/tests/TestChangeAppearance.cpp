#include "Tests.h"
#include <common/IPrefix.h>
#include <savefile\ChangeFormNPC.h>
#include <skse/GameReferences.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/LoadGame.h>

namespace {

SaveFile_::ChangeFormNPC_* CreateChangeFormNPC()
{
  static SaveFile_::ChangeFormNPC_ npcForm;

  std::vector<SaveFile_::RefID> headParts = {
    SaveFile_::RefID(FaceMaleHeadBreton),
    SaveFile_::RefID(MaleEyesHumanBrown),
    SaveFile_::RefID(HairMaleImperial1),
    SaveFile_::RefID(HumanBeard11),
    SaveFile_::RefID(BrowsMaleHumanoid06),
    SaveFile_::RefID(MarksMaleHumanoid00NoScar)
  };

  std::vector<float> options = { -1.f, -2.f, -3.f, -4.f, -5.f, -6.f, -7.f,
                                 8.f,  9.f,  10.f, 11.f, 12.f, 13.f, 14.f,
                                 15.f, 16.f, 17.f, 18.f, 19.f };
  std::vector<uint32_t> presets = { 132, 444, 523, 900 };

  npcForm.playerName = "Leonid";
  npcForm.gender = Male;
  npcForm.race = { SaveFile_::RefID(BretonRace),
                   SaveFile_::RefID(BretonRace) };
  npcForm.face = { SaveFile_::RefID(HairColor09DarkBrown),
                   { 0xf09c00 },
                   SaveFile_::RefID(SkinHeadMaleBretonComplexion_Rough01),
                   headParts,
                   options,
                   presets };
  return &npcForm;
}
}

SKYRIM_TEST_CASE(TestChangeAppearance)
{

  std::array<float, 3> pos = { -86642.1875f, 8655.1689f, -3890.1050f },
                       rot = { 0.0000f, -0.0000f, 317.9684f };
  uint32_t worldId = Tamriel;

  SaveFile_::ChangeFormNPC_* npcForm = CreateChangeFormNPC();

  LoadGame::Run(pos, rot, worldId, nullptr, nullptr, npcForm);

  ctx->TaskGameThread([=] {
    auto form = Cast::Run<TESNPC>(sd::GetBaseObject(sd::GetPlayer()));
    if (!form)
      throw std::runtime_error("form == nullptr");

    SKYRIM_ASSERT(form->fullName.GetName() == npcForm->playerName);

    for (size_t i = 0; i < npcForm->face->options.size(); ++i)
      SKYRIM_ASSERT(form->faceMorph->option[i] == npcForm->face->options[i]);

    for (size_t i = 0; i < npcForm->face->presets.size(); ++i)
      SKYRIM_ASSERT(form->faceMorph->presets[i] == npcForm->face->presets[i]);

    SKYRIM_ASSERT(SaveFile_::RefID((int)form->headData->hairColor->formID) ==
                  npcForm->face->hairColorForm);

    SKYRIM_ASSERT(SaveFile_::RefID((int)form->headData->headTexture->formID) ==
                  npcForm->face->headTextureSet);

    SKYRIM_ASSERT(uint32_t((form->color.blue * 256 * 256) |
                           (form->color.green * 256) | form->color.red) ==
                  npcForm->face->bodySkinColor);

    SKYRIM_ASSERT(form->numHeadParts == npcForm->face->headParts.size());

    for (size_t i = 0; i < form->numHeadParts; ++i)
      SKYRIM_ASSERT(SaveFile_::RefID((int)form->headparts[i]->formID) ==
                    npcForm->face->headParts[i]);

    SKYRIM_ASSERT(SaveFile_::RefID((int)form->race.race->formID) ==
                  npcForm->race->myRaceNow);

    SKYRIM_ASSERT(sd::GetSex(form) == *npcForm->gender);

    ctx->Pass();
  });
}