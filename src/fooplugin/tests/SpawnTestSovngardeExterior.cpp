#include "Tests.h"
#include <skyrim_plugin_base/utils/LoadGame.h>

SKYRIM_TEST_CASE(SpawnTestSovngardeExterior)
{

  std::array<float, 3> pos = { 63574.0938f, 84895.9688f, 8243.3457f },
                       rot = { 0.0000f, -0.0000f, 0.0000f };

  uint32_t worldId = Sovngarde;

  LoadGame::Run(pos, rot, worldId);

  ctx->TaskGameThread([=] {
    std::array<float, 3> realPos = GetPlayerPos();
    std::array<float, 3> realRot = GetPlayerRot();
    auto world = sd::GetWorldSpace(sd::GetPlayer());

    SKYRIM_ASSERT(world != nullptr);
    SKYRIM_ASSERT(world->formID == worldId);
    SKYRIM_ASSERT(abs(pos[0] - realPos[0]) < 50.f);
    SKYRIM_ASSERT(abs(pos[1] - realPos[1]) < 50.f);
    SKYRIM_ASSERT(abs(pos[2] - realPos[2]) < 50.f);

    ctx->Pass();
  });
}