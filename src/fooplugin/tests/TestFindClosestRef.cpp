#include "Tests.h"
#include <skyrim_plugin_base/utils/FindClosestRef.h>
#include <skyrim_plugin_base/utils/Teleport.h>

SKYRIM_TEST_CASE(TestFindClosestRef)
{
  Teleport::Run(sd::GetPlayer(), 0x32ae7,
                { 2496.0000f, 2304.0000f, 6976.0000f }, { 0, 0, 0 },
                Teleport::Wait::False);
  ctx->TaskGameThread([=] {
    FindClosestRef::Cache cache;

    {
      auto ref =
        FindClosestRef::Run({ 2623.4099f, 1984.1807f, 7047.7139f }, cache);
      SKYRIM_ASSERT(ref);
      SKYRIM_ASSERT(ref->formID == 0x32aff);
    }

    {
      auto pl = sd::GetPlayer();
      auto ref = FindClosestRef::Run(
        { sd::GetPositionX(pl), sd::GetPositionY(pl), sd::GetPositionZ(pl) },
        cache);
      SKYRIM_ASSERT(ref);
      SKYRIM_ASSERT(ref->formType != kFormType_Character);
    }

    NiPoint3 tamrielObjectPos = { -27278.7539f, -34541.8281f, -4643.3081f };
    {
      auto ref = FindClosestRef::Run(tamrielObjectPos, cache);
      SKYRIM_ASSERT(!ref);

      Teleport::Run(sd::GetPlayer(), 0x3c, tamrielObjectPos, { 0, 0, 0 },
                    Teleport::Wait::True);
      ref = FindClosestRef::Run(tamrielObjectPos, cache);
      SKYRIM_ASSERT(ref);
      SKYRIM_ASSERT(ref->formID == 0xa81e3);

      // Performance
      auto was = std::chrono::steady_clock::now();
      for (int i = 0; i < 1'000; ++i) {
        float n = i % 1000;
        volatile auto r =
          FindClosestRef::Run(tamrielObjectPos + NiPoint3{ n, n, n }, cache);
      }
      int ms = std::chrono::duration_cast<std::chrono::milliseconds>(
                 std::chrono::steady_clock::now() - was)
                 .count();
      SKYRIM_ASSERT(ms < 200);
    }

    ctx->Pass();
  });
}