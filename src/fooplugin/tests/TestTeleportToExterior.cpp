#include "Tests.h"
#include <skyrim_plugin_base/utils/Teleport.h>

SKYRIM_TEST_CASE(TestTeleportToExterior)
{
  auto player = sd::GetPlayer();

  NiPoint3 pos = { 165643.8281f, -69763.3125f, 8039.8496f };
  uint32_t worldId = 0x3c;

  Teleport::Run(player, worldId, pos, { 0, 0, 0 }, Teleport::Wait::True);

  ctx->TaskGameThread([=] {
    auto world = sd::GetWorldSpace(sd::GetPlayer());

    std::array<float, 3> realPos = GetPlayerPos();

    SKYRIM_ASSERT(abs(pos.x - realPos[0]) < 50.f);
    SKYRIM_ASSERT(abs(pos.y - realPos[1]) < 50.f);
    SKYRIM_ASSERT(abs(pos.z - realPos[2]) < 50.f);
    SKYRIM_ASSERT(world);
    SKYRIM_ASSERT(world->formID == worldId);

    ctx->Pass();
  });
}