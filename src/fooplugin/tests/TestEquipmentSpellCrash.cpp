#include "Tests.h"
#include <PapyrusWeapon.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/LoadGame.h>
#include <skyrim_plugin_base/utils/PlaceAtMePersistent.h>
#include <skyrim_plugin_base\utils\ApplyEquipment.h>
#include <skyrim_plugin_base\utils\GetEquipment.h>
#include <skyrim_plugin_base\utils\WeaponCreator.h>
#include <skyrim_plugin_base\utils\GetMovement.h>
#include <skyrim_plugin_base\utils\ApplyMovement.h>

SKYRIM_TEST_CASE(TestEquipmentSpellCrash)
{
  LoadGame::Run({ -86642.1875f, 8655.1689f, -3890.1050f },
                { 0.0000f, -0.0000f, 317.9684f }, Tamriel);

  ctx->TaskGameThread([=] {
    auto base = Cast::Run<TESNPC>(ID_TESNPC::Astrid);
    WeaponCreator weapCreator;
    Equipment equip;

    std::vector<uint32_t> spells = {
      ID_SpellItem::ConjureFlameAtronach,
      ID_SpellItem::ConjureFamiliar,
      ID_SpellItem::ConjureDragonPriest,
      ID_SpellItem::ConjureFrostAtronach,
      ID_SpellItem::Invisibility,
      ID_SpellItem::Fireball,
      ID_SpellItem::Firebolt,
      ID_SpellItem::FireRune,
      ID_SpellItem::HazardFireSpell,
      ID_SpellItem::HazardFireboltSpell,
      ID_SpellItem::Frostbite,
      ID_SpellItem::FrostCloak,
      ID_SpellItem::FrostRune,
      ID_SpellItem::Frenzy,
      ID_SpellItem::ConjureFlameAtronach,
      ID_SpellItem::ConjureFamiliar,
      ID_SpellItem::ConjureDragonPriest,
      ID_SpellItem::ConjureFrostAtronach,
      ID_SpellItem::Invisibility,
      ID_SpellItem::Fireball,
      ID_SpellItem::Firebolt,
      ID_SpellItem::FireRune,
      ID_SpellItem::HazardFireSpell,
      ID_SpellItem::HazardFireboltSpell,
      ID_SpellItem::Frostbite,
      ID_SpellItem::FrostCloak,
      ID_SpellItem::FrostRune,
      ID_SpellItem::Frenzy,
      ID_SpellItem::ConjureFlameAtronach,
      ID_SpellItem::ConjureFamiliar,
      ID_SpellItem::ConjureDragonPriest,
      ID_SpellItem::ConjureFrostAtronach,
      ID_SpellItem::Invisibility,
      ID_SpellItem::Fireball,
      ID_SpellItem::Firebolt,
      ID_SpellItem::FireRune,
      ID_SpellItem::HazardFireSpell,
      ID_SpellItem::HazardFireboltSpell,
      ID_SpellItem::Frostbite,
      ID_SpellItem::FrostCloak,
      ID_SpellItem::FrostRune,
      ID_SpellItem::Frenzy,
      ID_SpellItem::ConjureFlameAtronach,
      ID_SpellItem::ConjureFamiliar,
      ID_SpellItem::ConjureDragonPriest,
      ID_SpellItem::ConjureFrostAtronach,
      ID_SpellItem::Invisibility,
      ID_SpellItem::Fireball,
      ID_SpellItem::Firebolt,
      ID_SpellItem::FireRune,
      ID_SpellItem::HazardFireSpell,
      ID_SpellItem::HazardFireboltSpell,
      ID_SpellItem::Frostbite,
      ID_SpellItem::FrostCloak,
      ID_SpellItem::FrostRune,
      ID_SpellItem::Frenzy,
    };
    auto refr =
      Cast::Run<Actor>(PlaceAtMePersistent::Run(sd::GetPlayer(), base, equip));
    Equipment::EquippedWeapon info;

    sd::Wait(1000);
    auto equipment = GetEquipment::Run(sd::GetPlayer());
    ApplyEquipment::Run(refr, equipment, weapCreator);
    sd::StartCombat(refr, sd::GetPlayer());
    GetMovement::ActorCache refrCache;
    auto mov = GetMovement::Run(refr, refrCache);
    mov.isWeapDrawn = true;
    ApplyMovement::Run(refr, mov);
    sd::Wait(2000);

    SKYRIM_ASSERT(sd::IsWeaponDrawn(refr));

    for (size_t i = 0; i < spells.size()-1; ++i) {
      equipment.equippedWeapon.rightHand = spells[i];
      equipment.equippedWeapon.leftHand = spells[i+1];
      ApplyEquipment::Run(refr, equipment, weapCreator);
      sd::Wait(100);
    }

    ctx->Pass();
  });
}