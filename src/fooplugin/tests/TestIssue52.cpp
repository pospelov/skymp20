#include "Tests.h"
#include <skyrim_plugin_base/utils/ApplyEquipment.h>
#include <skyrim_plugin_base/utils/ApplyMovement.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/GetMovement.h>
#include <skyrim_plugin_base/utils/LoadGame.h>
#include <skyrim_plugin_base/utils/PlaceAtMePersistent.h>
#include <skyrim_plugin_base/utils/WeaponCreator.h>

SKYRIM_TEST_CASE(TestIssue52)
{
  LoadGame::Run({ -86642.1875f, 8655.1689f, -3890.1050f },
                { 0.0000f, -0.0000f, 317.9684f }, Tamriel);

  ctx->TaskGameThread([=] {
    WeaponCreator weapCreator;

    auto base = Cast::Run<TESNPC>(ID_TESNPC::Astrid);

    auto refr =
      Cast::Run<Actor>(PlaceAtMePersistent::Run(sd::GetPlayer(), base));
    SKYRIM_ASSERT(refr);

    Equipment equipment;
    equipment.equippedWeapon.leftHand = ID_TESObjectWEAP::DaedricSword;

    ApplyEquipment::Run(refr, equipment, weapCreator);

    GetMovement::ActorCache cache;
    auto mov = GetMovement::Run(sd::GetPlayer(), cache);
    mov.isWeapDrawn = true;
    ApplyMovement::Run(refr, mov);

    for (int i = 0; i < 20; ++i) {
      sd::Wait(100);
      ApplyEquipment::Run(refr, equipment, weapCreator);
    }
    SKYRIM_ASSERT(sd::IsWeaponDrawn(refr));

    bool left = true;

    {
      auto weap = sd::GetEquippedWeapon(refr, left);
      SKYRIM_ASSERT(weap);
      SKYRIM_ASSERT(
        WeaponCreator::GetOriginIdByCreatedId(weap->formID, weapCreator) ==
        ID_TESObjectWEAP::DaedricSword);
    }
    {
      auto weap = sd::GetEquippedWeapon(refr, !left);
      SKYRIM_ASSERT(weap);
      SKYRIM_ASSERT(WeaponCreator::GetOriginIdByCreatedId(
                      weap->formID, weapCreator) == ID_TESObjectWEAP::Unarmed);
    }

    ctx->Pass();
  });
}