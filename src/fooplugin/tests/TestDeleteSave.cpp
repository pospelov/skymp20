#include "Tests.h"
#include <skyrim_plugin_base/utils/LoadGame.h>

SKYRIM_TEST_CASE(TestDeleteSave)
{
  LoadGame::Run({ -86642.1875f, 8655.1689f, -3890.1050f },
                { 0.0000f, -0.0000f, 317.9684f }, Tamriel);

  ctx->TaskGameThread([=] {
    std::filesystem::path path =
      LoadGame::GetPathToMyDocuments() + L"\\My Games\\Skyrim\\Saves\\";

    for (auto& file : std::filesystem::directory_iterator(path)) {
      if (file.path().filename().generic_string().find("SKYMP2020-") !=
          std::string::npos)
        SKYRIM_ASSERT(0);
    }

    ctx->Pass();
  });
}