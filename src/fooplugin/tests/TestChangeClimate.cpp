#include "Tests.h"
#include <skyrim_plugin_base/utils/LoadGame.h>

namespace {

SaveFile_::Weather* CreateWeather(SaveFile_::RefID _climate,
                                  SaveFile_::RefID _weather,
                                  float _weatherPct = 0.0f)
{
  static SaveFile_::Weather weather;
  weather.climate = _climate;
  weather.weather = _weather;
  weather.prevWeather = _weather;
  weather.weatherPct = _weatherPct;

  return &weather;
}
}

SKYRIM_TEST_CASE(TestChangeClimate)
{
  std::array<float, 3> pos = { -86642.1875f, 8655.1689f, -3890.1050f },
                       rot = { 0.0000f, -0.0000f, 317.9684f };
  uint32_t worldId = Tamriel;

  auto weather = CreateWeather(SaveFile_::RefID(SovngardeClimate),
                               SaveFile_::RefID(HelgenAttackWeather));

  LoadGame::Run(pos, rot, worldId, nullptr, weather);

  ctx->TaskGameThread([=] {
    auto nowWeather = sd::GetCurrentWeather();
    SKYRIM_ASSERT(nowWeather->formID == HelgenAttackWeather);
    ctx->Pass();
  });
}