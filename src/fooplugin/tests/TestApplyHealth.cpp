#include "TestUtils.h"
#include "Tests.h"
#include <skyrim_plugin_base/utils/ApplyHealth.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/LoadGame.h>

SKYRIM_TEST_CASE(TestApplyHealth)
{
  std::array<float, 3> pos = { 165643.8281f, -69763.3125f, 8209.8496f };
  LoadGame::Run(pos, { 0, 0, 0 }, 0x3c);

  ctx->TaskGameThread([=] {
    FindClosestRef::Cache cache;

    std::shared_ptr<SmartRef> smartRef(
      new SmartRef({ pos[0], pos[1], pos[2] }, cache));

    ActorValueEntry health;
    health.base = 50.f;
    health.currentPercentage = 0.5f;

    ApplyHealth::Run(smartRef, health);

    sd::Wait(1000);

    auto ref = Cast::Run<Actor>(smartRef->GetRef());
    SKYRIM_ASSERT(ref);
    SKYRIM_ASSERT(abs(sd::GetBaseActorValue(ref, "health") -
                      ApplyHealth::g_realBaseHealth) < 1);
    SKYRIM_ASSERT(abs(sd::GetActorValuePercentage(ref, "health") - 0.5f) <
                  0.1f);

    ctx->Pass();
  });
}