#include "Tests.h"
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/LoadGame.h>
#include <skyrim_plugin_base/utils/SetFormId.h>

SKYRIM_TEST_CASE(TestCloneForm)
{
  auto x = Cast::Run<TESNPC>(ID_TESNPC::Adara);

  SKYRIM_ASSERT(x->formID == ID_TESNPC::Adara);
  SKYRIM_ASSERT(LookupFormByID(ID_TESNPC::Adara) == x);

  SetFormId::Run(x, 0xffff0000);

  SKYRIM_ASSERT(x->formID == 0xffff0000);
  SKYRIM_ASSERT(LookupFormByID(0xffff0000) == x);

  ctx->Pass();
}