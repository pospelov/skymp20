#include "Tests.h"
#include <PapyrusWeapon.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/LoadGame.h>
#include <skyrim_plugin_base/utils/PlaceAtMePersistent.h>
#include <skyrim_plugin_base\utils\ApplyEquipment.h>
#include <skyrim_plugin_base\utils\GetEquipment.h>
#include <skyrim_plugin_base\utils\WeaponCreator.h>

SKYRIM_TEST_CASE(TestEquipmentBow)
{
  LoadGame::Run({ -86642.1875f, 8655.1689f, -3890.1050f },
                { 0.0000f, -0.0000f, 317.9684f }, Tamriel);

  ctx->TaskGameThread([=] {
    auto base = Cast::Run<TESNPC>(ID_TESNPC::Astrid);
    WeaponCreator weapCreator;
    Equipment equip;

    auto refr =
      Cast::Run<Actor>(PlaceAtMePersistent::Run(sd::GetPlayer(), base, equip));
    Equipment::EquippedWeapon info;
    BGSEquipSlot* BothSlot =
      Cast::Run<BGSEquipSlot>(sd::GetFormById(0x00013F45));

    sd::Wait(1000);
    auto equipment = GetEquipment::Run(sd::GetPlayer());
    ApplyEquipment::Run(refr, equipment, weapCreator);

    sd::Wait(2000);

    SKYRIM_ASSERT(papyrusWeapon::GetEquipType((TESObjectWEAP*)sd::GetFormById(
                    ID_TESObjectWEAP::EbonyBow)) == BothSlot);

    SKYRIM_ASSERT(
      !sd::IsEquipped(refr, sd::GetFormById(ID_TESObjectWEAP::EbonyBow)));

    sd::EquipItem(sd::GetPlayer(), sd::GetFormById(ID_TESObjectWEAP::EbonyBow),
                  true, false);

    sd::Wait(1000);
    equipment = GetEquipment::Run(sd::GetPlayer());
    ApplyEquipment::Run(refr, equipment, weapCreator);

    sd::Wait(2000);

    info = WeaponCreator::GetCreatedIdByOriginId(ID_TESObjectWEAP::EbonyBow,
                                                 weapCreator);


      SKYRIM_ASSERT(sd::IsEquipped(refr, sd::GetFormById(info.rightHand)));


      SKYRIM_ASSERT(
        papyrusWeapon::GetEquipType((TESObjectWEAP*)sd::GetFormById(
          ID_TESObjectWEAP::EbonyBow)) == BothSlot);


      SKYRIM_ASSERT(papyrusWeapon::GetEquipType(
                      (TESObjectWEAP*)sd::GetFormById(info.rightHand)) ==
                    BothSlot);

    ctx->Pass();
  });
}

SKYRIM_TEST_CASE(TestEquipmentArrow)
{
  LoadGame::Run({ -86642.1875f, 8655.1689f, -3890.1050f },
                { 0.0000f, -0.0000f, 317.9684f }, Tamriel);

  ctx->TaskGameThread([=] {
    auto base = Cast::Run<TESNPC>(ID_TESNPC::Astrid);
    WeaponCreator weapCreator;
    Equipment equip;

    auto refr =
      Cast::Run<Actor>(PlaceAtMePersistent::Run(sd::GetPlayer(), base, equip));

    sd::Wait(1000);
    auto equipment = GetEquipment::Run(sd::GetPlayer());
    ApplyEquipment::Run(refr, equipment, weapCreator);

    sd::EquipItem(sd::GetPlayer(), sd::GetFormById(ID_TESAmmo::DaedricArrow),
                  true, false);
    SKYRIM_ASSERT(
      !sd::IsEquipped(refr, sd::GetFormById(ID_TESAmmo::DaedricArrow)));

    sd::Wait(1000);
    equipment = GetEquipment::Run(sd::GetPlayer());
    ApplyEquipment::Run(refr, equipment, weapCreator);

    sd::Wait(2000);

    SKYRIM_ASSERT(
      sd::IsEquipped(refr, sd::GetFormById(ID_TESAmmo::DaedricArrow)));

    ctx->Pass();
  });
}