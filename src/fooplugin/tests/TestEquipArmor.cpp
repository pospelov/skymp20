#include "Tests.h"
#include <scriptdragon/enums.h>
#include <skyrim_plugin_base/utils/ApplyEquipment.h>
#include <skyrim_plugin_base/utils/ApplyMovement.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/GetEquipment.h>
#include <skyrim_plugin_base/utils/GetLastAnimationEvent.h>
#include <skyrim_plugin_base/utils/GetMovement.h>
#include <skyrim_plugin_base/utils/LoadGame.h>
#include <skyrim_plugin_base/utils/PlaceAtMePersistent.h>

namespace {

void ChangeEquipment(WeaponCreator& weapCreator, Actor* refr)
{
  sd::Wait(1000);
  auto equipment = GetEquipment::Run(sd::GetPlayer());
  ApplyEquipment::Run(refr, equipment, weapCreator);
}
}

SKYRIM_TEST_CASE(TestEquipmentApplyArmor)
{
  LoadGame::Run({ -86642.1875f, 8655.1689f, -3890.1050f },
                { 0.0000f, -0.0000f, 317.9684f }, Tamriel);

  ctx->TaskGameThread([=] {
    WeaponCreator weapCreator;

    auto base = Cast::Run<TESNPC>(ID_TESNPC::Astrid);

    auto refr =
      Cast::Run<Actor>(PlaceAtMePersistent::Run(sd::GetPlayer(), base));
    SKYRIM_ASSERT(refr);

    ChangeEquipment(weapCreator, refr);

    sd::EquipItem(sd::GetPlayer(),
                  sd::GetFormById(ID_TESObjectARMO::ArmorDaedricCuirass), true,
                  false);

    SKYRIM_ASSERT(!sd::IsEquipped(
      refr, sd::GetFormById(ID_TESObjectARMO::ArmorDaedricCuirass)));

    ChangeEquipment(weapCreator, refr);
    sd::Wait(2000);
    SKYRIM_ASSERT(sd::IsEquipped(
      refr, sd::GetFormById(ID_TESObjectARMO::ArmorDaedricCuirass)));

    sd::EquipItem(sd::GetPlayer(),
                  sd::GetFormById(ID_TESObjectARMO::ArmorEbonyHelmet), true,
                  false);

    SKYRIM_ASSERT(!sd::IsEquipped(
      refr, sd::GetFormById(ID_TESObjectARMO::ArmorEbonyHelmet)));

    ChangeEquipment(weapCreator, refr);
    sd::Wait(2000);
    SKYRIM_ASSERT(sd::IsEquipped(
      refr, sd::GetFormById(ID_TESObjectARMO::ArmorEbonyHelmet)));

    ctx->Pass();
  });
}

SKYRIM_TEST_CASE(TestEquipmentRemoveArmorWithWeaponsActive)
{
  LoadGame::Run({ -86642.1875f, 8655.1689f, -3890.1050f },
                { 0.0000f, -0.0000f, 317.9684f }, Tamriel);

  ctx->TaskGameThread([=] {
    WeaponCreator weapCreator;

    auto base =
      Cast::Run<TESNPC>(ID_TESNPC::AADeleteWhenDoneTestJeremyRegular);

    auto refr =
      Cast::Run<Actor>(PlaceAtMePersistent::Run(sd::GetPlayer(), base));
    SKYRIM_ASSERT(refr);

    ChangeEquipment(weapCreator, refr);

    GetMovement::ActorCache refrCache;
    auto mov = GetMovement::Run(refr, refrCache);
    mov.isWeapDrawn = true;
    ApplyMovement::Run(refr, mov);

    while (!sd::IsWeaponDrawn(refr)) {
      sd::Wait(0);
    }
    sd::Wait(2000);

    // Ignore possible Unequip/EquipWeap animations we already have seen
    while (1) {
      auto lastAnimation =
        GetLastAnimationEvent::Run(refr, "TestUnequipArmorWithWeapons");
      if (lastAnimation.size() == 0)
        break;
    }

    Equipment noArmor = GetEquipment::Run(sd::GetPlayer());
    SKYRIM_ASSERT(!noArmor.equippedArmor.empty());
    noArmor.equippedArmor.clear();
    ApplyEquipment::Run(refr, noArmor, weapCreator);

    for (int i = 0; i < 20; ++i) {
      auto lastAnimation =
        GetLastAnimationEvent::Run(refr, "TestUnequipArmorWithWeapons");

      std::transform(lastAnimation.begin(), lastAnimation.end(),
                     lastAnimation.begin(), ::tolower);

      SKYRIM_ASSERT(lastAnimation.find("equip") == std::string::npos);

      sd::Wait(100);
    }
    ctx->Pass();
  });
}