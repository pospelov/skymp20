#pragma once
#include "Tests.h"
#include <skyrim_plugin_base/utils/LoadGame.h>

SKYRIM_TEST_CASE(SpawnTestSovngardeInterior)
{

  std::array<float, 3> pos = { -2768.0000f, 1584.0000f, -192.0000f },
                       rot = { 0.0000f, -0.0000f, 180.0000f };

  uint32_t worldId = Sovngarde_HallOfValor;

  LoadGame::Run(pos, rot, worldId);

  ctx->TaskGameThread([=] {
    std::array<float, 3> realPos = GetPlayerPos();
    std::array<float, 3> realRot = GetPlayerRot();
    auto world = sd::GetParentCell(sd::GetPlayer());

    SKYRIM_ASSERT(world != nullptr);
    SKYRIM_ASSERT(world->formID == worldId);
    SKYRIM_ASSERT(abs(pos[0] - realPos[0]) < 50.f);
    SKYRIM_ASSERT(abs(pos[1] - realPos[1]) < 50.f);
    SKYRIM_ASSERT(abs(pos[2] - realPos[2]) < 50.f);

    ctx->Pass();
  });
}