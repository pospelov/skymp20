#include "Tests.h"
#include <PapyrusWeapon.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/DisableAnimationEvent.h>
#include <skyrim_plugin_base/utils/LoadGame.h>
#include <skyrim_plugin_base/utils/PlaceAtMePersistent.h>

SKYRIM_TEST_CASE(TestUnplannedCombatActions)
{
  LoadGame::Run({ -86642.1875f, 8655.1689f, -3890.1050f },
                { 0.0000f, -0.0000f, 317.9684f }, Tamriel);

  ctx->TaskGameThread([=] {
    DisableAnimationEvent::DisableNpcIdles();

    auto base = Cast::Run<TESNPC>(ID_TESNPC::Astrid);

    Equipment equip;
    const auto playerHP = sd::GetActorValue(sd::GetPlayer(), "health");

    SKYRIM_ASSERT(playerHP > 0.0f);

    auto refr =
      Cast::Run<Actor>(PlaceAtMePersistent::Run(sd::GetPlayer(), base, equip));

    SKYRIM_ASSERT(refr);
    sd::StartCombat(refr, sd::GetPlayer());
    sd::Wait(3000);

    SKYRIM_ASSERT(refr->IsInCombat());
    SKYRIM_ASSERT(playerHP == sd::GetActorValue(sd::GetPlayer(), "health"));
    ctx->Pass();
  });
}