#include "Tests.h"
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/GetEquipment.h>
#include <skyrim_plugin_base/utils/LoadGame.h>
#include <skyrim_plugin_base/utils/PlaceAtMePersistent.h>

SKYRIM_TEST_CASE(TestEquipmentGet)
{
  LoadGame::Run({ -86642.1875f, 8655.1689f, -3890.1050f },
                { 0.0000f, -0.0000f, 317.9684f }, Tamriel);

  ctx->TaskGameThread([=] {
    auto base = Cast::Run<TESNPC>(ID_TESNPC::Astrid);

    Equipment equip;
    equip.equippedArmor = {
      ID_TESObjectARMO::ArmorSteelPlateCuirass,
      ID_TESObjectARMO::ArmorSteelPlateBoots,
      ID_TESObjectARMO::ArmorSteelPlateGauntlets,
      ID_TESObjectARMO::ArmorSteelPlateHelmet,
    };

    auto refr =
      Cast::Run<Actor>(PlaceAtMePersistent::Run(sd::GetPlayer(), base, equip));

    SKYRIM_ASSERT(refr);

    sd::SetGodMode(true);
    sd::StartCombat(refr, sd::GetPlayer());
    while (!sd::IsWeaponDrawn(refr))
      sd::Wait(0);
    sd::Wait(2000);

    Equipment equipment = GetEquipment::Run(refr);

    SKYRIM_ASSERT(0 ==
                  equipment.equippedWeapon.rightHand);

    SKYRIM_ASSERT(0 ==
                  equipment.equippedWeapon.leftHand);

    SKYRIM_ASSERT(equip.equippedArmor == equipment.equippedArmor);

    ctx->Pass();
  });
}