#include "Tests.h"
#include <PapyrusWeapon.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/LoadGame.h>
#include <skyrim_plugin_base/utils/PlaceAtMePersistent.h>
#include <skyrim_plugin_base\utils\ApplyEquipment.h>
#include <skyrim_plugin_base\utils\GetEquipment.h>
#include <skyrim_plugin_base\utils\WeaponCreator.h>

SKYRIM_TEST_CASE(TestEquipmentSpella)
{
  LoadGame::Run({ -86642.1875f, 8655.1689f, -3890.1050f },
                { 0.0000f, -0.0000f, 317.9684f }, Tamriel);

  ctx->TaskGameThread([=] {
    auto base = Cast::Run<TESNPC>(ID_TESNPC::Astrid);
    WeaponCreator weapCreator;
    Equipment equip;

    auto refr =
      Cast::Run<Actor>(PlaceAtMePersistent::Run(sd::GetPlayer(), base, equip));

    auto equipment = GetEquipment::Run(sd::GetPlayer());
    ApplyEquipment::Run(refr, equipment, weapCreator);
    sd::Wait(1000);

    sd::EquipSpell(
      sd::GetPlayer(),
      Cast::Run<SpellItem>(sd::GetFormById(ID_SpellItem::Fireball)), false);
    sd::EquipSpell(
      sd::GetPlayer(),
      Cast::Run<SpellItem>(sd::GetFormById(ID_SpellItem::FrostCloak)), true);

    sd::Wait(2000);
    equipment = GetEquipment::Run(sd::GetPlayer());
    ApplyEquipment::Run(refr, equipment, weapCreator);

    SKYRIM_ASSERT(refr->GetEquippedObject(false));
    SKYRIM_ASSERT(refr->GetEquippedObject(true));

    auto leftHand = WeaponCreator::GetOriginIdByCreatedId(
      refr->GetEquippedObject(false)->formID, weapCreator);
    auto rightHand = WeaponCreator::GetOriginIdByCreatedId(
      refr->GetEquippedObject(true)->formID, weapCreator);

     SKYRIM_ASSERT(leftHand == ID_SpellItem::FrostCloak);
     SKYRIM_ASSERT(rightHand == ID_SpellItem::Fireball);

    ctx->Pass();
  });
}