#include "KeyDown.h"
#include "TestUtils.h"
#include "Tests.h"
#include <imgui.h>
#include <scriptdragon/obscript.h>
#include <skyrim_plugin_base/utils/ApplyMovement.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/FindClosestRef.h>
#include <skyrim_plugin_base/utils/GetCellOrWorld.h>
#include <skyrim_plugin_base/utils/GetLastAnimationEvent.h>
#include <skyrim_plugin_base/utils/GetMovement.h>
#include <skyrim_plugin_base/utils/LoadGame.h>
#include <skyrim_plugin_base/utils/Misc.h>
#include <skyrim_plugin_base/utils/SmartRef.h>

namespace {

bool IsRefNear(TESObjectREFR* ref1, TESObjectREFR* ref2)
{
  if (!ref1)
    throw std::runtime_error("IsRefNear: ref1 was nullptr");
  if (!ref2)
    throw std::runtime_error("IsRefNear: ref2 was nullptr");

  float distance =
    (GetPlayerPos<NiPoint3>(ref1) - GetPlayerPos<NiPoint3>(ref2)).Length();
  return distance < 25.f &&
    sd::GetWorldSpace(ref1) == sd::GetWorldSpace(ref2) &&
    sd::GetParentCell(ref1) == sd::GetParentCell(ref2);
}

void ApplyPlayerMovement(std::shared_ptr<SmartRef> smart,
                         GetMovement::ActorCache& c)
{
  for (int i = 0; i < 5; ++i) {
    auto ref = Cast::Run<Actor>(smart->GetRef());
    SKYRIM_ASSERT(ref);
    ApplyMovement::Run(ref, GetMovement::Run(sd::GetPlayer(), c));
    sd::Wait(100);
  }
}

void ApplyMovementInLoop(std::shared_ptr<SmartRef> smart, Movement mov)
{
  for (int i = 0; i < 3; ++i) {
    auto ref = Cast::Run<Actor>(smart->GetRef());
    SKYRIM_ASSERT(ref);
    ApplyMovement::Run(ref, mov);
    sd::Wait(100);
  }
}

void DrawSheatheWeaponAndWait(bool draw)
{
  sd::GetPlayer()->DrawSheatheWeapon(draw);
  while (sd::IsWeaponDrawn(sd::GetPlayer()) != draw) {
    sd::Wait(0);
  }
}
}

namespace {
using TWait = std::function<void(int)>;

void TestInJumpState(Actor* ac, GetMovement::ActorCache& c, TWait wait)
{
  SKYRIM_ASSERT(!GetMovement::Run(ac, c).isInJumpState);
  KeyDown(VK_SPACE);
  while (!Misc::GetAnimVar<bool>(ac, "bInJumpState"))
    wait(0);
  SKYRIM_ASSERT(GetMovement::Run(ac, c).isInJumpState);
  while (Misc::GetAnimVar<bool>(ac, "bInJumpState"))
    wait(0);
  SKYRIM_ASSERT(!GetMovement::Run(ac, c).isInJumpState);
}

void TestWeapDrawn(Actor* ac, GetMovement::ActorCache& c, TWait wait)
{
  SKYRIM_ASSERT(!GetMovement::Run(ac, c).isWeapDrawn);
  ac->animGraphHolder.SendAnimationEvent(Misc::FixedStr("WeapEquip"));
  SKYRIM_ASSERT(GetMovement::Run(ac, c).isWeapDrawn);

  while (!sd::IsWeaponDrawn(ac))
    wait(50);
  SKYRIM_ASSERT(GetMovement::Run(ac, c).isWeapDrawn);

  wait(1000);
  SKYRIM_ASSERT(GetMovement::Run(ac, c).isWeapDrawn);

  ac->animGraphHolder.SendAnimationEvent(Misc::FixedStr("Unequip"));
  ac->animGraphHolder.SendAnimationEvent(Misc::FixedStr("asdasdasd"));
  SKYRIM_ASSERT(!GetMovement::Run(ac, c).isWeapDrawn);
  while (sd::IsWeaponDrawn(ac))
    wait(50);
  SKYRIM_ASSERT(!GetMovement::Run(ac, c).isWeapDrawn);

  wait(1000);
  SKYRIM_ASSERT(!GetMovement::Run(ac, c).isWeapDrawn);
}

void TestSneaking(Actor* ac, GetMovement::ActorCache& c, TWait wait)
{
  SKYRIM_ASSERT(!GetMovement::Run(ac, c).isSneaking);
  ac->animGraphHolder.SendAnimationEvent(Misc::FixedStr("SneakStart"));
  SKYRIM_ASSERT(GetMovement::Run(ac, c).isSneaking);

  while (!Misc::GetAnimVar<bool>(ac, "IsSneaking"))
    wait(50);
  SKYRIM_ASSERT(GetMovement::Run(ac, c).isSneaking);

  wait(1000);
  SKYRIM_ASSERT(GetMovement::Run(ac, c).isSneaking);

  ac->animGraphHolder.SendAnimationEvent(Misc::FixedStr("SneakStop"));
  ac->animGraphHolder.SendAnimationEvent(Misc::FixedStr("asdasdasd"));
  SKYRIM_ASSERT(!GetMovement::Run(ac, c).isSneaking);
  while (Misc::GetAnimVar<bool>(ac, "IsSneaking"))
    wait(50);
  SKYRIM_ASSERT(!GetMovement::Run(ac, c).isSneaking);

  wait(1000);
  SKYRIM_ASSERT(!GetMovement::Run(ac, c).isSneaking);
}

void TestBlocking(Actor* ac, GetMovement::ActorCache& c, TWait wait)
{
  ac->animGraphHolder.SendAnimationEvent(Misc::FixedStr("WeapEquip"));
  while (!sd::IsWeaponDrawn(ac))
    wait(0);
  wait(1000);

  SKYRIM_ASSERT(!GetMovement::Run(ac, c).isBlocking);
  ac->animGraphHolder.SendAnimationEvent(Misc::FixedStr("blockstart"));
  ac->animGraphHolder.SendAnimationEvent(Misc::FixedStr("dsdsdssdds"));
  SKYRIM_ASSERT(GetMovement::Run(ac, c).isBlocking);

  while (!sd::Obscript::IsBlocking(ac))
    wait(0);
  SKYRIM_ASSERT(GetMovement::Run(ac, c).isBlocking);

  std::list<bool> isBlocking;
  ac->animGraphHolder.SendAnimationEvent(Misc::FixedStr("bashStart"));
  while (!Misc::GetAnimVar<bool>(ac, "IsBashing")) {
    wait(0);
    SKYRIM_ASSERT(GetMovement::Run(ac, c).isBlocking);
  }
  while (1) {
    wait(0);
    if (Misc::GetAnimVar<bool>(ac, "IsBashing")) {
      wait(100);
      break;
    }
  }
  SKYRIM_ASSERT(GetMovement::Run(ac, c).isBlocking);
  while (Misc::GetAnimVar<bool>(ac, "IsBashing")) {
    wait(0);
  }
  SKYRIM_ASSERT(!GetMovement::Run(ac, c).isBlocking);

  ac->animGraphHolder.SendAnimationEvent(Misc::FixedStr("blockstart"));
  SKYRIM_ASSERT(GetMovement::Run(ac, c).isBlocking);
  while (!sd::Obscript::IsBlocking(ac))
    wait(0);

  ac->animGraphHolder.SendAnimationEvent(Misc::FixedStr("blockStop"));
  SKYRIM_ASSERT(!GetMovement::Run(ac, c).isBlocking);
  while (sd::Obscript::IsBlocking(ac))
    wait(0);
  SKYRIM_ASSERT(!GetMovement::Run(ac, c).isBlocking);

  ac->animGraphHolder.SendAnimationEvent(Misc::FixedStr("Unequip"));
  while (sd::IsWeaponDrawn(ac))
    wait(0);
  wait(1000);
}

void TestRunModes(Actor* ac, GetMovement::ActorCache& c, TWait wait)
{
  SKYRIM_ASSERT(GetMovement::Run(ac, c).runMode == RunMode::Standing);

  {
    KeyDown w('W');
    wait(1000);
    SKYRIM_ASSERT(GetMovement::Run(ac, c).runMode == RunMode::Running);
  }
  wait(1000);
  SKYRIM_ASSERT(GetMovement::Run(ac, c).runMode == RunMode::Standing);

  {
    KeyDown w('W'), shift(VK_LSHIFT);
    wait(1000);
    SKYRIM_ASSERT(GetMovement::Run(ac, c).runMode == RunMode::Walking);
  }
  wait(1000);
  SKYRIM_ASSERT(GetMovement::Run(ac, c).runMode == RunMode::Standing);

  {
    KeyDown w('W'), alt(VK_LMENU);
    wait(1000);
    SKYRIM_ASSERT(GetMovement::Run(ac, c).runMode == RunMode::Sprinting);
  }
  wait(1000);
  SKYRIM_ASSERT(GetMovement::Run(ac, c).runMode == RunMode::Standing);

  std::vector<RunMode> runModes;
  {
    KeyDown w('W'), alt(VK_LMENU);
    for (int i = 0; i < 100; ++i) {
      auto mode = GetMovement::Run(ac, c).runMode;
      if (runModes.empty() || runModes.back() != mode)
        runModes.push_back(mode);
      wait(10);
    }
  }
  const std::vector<RunMode> expected = { RunMode::Standing, RunMode::Walking,
                                          RunMode::Running,
                                          RunMode::Sprinting };
  const std::vector<RunMode> expected2 = { RunMode::Standing, RunMode::Walking,
                                           RunMode::Running,
                                           RunMode::Sprinting,
                                           RunMode::Running };
  if (runModes != expected && runModes != expected2) {
    std::stringstream ss;
    for (auto v : runModes)
      ss << int(v) << " ";
    throw std::runtime_error("Unexpected run modes: " + ss.str());
  }
}

void TestDirection(Actor* ac, GetMovement::ActorCache& c, TWait wait)
{
  SKYRIM_ASSERT(GetMovement::Run(ac, c).direction == 0);
  {
    KeyDown w('W'), alt('D');
    wait(1000);
    SKYRIM_ASSERT(GetMovement::Run(ac, c).direction == 45);
  }
}
}

SKYRIM_TEST_CASE(MovementGet)
{
  LoadGame::Run({ 165643.8281f, -69763.3125f, 8039.1234f }, { 0, 0, 180.f },
                0x3c);

  ctx->TaskGameThread([=] {
    auto ac = sd::GetPlayer();
    GetMovement::ActorCache c;

    Movement mov = GetMovement::Run(ac, c);
    SKYRIM_ASSERT(mov.cellOrWorld == 0x3c);
    SKYRIM_ASSERT(abs(mov.pos[0] - 165643.8281f) < 5.f);
    SKYRIM_ASSERT(abs(mov.pos[1] - -69763.3125f) < 5.f);
    SKYRIM_ASSERT(abs(mov.pos[2] - 8039.1234f) < 100.f);
    SKYRIM_ASSERT(abs(mov.angleZ - 180.f) < 1.f);

    auto wait = sd::Wait;
    TestInJumpState(ac, c, wait);
    TestWeapDrawn(ac, c, wait);
    TestSneaking(ac, c, wait);
    TestBlocking(ac, c, wait);
    TestRunModes(ac, c, wait);
    TestDirection(ac, c, wait);

    ctx->Pass();
  });
}

SKYRIM_TEST_CASE(NextAnimationEvent)
{
  LoadGame::Run({ 165643.8281f, -69763.3125f, 8209.8496f }, { 0, 0, 0 }, 0x3c);

  ctx->TaskGameThread([=] {
    std::string s;

    s = GetLastAnimationEvent::Run(sd::GetPlayer(), "Channel1");
    SKYRIM_ASSERT(s == "");

    sd::GetPlayer()->animGraphHolder.SendAnimationEvent(
      new BSFixedString("JumpStart"));

    s = GetLastAnimationEvent::Run(sd::GetPlayer(), "Channel1");
    SKYRIM_ASSERT(s == "JumpStart");
    s = GetLastAnimationEvent::Run(sd::GetPlayer(), "Channel1");
    SKYRIM_ASSERT(s == "");

    sd::GetPlayer()->animGraphHolder.SendAnimationEvent(
      new BSFixedString("JumpStop"));
    sd::Wait(int(GetLastAnimationEvent::StoringDurationMs() * 0.9));
    s = GetLastAnimationEvent::Run(sd::GetPlayer(), "Channel1");
    SKYRIM_ASSERT(s == "JumpStop");
    s = GetLastAnimationEvent::Run(sd::GetPlayer(), "Channel1");
    SKYRIM_ASSERT(s == "");

    sd::GetPlayer()->animGraphHolder.SendAnimationEvent(
      new BSFixedString("SneakStart"));
    sd::Wait(int(GetLastAnimationEvent::StoringDurationMs() * 1.1));
    s = GetLastAnimationEvent::Run(sd::GetPlayer(), "Channel1");
    SKYRIM_ASSERT(s == "");
    s = GetLastAnimationEvent::Run(sd::GetPlayer(), "Channel2");
    SKYRIM_ASSERT(s == "");

    sd::GetPlayer()->animGraphHolder.SendAnimationEvent(
      new BSFixedString("SneakStop"));

    s = GetLastAnimationEvent::Run(sd::GetPlayer(), "Channel1");
    SKYRIM_ASSERT(s == "SneakStop");
    s = GetLastAnimationEvent::Run(sd::GetPlayer(), "Channel1");
    SKYRIM_ASSERT(s == "");
    s = GetLastAnimationEvent::Run(sd::GetPlayer(), "Channel2");
    SKYRIM_ASSERT(s == "SneakStop");
    s = GetLastAnimationEvent::Run(sd::GetPlayer(), "Channel2");
    SKYRIM_ASSERT(s == "");

    ctx->Pass();
  });
}

SKYRIM_TEST_CASE(SmartRefSpawnsAtClosestRef)
{
  LoadGame::Run({ 165643.8281f, -69763.3125f, 8209.8496f }, { 0, 0, 0 }, 0x3c);

  ctx->TaskGameThread([=] {
    FindClosestRef::Cache cache;

    auto playerStartingPos = GetPlayerPos<NiPoint3>();
    std::shared_ptr<SmartRef> smart(new SmartRef(playerStartingPos, cache));

    auto ref = smart->GetRef();

    SKYRIM_ASSERT(ref);
    SKYRIM_ASSERT(FindClosestRef::Run(GetPlayerPos<NiPoint3>(ref), cache) ==
                  FindClosestRef::Run(playerStartingPos, cache));
    ctx->Pass();
  });
}

SKYRIM_TEST_CASE(MovementApplyExterior)
{
  LoadGame::Run({ 165643.8281f, -69763.3125f, 8209.8496f }, { 0, 0, 0 }, 0x3c);

  ctx->TaskGameThread([=] {
    auto playerStartingPos = GetPlayerPos<NiPoint3>();

    FindClosestRef::Cache cache;
    std::shared_ptr<SmartRef> smart(new SmartRef(playerStartingPos, cache));

    sd::SetPosition(sd::GetPlayer(), 17542.8770f, -10722.4434, -4683.3721);
    sd::Wait(1000);

    GetMovement::ActorCache c;
    ApplyPlayerMovement(smart, c);

    auto ref = Cast::Run<Actor>(smart->GetRef());
    SKYRIM_ASSERT(ref);
    SKYRIM_ASSERT(IsRefNear(ref, sd::GetPlayer()));

    ctx->Pass();
  });
}

SKYRIM_TEST_CASE(MovementApplyInterior)
{
  LoadGame::Run({ 165643.8281f, -69763.3125f, 8209.8496f }, { 0, 0, 0 }, 0x3c);

  ctx->TaskGameThread([=] {
    auto playerStartingPos = GetPlayerPos<NiPoint3>();

    FindClosestRef::Cache cache;
    std::shared_ptr<SmartRef> smart(new SmartRef(playerStartingPos, cache));

    SKYRIM_ASSERT(smart->GetRef());

    TeleportToQasmoke();

    GetMovement::ActorCache c;
    ApplyPlayerMovement(smart, c);

    SKYRIM_ASSERT(IsRefNear(smart->GetRef(), sd::GetPlayer()));

    ctx->Pass();
  });
}

SKYRIM_TEST_CASE(MovementApplyCommon)
{
  LoadGame::Run({ 165643.8281f, -69763.3125f, 8209.8496f }, { 0, 0, 0 }, 0x3c);

  ctx->TaskGameThread([=] {
    auto playerStartingPos = GetPlayerPos<NiPoint3>();

    FindClosestRef::Cache cache;
    GetMovement::ActorCache c;
    std::shared_ptr<SmartRef> smart(new SmartRef(playerStartingPos, cache));

    SKYRIM_ASSERT(smart->GetRef());

    {
      auto ac = Cast::Run<Actor>(smart->GetRef());
      SKYRIM_ASSERT(ac);
      Movement mov = GetMovement::Run(ac, c);
      mov.runMode = RunMode::Running;
      ApplyMovementInLoop(smart, mov);
    }
    {
      auto ac = Cast::Run<Actor>(smart->GetRef());
      SKYRIM_ASSERT(ac);
      SKYRIM_ASSERT(sd::IsRunning(ac));
    }

    {
      auto ac = Cast::Run<Actor>(smart->GetRef());
      SKYRIM_ASSERT(ac);
      Movement mov = GetMovement::Run(ac, c);
      mov.runMode = RunMode::Standing;
      ApplyMovementInLoop(smart, mov);
    }
    {
      auto ac = Cast::Run<Actor>(smart->GetRef());
      SKYRIM_ASSERT(ac);
      SKYRIM_ASSERT(!sd::IsRunning(ac));
    }

    ctx->Pass();
  });
}

SKYRIM_TEST_CASE(MovementApplyWeapDrawn)
{
  LoadGame::Run({ 165643.8281f, -69763.3125f, 8209.8496f }, { 0, 0, 0 }, 0x3c);

  ctx->TaskGameThread([=] {
    auto playerStartingPos = GetPlayerPos<NiPoint3>();

    FindClosestRef::Cache cache;
    GetMovement::ActorCache c;
    std::shared_ptr<SmartRef> smart(new SmartRef(playerStartingPos, cache));

    ApplyPlayerMovement(smart, c);

    sd::Wait(1000);

    {
      auto ref = Cast::Run<Actor>(smart->GetRef());
      SKYRIM_ASSERT(ref);
      SKYRIM_ASSERT(!sd::IsWeaponDrawn(ref));
    }

    DrawSheatheWeaponAndWait(true);
    ApplyPlayerMovement(smart, c);
    sd::Wait(1000);

    {
      auto ref = Cast::Run<Actor>(smart->GetRef());
      SKYRIM_ASSERT(ref);
      SKYRIM_ASSERT(sd::IsWeaponDrawn(ref));
    }

    sd::Wait(5000);
    {
      auto ref = Cast::Run<Actor>(smart->GetRef());
      SKYRIM_ASSERT(ref);
      SKYRIM_ASSERT(sd::IsWeaponDrawn(ref));
    }

    DrawSheatheWeaponAndWait(false);
    ApplyPlayerMovement(smart, c);
    sd::Wait(1000);

    {
      auto ref = Cast::Run<Actor>(smart->GetRef());
      SKYRIM_ASSERT(ref);
      SKYRIM_ASSERT(!sd::IsWeaponDrawn(ref));
    }

    sd::Wait(5000);
    {
      auto ref = Cast::Run<Actor>(smart->GetRef());
      SKYRIM_ASSERT(ref);
      SKYRIM_ASSERT(!sd::IsWeaponDrawn(ref));
    }

    ctx->Pass();
  });
}