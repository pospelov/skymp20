#pragma once
#include <common/IPrefix.h>
#include <scriptdragon/skyscript.h>
#include <skse/GameReferences.h>
#include <skyrim_plugin_base/utils/Teleport.h>

inline void TeleportToQasmoke()
{
  Teleport::Run(sd::GetPlayer(), 0x32ae7,
                { 2496.0000f, 2304.0000f, 6976.0000f }, { 0, 0, 0 },
                Teleport::Wait::True);
}