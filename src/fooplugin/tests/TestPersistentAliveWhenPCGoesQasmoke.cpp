#include "TestUtils.h"
#include "Tests.h"
#include <common/IPrefix.h>
#include <skse/GameReferences.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/LoadGame.h>
#include <skyrim_plugin_base/utils/PlaceAtMePersistent.h>
#include <skyrim_plugin_base/utils/Teleport.h>

SKYRIM_TEST_CASE(TestPersistentAliveWhenPCGoesQasmoke)
{
  LoadGame::Run({ 165643.8281f, -69763.3125f, 8209.8496f }, { 0, 0, 0 }, 0x3c);

  ctx->TaskGameThread([=] {
    uint32_t id = PlaceAtMePersistent::Run(sd::GetPlayer(),
                                           sd::GetBaseObject(sd::GetPlayer()));

    auto refrWas = Cast::Run<TESObjectREFR>(id);

    SKYRIM_ASSERT(refrWas);
    SKYRIM_ASSERT(sd::GetParentCell(refrWas) ==
                  sd::GetParentCell(sd::GetPlayer()));
    SKYRIM_ASSERT(sd::GetWorldSpace(refrWas) ==
                  sd::GetWorldSpace(sd::GetPlayer()));

    TeleportToQasmoke();

    SKYRIM_ASSERT(!sd::GetWorldSpace(sd::GetPlayer()));
    SKYRIM_ASSERT(Cast::Run<TESObjectREFR>(id));

    ctx->Pass();
  });
}