#include "Tests.h"
#include <skyrim_plugin_base/utils/LoadGame.h>

SKYRIM_TEST_CASE(SpawnTestTamrielExterior)
{
  std::array<float, 3> pos = { -86642.1875f, 8655.1689f, -3890.1050f },
                       rot = { 0.0000f, -0.0000f, 317.9684f };
  uint32_t worldId = Tamriel;

  LoadGame::Run(pos, rot, worldId);

  ctx->TaskGameThread([=] {
    std::array<float, 3> realPos = GetPlayerPos();
    std::array<float, 3> realRot = GetPlayerRot();
    auto world = sd::GetWorldSpace(sd::GetPlayer());

    SKYRIM_ASSERT(abs(pos[0] - realPos[0]) < 5.f);
    SKYRIM_ASSERT(abs(pos[1] - realPos[1]) < 5.f);
    SKYRIM_ASSERT(abs(pos[2] - realPos[2]) < 50.f);
    SKYRIM_ASSERT(world != nullptr);
    SKYRIM_ASSERT(world->formID == worldId);
    ctx->Pass();
  });
}