#include "Tests.h"
#include <common/IPrefix.h>
#include <skse/GameForms.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/LoadGame.h>

SKYRIM_TEST_CASE(TestChangeGameTime)
{
  std::array<float, 3> pos = { -86642.1875f, 8655.1689f, -3890.1050f },
                       rot = { 0.0000f, -0.0000f, 317.9684f };
  uint32_t worldId = Tamriel;
  Time time;
  time.Set(0, 30, 12);

  LoadGame::Run(pos, rot, worldId, &time);

  ctx->TaskGameThread([=] {
    float hour = sd::GetValue(Cast::Run<TESGlobal>(ID_TESGlobal::GameHour));
    SKYRIM_ASSERT(abs(hour - 12.5) < 0.1);
    ctx->Pass();
  });
}