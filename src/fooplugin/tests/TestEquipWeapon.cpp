#include "Tests.h"
#include <skyrim_plugin_base/utils/ApplyEquipment.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/GetEquipment.h>
#include <skyrim_plugin_base/utils/LoadGame.h>
#include <skyrim_plugin_base/utils/PlaceAtMePersistent.h>
#include <skyrim_plugin_base/utils/WeaponCreator.h>

namespace {
void ChekEquipedWeaponID(Actor* refr,
                         std::map<uint32_t, WeaponCreator::SlotType>& cache)
{
  Equipment equipment = GetEquipment::Run(sd::GetPlayer());



  if(refr->GetEquippedObject(false))
  SKYRIM_ASSERT(refr->GetEquippedObject(false)->formID ==
                cache[equipment.equippedWeapon.rightHand].right);

  if(refr->GetEquippedObject(true))
  SKYRIM_ASSERT(refr->GetEquippedObject(true)->formID ==
                cache[equipment.equippedWeapon.leftHand].left);
}

void ChangeEquipment(WeaponCreator& weapCreator, Actor* refr)
{
  auto equipment = GetEquipment::Run(sd::GetPlayer());
  ApplyEquipment::Run(refr, equipment, weapCreator);
}
}

SKYRIM_TEST_CASE(TestEquipmentApplyWeapon)
{
  LoadGame::Run({ -86642.1875f, 8655.1689f, -3890.1050f },
                { 0.0000f, -0.0000f, 317.9684f }, Tamriel);

  ctx->TaskGameThread([=] {
    WeaponCreator weapCreator;

    auto base = Cast::Run<TESNPC>(ID_TESNPC::Astrid);

    auto refr =
      Cast::Run<Actor>(PlaceAtMePersistent::Run(sd::GetPlayer(), base));
    SKYRIM_ASSERT(refr);

    sd::SetGodMode(true);
    sd::StartCombat(refr, sd::GetPlayer());

    ChangeEquipment(weapCreator, refr);
    sd::Wait(2000);

    ChekEquipedWeaponID(refr, weapCreator.weaponIdCash);

    sd::EquipItem(sd::GetPlayer(),
                  sd::GetFormById(ID_TESObjectWEAP::DaedricSword),
                  false, false);

    ChangeEquipment(weapCreator, refr);
    sd::Wait(3000);

    ChekEquipedWeaponID(refr, weapCreator.weaponIdCash);

    ctx->Pass();
  });
}