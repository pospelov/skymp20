#pragma once
#include <windows.h>
#include <winuser.h>

class KeyDown
{
public:
  enum
  {
    Down = 'S',
    Right = 'D',
    Up = 'W',
    Left = 'A'
  };

  KeyDown(int vkCode)
    : m_scanCode(MapVirtualKeyA(vkCode, MAPVK_VK_TO_VSC))
    , m_vkCode(vkCode)
  {
    Press();
  }

  ~KeyDown() { Release(); }

private:
  const int m_scanCode = 0;
  const int m_vkCode = 0;

  void Press() { keybd_event(m_vkCode, m_scanCode, 0, 0); }
  void Release() { keybd_event(m_vkCode, m_scanCode, KEYEVENTF_KEYUP, 0); }
};