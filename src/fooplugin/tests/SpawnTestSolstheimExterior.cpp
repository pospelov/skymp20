#include "Tests.h"
#include <skyrim_plugin_base/utils/LoadGame.h>

SKYRIM_TEST_CASE(SpawnTestSolstheimExterior)
{

  auto index = GetLoadedModIndex("Dragonborn.esm");
  if (index != 2)
    throw std::runtime_error(
      "Dragonborn.esm out of place (Should load first)");

  std::array<float, 3> pos = { 25945.3f, 36973.8f, 559.459f },
                       rot = { 12.0f, -0.0000f, 119.88f };

  uint32_t worldId = Solstheim;

  LoadGame::Run(pos, rot, worldId);

  ctx->TaskGameThread([=] {
    std::array<float, 3> realPos = GetPlayerPos();
    std::array<float, 3> realRot = GetPlayerRot();
    auto world = sd::GetWorldSpace(sd::GetPlayer());

    SKYRIM_ASSERT(world != nullptr);
    SKYRIM_ASSERT(world->formID == worldId);
    SKYRIM_ASSERT(abs(pos[0] - realPos[0]) < 50.f);
    SKYRIM_ASSERT(abs(pos[1] - realPos[1]) < 50.f);
    SKYRIM_ASSERT(abs(pos[2] - realPos[2]) < 50.f);
    SKYRIM_ASSERT(abs(rot[2] - realRot[2]) < 1.f);

    ctx->Pass();
  });
}