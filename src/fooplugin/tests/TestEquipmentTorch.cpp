#include "Tests.h"
#include <PapyrusWeapon.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/LoadGame.h>
#include <skyrim_plugin_base/utils/PlaceAtMePersistent.h>
#include <skyrim_plugin_base\utils\ApplyEquipment.h>
#include <skyrim_plugin_base\utils\GetEquipment.h>
#include <skyrim_plugin_base\utils\WeaponCreator.h>


SKYRIM_TEST_CASE(TestEquipmentTorch)
{
  LoadGame::Run({ -86642.1875f, 8655.1689f, -3890.1050f },
                { 0.0000f, -0.0000f, 317.9684f }, Tamriel);

  ctx->TaskGameThread([=] {
    auto base = Cast::Run<TESNPC>(ID_TESNPC::Astrid);
    WeaponCreator weapCreator;
    Equipment equip;

    auto refr =
      Cast::Run<Actor>(PlaceAtMePersistent::Run(sd::GetPlayer(), base, equip));

    auto equipment = GetEquipment::Run(sd::GetPlayer());
    ApplyEquipment::Run(refr, equipment, weapCreator);
    sd::Wait(1000);
    sd::EquipItem(sd::GetPlayer(), sd::GetFormById(ID_TESObjectLIGH::Torch01),
                  true, false);
    SKYRIM_ASSERT(sd::GetEquippedItemType(refr, 0) != 11);

    sd::Wait(2000);
    equipment = GetEquipment::Run(sd::GetPlayer());
    ApplyEquipment::Run(refr, equipment, weapCreator);
    sd::Wait(2000);
    equipment = GetEquipment::Run(sd::GetPlayer());
    ApplyEquipment::Run(refr, equipment, weapCreator);


    SKYRIM_ASSERT(sd::GetEquippedItemType(refr, 0) == 11);
    ctx->Pass();
  });
}