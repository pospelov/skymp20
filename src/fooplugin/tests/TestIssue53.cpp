#include "KeyDown.h"
#include "TestUtils.h"
#include "Tests.h"
#include <skyrim_plugin_base/DInputHook.h>
#include <skyrim_plugin_base/utils/ApplyEquipment.h>
#include <skyrim_plugin_base/utils/ApplyMovement.h>
#include <skyrim_plugin_base/utils/Cast.h>
#include <skyrim_plugin_base/utils/GetEquipment.h>
#include <skyrim_plugin_base/utils/GetMovement.h>
#include <skyrim_plugin_base/utils/LoadGame.h>
#include <skyrim_plugin_base/utils/SmartRef.h>
#include <skyrim_plugin_base/utils/Wait.h>

namespace {
class ClickFloodGuard
{
public:
  ClickFloodGuard() { DInputHook::SetClickFloodActive(true); }
  ~ClickFloodGuard() { DInputHook::SetClickFloodActive(false); }

private:
  ClickFloodGuard(const ClickFloodGuard&) = delete;
  ClickFloodGuard& operator=(const ClickFloodGuard&) = delete;
};
}

SKYRIM_TEST_CASE(TestGetLastDangerousButtonDownMoment)
{
  constexpr int oneFrame = 17;

  SKYRIM_ASSERT(!DInputHook::WasDangerousButtonDown(50));

  {
    ClickFloodGuard flooder;

    Sleep(1);
    SKYRIM_ASSERT(!DInputHook::WasDangerousButtonDown(50));

    Sleep(oneFrame + 20);
    SKYRIM_ASSERT(DInputHook::WasDangerousButtonDown(50));
  }

  Sleep(500);
  SKYRIM_ASSERT(!DInputHook::WasDangerousButtonDown(50));

  {
    KeyDown key('E');

    Sleep(oneFrame + 20);
    SKYRIM_ASSERT(DInputHook::WasDangerousButtonDown(100));
  }

  ctx->Pass();
}

SKYRIM_TEST_CASE(TestIssue53)
{
  using namespace std::chrono_literals;

  LoadGame::Run({ 165643.8281f, -69763.3125f, 8039.1234f }, { 0, 0, 180.f },
                0x3c);
  ctx->TaskGameThread([=] {
    FindClosestRef::Cache cache;
    auto playerStartingPos = GetPlayerPos<NiPoint3>();
    std::shared_ptr<SmartRef> smart(new SmartRef(playerStartingPos, cache));

    GetMovement::ActorCache getMovCache;
    WeaponCreator weapCreator;

    {
      KeyDown('P');
      sd::Wait(333);
      KeyDown('S');
      sd::Wait(333);
      KeyDown('S');
      sd::Wait(333);
      KeyDown('A');
      sd::Wait(333);
    }

    ClickFloodGuard clickFlood;

    sd::GetPlayer()->DrawSheatheWeapon(1);

    auto was = std::chrono::steady_clock::now();

    uint32_t i = 0, n = 0;
    while (1) {
      auto ref = Cast::Run<Actor>(smart->GetRef());
      SKYRIM_ASSERT(ref);

      auto mov = GetMovement::Run(sd::GetPlayer(), getMovCache);
      mov.pos[0] -= 128;
      mov.pos[1] -= 128;
      ApplyMovement::Run(ref, mov);

      auto eq = GetEquipment::Run(sd::GetPlayer());
      ApplyEquipment::Run(ref, eq, weapCreator);

      if (std::chrono::steady_clock::now() - was > 15000ms)
        break;

      Wait::Run();
    }

    ctx->Pass();
  });
}