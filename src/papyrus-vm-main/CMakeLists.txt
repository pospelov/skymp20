file(GLOB src "${CMAKE_CURRENT_SOURCE_DIR}/src/papyrus-vm-lib/*")
add_library(papyrus-vm-lib STATIC ${src})

file(GLOB src "${CMAKE_CURRENT_SOURCE_DIR}/src/papyrus-vm-main/*")
add_executable(papyrus-vm-main ${src})
target_include_directories(papyrus-vm-main PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/src/papyrus-vm-lib")
target_link_libraries(papyrus-vm-main PRIVATE papyrus-vm-lib Catch2::Catch2)
set(VM_MAIN_PATH ${CMAKE_CURRENT_BINARY_DIR}/$<CONFIG>/papyrus-vm-main.exe)
