#include "MovementComparison.h"
#include <imgui.h>
#include <string>
#include <vector>

namespace {
template <class T>
std::string to_hex(T v)
{
  std::stringstream ss;
  ss << std::hex << "0x" << v;
  return ss.str();
}

std::string to_string(std::array<float, 3> pos)
{
  char buf[128] = { 0 };
  sprintf_s(buf, "[%f, %f, %f]", pos[0], pos[1], pos[2]);
  return buf;
}
}

void MovementComparison::Draw(const MovementComparisonData &data)
{
  static const std::string g_runModeNames[] = { "Standing", "Walking",
                                                "Running", "Sprinting" };

  ImGui::Begin("###test_movement", 0,
               ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize |
                 ImGuiWindowFlags_NoCollapse);

  ImGui::SetWindowPos({ 0, 0 });
  ImGui::SetWindowSize({ 512 * 1.5, 256 });

  ImGui::Columns(2);
  for (int i = 0; i < 2; ++i) {
    auto& m = data.movements[i];
    auto movDifference = data.movements[0].GetDifference(data.movements[1]);

    ImGui::Separator();

    struct Entry
    {
      std::string key;
      MovementComponent comp;
      std::string value;
    };

    std::vector<Entry> kv;
    kv.push_back({ "cellOrWorld", MovementComponent::CellOrWorld,
                   to_hex(m.cellOrWorld) });
    kv.push_back({ "pos", MovementComponent::Pos, to_string(m.pos) });
    kv.push_back(
      { "angleZ", MovementComponent::AngleZ, std::to_string(m.angleZ) });
    kv.push_back({ "runMode", MovementComponent::RunMode,
                   g_runModeNames[(int)m.runMode] });
    kv.push_back({ "direction", MovementComponent::Direction,
                   std::to_string(m.direction) });
    kv.push_back({ "isWeapDrawn", MovementComponent::IsWeapDrawn,
                   std::to_string(m.isWeapDrawn) });
    kv.push_back({ "isInJumpState", MovementComponent::IsInJumpState,
                   std::to_string(m.isInJumpState) });
    kv.push_back({ "isBlocking", MovementComponent::IsBlocking,
                   std::to_string(m.isBlocking) });
    kv.push_back({ "isSneaking", MovementComponent::IsSneaking,
                   std::to_string(m.isSneaking) });

    for (auto entry : kv) {

      bool differs = movDifference.count(entry.comp) > 0;

      if (differs)
        ImGui::PushStyleColor(ImGuiCol_Text, { 1, 0, 0, 1 });

      float red =
        decltype(data.redByCompStrike)(data.redByCompStrike)[entry.comp];
      ImGui::Text("[%d] %s: %s", (int)red, entry.key.data(),
                  entry.value.data());

      if (differs)
        ImGui::PopStyleColor();
    }

    ImGui::NextColumn();
  }

  ImGui::End();
}