#pragma once
#include <functional>
#include <optional>
#include <string>
#include <utility>
#include <vector>
#include <viet/Logging.h> // Logging::Severity

struct DebugPresenter
{
  using ForEachCb = std::function<void(Viet::Logging::Severity, const char*)>;

  std::function<void(ForEachCb)> forEachLogLine;

  std::function<std::string()> getNotification;

  std::function<void(std::vector<std::string>)> startServer;
  std::function<void(std::vector<std::string>)> connect;
};

class DebugView
{
public:
  DebugView(DebugPresenter presenter) noexcept;
  ~DebugView();

  void OnPresent();

private:
  struct Impl;
  Impl* const pImpl;
};