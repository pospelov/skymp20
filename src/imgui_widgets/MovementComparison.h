#pragma once
#include <array>
#include <cstdint>
#include <foo/PropMovement.h>
#include <map>

struct MovementComparisonData
{
  std::array<Movement, 2> movements;
  uint32_t total = 0;
  std::map<MovementComponent, uint32_t> redByCompStrike;
};

class MovementComparison
{
public:
  static void Draw(const MovementComparisonData& data);
};