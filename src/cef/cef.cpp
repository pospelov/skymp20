// Based on https://github.com/qwertzui11/cef_osr

#include <cstdint>
#include <iostream>
#include <list>
#include <mutex>
#include <optional>
#include <sstream>
#include <stdio.h>
#include <vector>

#include <include/cef_app.h>
#include <include/cef_client.h>
#include <include/cef_life_span_handler.h>
#include <include/cef_load_handler.h>
#include <include/cef_render_handler.h>
#include <include/cef_render_process_handler.h>
#include <include/wrapper/cef_helpers.h>

#include "cef.h"

typedef void(OnPaint)(void* ctx, const void* paintData, uint32_t size);

using OnPaint_ = OnPaint;

struct
{
  std::mutex m;
  int flags = 0;
  std::optional<cef_mouse_event_t> mouseMove;

} g_share;

struct
{
  std::mutex m;
  OnPaint* cb = nullptr;
  void* ctx = nullptr;
} g_share2;

struct
{
  std::mutex m;
  std::list<std::string> outCustomPackets;
  uint32_t requiredBufSize = 1;
} g_share3;

class V8Handler : public CefV8Handler
{
public:
  V8Handler(CefRefPtr<CefBrowser> br) { m_browser = br; }

  std::vector<std::string> GetFunctionNames()
  {
    return { "sendCustomPacket" };
  }

  bool Execute(const CefString& name, CefRefPtr<CefV8Value> object,
               const CefV8ValueList& arguments, CefRefPtr<CefV8Value>& retval,
               CefString& exception) override
  {
    if (name == "sendCustomPacket") {

      auto msg = CefProcessMessage::Create(name);
      auto args = msg->GetArgumentList();

      if (arguments.size() == 0) {
        return true;
      }
      assert(arguments[0]);
      if (!arguments[0]) {
        return true;
      }

      assert(arguments[0]->IsString());
      auto customPacketContent =
        arguments[0]->IsString() ? arguments[0]->GetStringValue() : "";
      args->SetString(0, customPacketContent);

      auto frame = m_browser->GetMainFrame();
      assert(frame);
      if (frame) {
        frame->SendProcessMessage(PID_BROWSER, msg);
      }
      return true;
    }

    // Function does not exist.
    return false;
  }

  // Provide the reference counting implementation for this class.
  IMPLEMENT_REFCOUNTING(V8Handler);

private:
  CefRefPtr<CefBrowser> m_browser;
};

class RenderProcessHandler : public CefRenderProcessHandler
{
public:
  void OnContextCreated(CefRefPtr<CefBrowser> browser,
                        CefRefPtr<CefFrame> frame,
                        CefRefPtr<CefV8Context> context) override
  {

    CefRefPtr<CefV8Value> skympObject =
      CefV8Value::CreateObject(nullptr, nullptr);

    // Create an instance of my CefV8Handler object.
    CefRefPtr<V8Handler> handler = new V8Handler(browser);

    auto keys = handler->GetFunctionNames();
    auto v8PropFlags = cef_v8_propertyattribute_t(V8_PROPERTY_ATTRIBUTE_NONE);
    for (auto& key : keys) {
      CefRefPtr<CefV8Value> func = CefV8Value::CreateFunction(key, handler);
      skympObject->SetValue(key, func, v8PropFlags);
    }
    context->GetGlobal()->SetValue("_skymp", skympObject, v8PropFlags);
  }

  IMPLEMENT_REFCOUNTING(RenderProcessHandler);

private:
};

class RenderHandler : public CefRenderHandler
{
public:
  RenderHandler(int width_, int height_)
    : width(width_)
    , height(height_)
  {
  }

  void GetViewRect(CefRefPtr<CefBrowser> browser, CefRect& rect) override
  {
    rect = CefRect(0, 0, this->width, this->height);
  }

  void OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type,
               const RectList& dirtyRects, const void* buffer, int width,
               int height) override
  {
    if (type != PaintElementType::PET_VIEW)
      return;

    const uint32_t size = width * height * 4;

    std::unique_lock l(g_share2.m);

    auto cb = g_share2.cb;
    auto ctx = g_share2.ctx;

    l.unlock();

    g_share2.cb(g_share2.ctx, buffer, size);
  }

  IMPLEMENT_REFCOUNTING(RenderHandler);

private:
  const int width;
  const int height;
};

class BrowserClient : public CefClient
{
public:
  BrowserClient(RenderHandler* renderHandler)
    : m_renderHandler(renderHandler)
  {
  }

  CefRefPtr<CefRenderHandler> GetRenderHandler() override
  {
    return m_renderHandler;
  }

  bool OnProcessMessageReceived(CefRefPtr<CefBrowser> browser,
                                CefRefPtr<CefFrame> frame,
                                CefProcessId source_process,
                                CefRefPtr<CefProcessMessage> message) override
  {
    assert(message && message->IsValid());
    if (!message || !message->IsValid())
      return false;

    auto name = message->GetName();
    if (name == "sendCustomPacket") {
      auto args = message->GetArgumentList();
      if (args->GetSize() != 1) {
        assert(0 && "Bad message arguments count");
        return true;
      }
      auto str = args->GetString(0);

      std::lock_guard l(g_share3.m);
      g_share3.outCustomPackets.push_back(str);
      if (str.size() >= g_share3.requiredBufSize) {
        g_share3.requiredBufSize = str.size() + 1;
      }
      return true;
    }

    return false;
  }

  CefRefPtr<CefRenderHandler> m_renderHandler;

  IMPLEMENT_REFCOUNTING(BrowserClient);
};

struct MyCefTaskContext
{
  CefRefPtr<CefBrowser> br;
  CefRefPtr<BrowserClient> brClient;
};

using MyCefTask = std::function<bool(MyCefTaskContext)>;

class CefGlobalState
{
public:
  static CefGlobalState& GetSingleton()
  {
    static CefGlobalState g;
    return g;
  }

  bool PollTask(MyCefTask* out)
  {
    assert(out);
    if (!out)
      return false;

    std::lock_guard l(m);

    if (events.empty())
      return false;

    *out = std::move(events.front());
    events.pop_front();

    return true;
  }

  void AddTask(const MyCefTask& e)
  {
    std::lock_guard l(m);
    events.push_back(e);
  }

private:
  CefGlobalState() {}

  std::mutex m;
  std::list<MyCefTask> events;
};

class MyCefApp : public CefApp
{
public:
  MyCefApp(CefRefPtr<CefRenderProcessHandler> handler)
  {
    m_renderProcessHandler = handler;
  }

  CefRefPtr<CefRenderProcessHandler> GetRenderProcessHandler() override
  {
    return m_renderProcessHandler;
  }

  IMPLEMENT_REFCOUNTING(MyCefApp);

private:
  CefRefPtr<CefRenderProcessHandler> m_renderProcessHandler;
};

CefRefPtr<CefBrowser> g_browser;
CefRefPtr<BrowserClient> g_browserClient;

int cef_stuff::Main()
{
  CefMainArgs args;

  CefRefPtr<CefRenderProcessHandler> renderProcessHandler =
    new RenderProcessHandler();
  CefRefPtr<CefApp> cefApp = new MyCefApp(renderProcessHandler);

  std::thread([=] {
    while (1) {
      if (!FindWindowA("Skyrim", "Skyrim"))
        ExitProcess(0);
      Sleep(500);
    }
  })
    .detach();

  {
    const int result = CefExecuteProcess(args, cefApp, nullptr);
    // checkout CefApp, derive it and set it as second parameter, for more
    // control on command args and resources.
    if (result >= 0) { // child proccess has endend, so exit.
      return result;
    } else if (result == -1) {
      // we are here in the father proccess.
    }
  }

  {
    CefSettings settings;
    settings.remote_debugging_port = 8000;
    settings.windowless_rendering_enabled = true;

    CefString(&settings.resources_dir_path).FromASCII("");
    CefString(&settings.locales_dir_path).FromASCII("");

    // checkout detailed settings options
    // http://magpcss.org/ceforum/apidocs/projects/%28default%29/_cef_settings_t.html
    // nearly all the settings can be set via args too.
    // settings.multi_threaded_message_loop = true; // not supported, except
    // windows
    CefString(&settings.browser_subprocess_path)
      .FromASCII("Data/Multiplayer/skymp_cef_renderer_exe.exe");
    // CefString(&settings.cache_path).FromASCII("");
    // CefString(&settings.log_file).FromASCII("");
    // settings.log_severity = LOGSEVERITY_DEFAULT;

    const bool result = CefInitialize(args, settings, nullptr, nullptr);
    // CefInitialize creates a sub-proccess and executes the same
    // executeable, as calling CefInitialize, if not set different in
    // settings.browser_subprocess_path if you create an extra program just
    // for the childproccess you only have to call CefExecuteProcess(...) in
    // it.
    if (!result) {
      // handle error
      return -1;
    }
  }

  const auto hwnd = FindWindowA("Skyrim", "Skyrim");
  assert(hwnd);
  RECT rect;
  GetWindowRect(hwnd, &rect);
  const auto width = rect.right - rect.left;
  const auto height = rect.bottom - rect.top;
  CefRefPtr<RenderHandler> renderHandler = new RenderHandler(width, height);

  {
    CefWindowInfo window_info;
    CefBrowserSettings browserSettings;

    browserSettings.windowless_frame_rate = 30; // 30 is default

    window_info.SetAsWindowless(NULL);

    g_browserClient = new BrowserClient(renderHandler);

    // "file://C:\\Users\\Leonid\\AppData\\Local\\Temp\\skymp_front\\index.html"
    g_browser = CefBrowserHost::CreateBrowserSync(
      window_info, g_browserClient.get(), "https://google.com/",
      browserSettings, nullptr, nullptr);
    // g_browser->GetMainFrame()->LoadURL("https://vk.com");

    // inject user-input by calling - non-trivial for non-windows - checkout
    // the cefclient source and the platform specific cpp, like
    // cefclient_osr_widget_gtk.cpp for linux
    // browser->GetHost()->SendKeyEvent(...);
    // browser->GetHost()->SendMouseMoveEvent(...);
    // browser->GetHost()->SendMouseClickEvent(...);
    // browser->GetHost()->SendMouseWheelEvent(...);
  }
  return 0;
}

extern "C" {
__declspec(dllexport) void SkympCef_Configure()
{
  cef_stuff::Main();
}

__declspec(dllexport) void SkympCef_Tick()
{
  assert(g_browser);
  assert(g_browserClient);

  MyCefTask e;
  MyCefTaskContext ctx;
  // send events to browser

  std::vector<MyCefTask> toAdd;

  while (CefGlobalState::GetSingleton().PollTask(&e)) {
    ctx = {};
    ctx.br = g_browser;
    ctx.brClient = g_browserClient;
    const bool success = e(ctx);
    if (!success) {
      toAdd.push_back(e);
    }
  }

  for (auto& task : toAdd) {
    CefGlobalState::GetSingleton().AddTask(task);
  }

  CefDoMessageLoopWork();
}

__declspec(dllexport) int SkympCef_Main()
{
  return cef_stuff::Main();
}

__declspec(dllexport) void SkympCef_SetFocus(bool focus)
{
  CefGlobalState::GetSingleton().AddTask([=](MyCefTaskContext ctx) {
    auto h = ctx.br->GetHost();
    assert(h);
    if (h) {
      h->SetFocus(focus);
    }
    return true;
  });
}

// MouseButtons: LEFT = 0, MIDDLE = 1, RIGHT = 2
__declspec(dllexport) void SkympCef_OnMouseStateChange(float x, float y,
                                                       uint8_t mouseButton,
                                                       bool down)
{
  CefGlobalState::GetSingleton().AddTask([=](MyCefTaskContext ctx) {
    CefRefPtr<CefBrowser>& br = ctx.br;

    auto host = br->GetHost();
    assert(host);
    if (!host)
      return true;

    cef_mouse_event_t e;
    e.x = int(std::floor(x));
    e.y = int(std::floor(y));

    {
      std::lock_guard l(g_share.m);
      e.modifiers = g_share.flags;
    }

    host->SendMouseClickEvent(e, (cef_mouse_button_type_t)mouseButton, !down,
                              1);
    return true;
  });
}

__declspec(dllexport) void SkympCef_OnMouseMove(float x, float y)
{
  CefGlobalState::GetSingleton().AddTask([=](MyCefTaskContext ctx) {
    auto host = ctx.br->GetHost();
    assert(host);
    if (!host)
      return true;

    cef_mouse_event_t e;
    e.x = int(std::floor(x));
    e.y = int(std::floor(y));

    {
      std::lock_guard l(g_share.m);
      e.modifiers = g_share.flags;

      g_share.mouseMove = e;
    }

    host->SendMouseMoveEvent(e, false);
    return true;
  });
}

__declspec(dllexport) void SkympCef_OnMouseWheel(int wheelDelta)
{
  CefGlobalState::GetSingleton().AddTask([=](MyCefTaskContext ctx) {
    auto host = ctx.br->GetHost();
    assert(host);
    if (!host)
      return true;

    std::lock_guard l(g_share.m);
    if (!g_share.mouseMove)
      return true;

    const bool isHorizontalScrolling =
      g_share.flags & (int)cef_event_flags_t::EVENTFLAG_SHIFT_DOWN;
    host->SendMouseWheelEvent(*g_share.mouseMove,
                              isHorizontalScrolling ? wheelDelta : 0,
                              isHorizontalScrolling ? 0 : wheelDelta);

    return true;
  });
}

__declspec(dllexport) void SkympCef_OnKeyStateChange(uint8_t vkCode, bool down)
{
  CefGlobalState::GetSingleton().AddTask([=](MyCefTaskContext ctx) {
    auto host = ctx.br->GetHost();
    assert(host);
    if (!host)
      return true;

    std::lock_guard l(g_share.m);

    cef_key_event_t keyEvent;
    keyEvent.windows_key_code = vkCode;
    keyEvent.focus_on_editable_field = true;
    keyEvent.is_system_key = false;
    keyEvent.modifiers = g_share.flags;
    keyEvent.type = down ? cef_key_event_type_t::KEYEVENT_KEYDOWN
                         : cef_key_event_type_t::KEYEVENT_KEYUP;
    host->SendKeyEvent(keyEvent);

    static auto flagByVkCode = [](int vkCode) {
      switch (vkCode) {
        case VK_SHIFT:
          return cef_event_flags_t::EVENTFLAG_SHIFT_DOWN;
        case VK_CONTROL:
          return cef_event_flags_t::EVENTFLAG_CONTROL_DOWN;
        case VK_MENU:
          return cef_event_flags_t::EVENTFLAG_ALT_DOWN;
        case VK_LBUTTON:
          return cef_event_flags_t::EVENTFLAG_LEFT_MOUSE_BUTTON;
        case VK_RBUTTON:
          return cef_event_flags_t::EVENTFLAG_RIGHT_MOUSE_BUTTON;
        case VK_MBUTTON:
          return cef_event_flags_t::EVENTFLAG_MIDDLE_MOUSE_BUTTON;
      }
      return cef_event_flags_t::EVENTFLAG_NONE;
      // TODO
      /*if (::GetKeyState(VK_NUMLOCK) & 1)
              modifiers |= IBrowserTab::NumLockOn;
      if (::GetKeyState(VK_CAPITAL) & 1)
              modifiers |= IBrowserTab::CapsLockOn;*/
    };
    const auto flag = flagByVkCode(vkCode);
    if (flag != cef_event_flags_t::EVENTFLAG_NONE) {
      if (down) {
        g_share.flags |= (int)flag;
      } else {
        g_share.flags &= ~(int)flag;
      }
    }

    return true;
  });
}

__declspec(dllexport) void SkympCef_OnChar(wchar_t character)
{
  CefGlobalState::GetSingleton().AddTask([=](MyCefTaskContext ctx) {
    auto host = ctx.br->GetHost();
    assert(host);
    if (!host)
      return true;

    std::lock_guard l(g_share.m);

    cef_key_event_t keyEvent;
    keyEvent.windows_key_code = character;
    keyEvent.focus_on_editable_field = true;
    keyEvent.is_system_key = false;
    keyEvent.modifiers = g_share.flags;
    keyEvent.type = cef_key_event_type_t::KEYEVENT_CHAR;
    host->SendKeyEvent(keyEvent);
    return true;
  });
};

__declspec(dllexport) void SkympCef_LoadUrl(const wchar_t* url_)
{
  std::wstring url = url_;
  CefGlobalState::GetSingleton().AddTask([=](MyCefTaskContext ctx) {
    auto frame = ctx.br->GetMainFrame();
    assert(frame);
    if (frame) {
      frame->LoadURL(url);
    }
    return true;
  });
};

__declspec(dllexport) void SkympCef_ExecuteJs(const char* jsSrc_)
{
  std::string jsSrc = jsSrc_;
  CefGlobalState::GetSingleton().AddTask([=](MyCefTaskContext ctx) {
    CefRefPtr<CefFrame> frame = ctx.br->GetMainFrame();
    assert(frame);
    if (frame) {
      if (!ctx.br->IsLoading()) {
        frame->ExecuteJavaScript(jsSrc, frame->GetURL(), 0);
        return true;
      } else {
        return false;
      }
    }

    return true;
  });
};

__declspec(dllexport) uint32_t SkympCef_GetCPRequiredBufSize()
{
  std::lock_guard l(g_share3.m);
  return g_share3.requiredBufSize;
}

__declspec(dllexport) void SkympCef_NextOutCP(char* outBuf, bool* outSuccess,
                                              uint32_t bufSize)
{
  assert(outBuf);
  assert(outSuccess);
  assert(bufSize > 0);
  if (!outBuf || !outSuccess || !bufSize)
    return;

  std::lock_guard l(g_share3.m);

  bool empty = g_share3.outCustomPackets.empty();
  if (empty) {
    *outSuccess = false;
    return;
  }

  auto v = std::move(g_share3.outCustomPackets.front());
  g_share3.outCustomPackets.pop_front();

  // bufSize must be at least v.size() + 1 because it's a null-terminated
  // string
  assert(bufSize > v.size());
  if (bufSize <= v.size()) {
    *outSuccess = false;
    return;
  }

  memcpy(outBuf, v.data(), v.size());
  outBuf[v.size()] = 0;
  *outSuccess = true;
}

__declspec(dllexport) void SkympCef_SetOnPaint(void* ctx, OnPaint* callback)
{
  assert(ctx);
  assert(callback);

  std::lock_guard l(g_share2.m);
  g_share2.cb = callback;
  g_share2.ctx = ctx;
}
}