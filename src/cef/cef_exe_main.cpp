#include <Windows.h>

#include "cef.h"

INT __stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                      PSTR lpCmdLine, INT nCmdShow)
{
  return cef_stuff::Main();
}