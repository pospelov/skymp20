#include "ImGuiContext.h"
#include <Windows.h>
#include <array>
#include <d3d9.h>

#include "imgui.h"
#include "imgui_impl_dx9.h"

namespace {
ImVec2 GetWindowSize(HWND hwnd)
{
  if (!hwnd)
    throw std::runtime_error("bad hwnd");
  RECT rect;
  GetWindowRect(hwnd, &rect);
  const auto width = rect.right - rect.left;
  const auto height = rect.bottom - rect.top;
  auto res = ImVec2((float)width, (float)height);
  if (res.x == 0 || res.y == 0)
    throw std::runtime_error("bad window size");
  return res;
}
}

thread_local ImFont* g_font = nullptr;

MyImGuiContext::MyImGuiContext(void* device, HWND hwnd)
{
  ImGui::CreateContext();
  auto& io = ImGui::GetIO();

  if (!g_font) {
    g_font = ImGui::GetIO().Fonts->AddFontDefault();
  }

  unsigned char* tex_pixels = NULL;
  int tex_w, tex_h;
  io.Fonts->GetTexDataAsRGBA32(&tex_pixels, &tex_w, &tex_h);

  // io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard
  // Controls
  io.DisplaySize = GetWindowSize(hwnd);

  ImGui_ImplDX9_Init(static_cast<IDirect3DDevice9*>(device));
  ImGui::StyleColorsDark();
}

void MyImGuiContext::Render(void* device, Callback drawWidgets, float mouseX,
                            float mouseY, bool allowInputs) noexcept
{
  const float mouseWheelAbsolute = 0;

  ImGui::NewFrame();
  ImGui_ImplDX9_NewFrame();

  auto& io = ImGui::GetIO();

  std::array<uint8_t, 256> keyboardState;
  keyboardState.fill(0x00);
  if (io.KeyShift) {
    keyboardState[VK_SHIFT] = 0xff;
  }

  for (int32_t i = 0; i < 256; ++i) {
    const bool down = GetKeyPressed(i);
    if (down != io.KeysDown[i]) {
      io.KeysDown[i] = down;
      if (down && allowInputs) {
        wchar_t buf[128] = { 0 };
        ToUnicode(i, 0, keyboardState.data(), buf, std::size(buf), 0);
        if (buf[0])
          io.AddInputCharacter(buf[0]);
      }
    }
  }

  if (allowInputs) {
    io.MouseDown[0] = GetKeyPressed(VK_LBUTTON);
    io.MouseDown[1] = GetKeyPressed(VK_RBUTTON);
    io.MouseDown[2] = GetKeyPressed(VK_MBUTTON);
    io.KeyCtrl = GetKeyPressed(VK_CONTROL);
    io.KeyAlt = GetKeyPressed(VK_MENU);
    io.KeyShift = GetKeyPressed(VK_SHIFT);
    io.KeySuper = GetKeyPressed(VK_LWIN) || GetKeyPressed(VK_RWIN);
    io.MouseWheel = mouseWheelAbsolute;

    io.KeyMap[ImGuiKey_Tab] = VK_TAB;
    io.KeyMap[ImGuiKey_LeftArrow] = VK_LEFT;
    io.KeyMap[ImGuiKey_RightArrow] = VK_RIGHT;
    io.KeyMap[ImGuiKey_UpArrow] = VK_UP;
    io.KeyMap[ImGuiKey_DownArrow] = VK_DOWN;
    io.KeyMap[ImGuiKey_PageUp] = VK_PRIOR;
    io.KeyMap[ImGuiKey_PageDown] = VK_NEXT;
    io.KeyMap[ImGuiKey_Home] = VK_HOME;
    io.KeyMap[ImGuiKey_End] = VK_END;
    io.KeyMap[ImGuiKey_Insert] = VK_INSERT;
    io.KeyMap[ImGuiKey_Delete] = VK_DELETE;
    io.KeyMap[ImGuiKey_Backspace] = VK_BACK;
    io.KeyMap[ImGuiKey_Space] = VK_SPACE;
    io.KeyMap[ImGuiKey_Enter] = VK_RETURN;
    io.KeyMap[ImGuiKey_Escape] = VK_ESCAPE;
    io.KeyMap[ImGuiKey_A] = 'A'; // for text edit CTRL+A: select all
    io.KeyMap[ImGuiKey_C] = 'C'; // for text edit CTRL+C: copy
    io.KeyMap[ImGuiKey_V] = 'V'; // for text edit CTRL+V: paste
    io.KeyMap[ImGuiKey_X] = 'X'; // for text edit CTRL+X: cut
    io.KeyMap[ImGuiKey_Y] = 'Y'; // for text edit CTRL+Y: redo
    io.KeyMap[ImGuiKey_Z] = 'Z'; // for text edit CTRL+Z: undo

    io.MousePos = ImVec2(mouseX, mouseY);
  } else {
    io.MousePos = ImVec2(-1.f, -1.f);
  }

  ImGui::PushFont(g_font);

  drawWidgets();

  ImGui::PopFont();

  auto d = static_cast<IDirect3DDevice9*>(device);
  d->SetRenderState(D3DRS_ZENABLE, false);
  d->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
  d->SetRenderState(D3DRS_SCISSORTESTENABLE, false);

  ImGui::Render();
  ImGui_ImplDX9_RenderDrawData(ImGui::GetDrawData());
}

bool MyImGuiContext::GetKeyPressed(unsigned char key)
{
  return (GetAsyncKeyState(key) & 0x80000000) > 0;
}
