#pragma once
#include <Windows.h>
#include <functional>

class MyImGuiContext
{
public:
  using Callback = std::function<void()>;

  MyImGuiContext(void* device, HWND hwnd);

  void Render(void* device, Callback drawWidgets, float mouseX, float mouseY,
              bool allowInputs) noexcept;

private:
  bool GetKeyPressed(unsigned char key);
};
